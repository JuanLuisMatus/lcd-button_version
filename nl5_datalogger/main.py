"""
# Script to configure the interface between instruments and NL5 to use it as a
# data logger.
@author: EP
@date: 2020.04.23
@TODO:
"""
SCRIPT_VERSION = 1.0

import logging
import argparse
import sys
import time
from time import sleep
import xml.etree.ElementTree as ET
from urllib.parse import unquote
import requests
import threading
from multiprocessing import Process, Pool
import os
from shutil import move as move_file


def main():
    init_logger()
    # print = logging.info # redefine print to logging

    # initiate the parser
    parser = argparse.ArgumentParser()
    parser.add_argument("-V", "--version", help="show program version", action="store_true")
    parser.add_argument("-S", "--simfile", help="nl5 simulation file.", default=False)
    parser.add_argument("-I", "--ip", help="nl5 server ip", default='127.0.0.1')
    parser.add_argument("-P", "--port", help="nl5 server port.", default='80')

    args = parser.parse_args()

    args.simfile = 'example_simulation.nl5'
    # If running from Elcipse define the arguments
    if not args.simfile :
        logging.critical('You must specify the NL5 simulation file.')
        quit()
    
    # Print logfile header
    logging.info('Log file from running the test and calibration script of the Wabllbox/Quasar.')
    logging.info('Script version: %s' % SCRIPT_VERSION)
    logging.info('Start time %s' % time.strftime("%Y-%m-%d %H:%M"))

    nl5_file = args.simfile
    nl5_ip = args.ip
    nl5_port = args.port
    nl5_cli = nl5_client(nl5_file, nl5_ip, nl5_port)
    

def init_logger():
    # initialize the msg logger
    # DEBUG    Detailed information, typically of interest only when diagnosing problems.
    # INFO     Confirmation that things are working as expected.
    # WARNING  An indication that something unexpected happened, or indicative of some problem in the near future (e.g. ‘disk space low’). The software is still working as expected.
    # ERROR    Due to a more serious problem, the software has not been able to perform some function.
    # CRITICAL A serious error, indicating that the program itself may be unable to continue running.
    LOG_FILENAME = 'logfile.log'
    FORMAT_STR = '%(asctime)s\t%(levelname)s -- %(filename)s:%(lineno)s -- %(message)s'

    file_handler = logging.FileHandler(filename=LOG_FILENAME)
    stdout_handler = logging.StreamHandler(sys.stdout)
    
    handlers = [file_handler, stdout_handler]
    
    logging.basicConfig(
            level=logging.DEBUG, 
            format=FORMAT_STR,
            handlers=handlers)
    
    #sys.stderr = logging.error
    #sys.stdout = logging.info
    

class nl5_client:
    def __init__(self, nl5_file, nl5_ip, nl5_port):
        # define some general parameters for the nl5 client
        self.nl5_file = nl5_file
        self.ip = nl5_ip
        self.port = nl5_port
        self.asv_name = time.strftime("%Y%m%d_%H_%M")
        
        # load the nl5 xml file
        self.nl5_xml = ET.parse(nl5_file).getroot()
        
        # load parameters from a nl5 simulation comment
        self.params = self.get_nl5_params() # dict including parameters
    
        self.find_nl5_drvs()
    
        '''
        # open nl5 software
        logging.info('Opening NL5 simulation soft')
        cmd = 'nl5.exe -http ' + filename 
        process = subprocess.Popen(cmd,
                                   shell=True)

        sleep(3)
        #self.send_cmd('display on')
        '''
        
        tran_step = self.params['step']
        tran_start = 0
        tran_screen = self.params['step']
        
        self.time = tran_step
        
        cmd = 'rununtil t>=%.5f' % tran_step
        self.send_query(cmd)

        cmd = 'tran %.5f, %.5f, %.5f' % (tran_start, tran_screen, tran_step)
        self.send_query(cmd)
        sleep(.5)
        
        # start steps thread
        self.run_step() 

        # start autosave thread
        threading.Timer(10.0, self.autosave).start()


    def get_nl5_params(self):    
        # parse the parameters form a comment in nl5
        # search if there is a parameter comment
        header = 'python_driver_params'   
        nl5_xml = self.nl5_xml
        
        for cmp_pct in nl5_xml.iter('Pct'):
            cmp_type = cmp_pct.get('type')
            
            if cmp_type == 'pict':
                cmp_txt = cmp_pct.get('txt')
                params_txt = cmp_txt.replace('+', ' ') # replace + by space
                params_txt = unquote(params_txt) # converts %xx characters
                #cmp_txt = unquote(cmp_txt) # converts %xx characters
                
                if header not in params_txt :
                    logging.info('No NL5 parameters comment found. You can pass parameters including a NL5 comment with "python_driver_params" on first line.')
                    params = {'step': 1                             
                            }
                    
                    logging.info('Using default params idct: %s' % params)

                    return params
                    
                logging.info('NL5 parameters comment find, loading...')            
                # load parameters passed through NL5 IC text box
                # remove header line
                params_name, params_code = params_txt.split("\r\n",1)
                
                # verify if params_txt is a valid python code
                import ast
                try:
                    ast.parse(params_code)
                except SyntaxError:
                    logging.critical('NL5 %s comment does not contains valid python code.' % params_name)
                    quit()
                
                old_keys = list(locals().keys())
                exec(params_code) # execute python code
                new_variables = dict(locals())
                
                # obtain new variables
                params = {k:v for k,v in new_variables.items() if k not in old_keys}
                params.pop('old_keys') # removes this aux variable
                      
                logging.info('Loaded params dict: %s' % params)
                return params              


    def send_cmd(self, command):
        # send command to nl5 with http
        try:
            URL = ('http://%s/?cmd=%s' % (self.ip, command))
            logging.debug(URL)
            
            try: # workaround to speed things a little bit
                requests.get(URL, timeout=0.001)
            except requests.exceptions.ReadTimeout: 
                pass

        except Exception as error:
            logging.critical('Connection with NL5 software lost.')
            raise Exception(error)    


    def send_query(self, command):
        # send command to nl5 with http
        try:
            URL = ('http://%s/?cmd=%s' % (self.ip, command))
            logging.debug(URL)
    
            response = requests.get(URL)
    
            #logging.debug(response.text)
            return response.text
        
        except Exception as error:
            logging.critical('Connection with NL5 software lost.')
            raise Exception(error)    


    def run_step(self):
        # run a simulation step
        init_time = time.time()

        tran_step = self.params['step']
        stop_time = self.time + tran_step        
        
        cmd = 'rununtil t>=%.5f' % stop_time
        self.send_query(cmd)
        
        self.update_nl5_drivers()
        
        cmd = 'cont'
        self.send_query(cmd) 
       
        self.time = stop_time # update current simulation time
        
        end_time = time.time() -init_time
        next_step_time = float(tran_step) - end_time
        
        logging.debug('Run step time %.3f' % end_time)
        logging.debug('Next step in %.3f' % next_step_time)

        if next_step_time < 0:
            # run step as soon as posible
            logging.warning('Step took to long')
            next_step_time = 0
        
        threading.Timer(next_step_time, self.run_step).start()



    def autosave(self):
        try:
            logging.info('Executing autosave.')
            dir_path = './data/'
            file_name = self.asv_name + '.nlt'
            csv_name = self.asv_name + '.csv'
            
            # check if directory exists or not yet
            if not os.path.exists(dir_path):
                os.makedirs(dir_path)
    
            # Save simulation
            cmd = 'save'
            self.send_query(cmd) 
    
            # Save signals to nlt file
            cmd = 'savedata ' + file_name
            self.send_query(cmd)
            
            # ckeck if file exist
            while not os.path.isfile(file_name):
                sleep(.1)
                            
            # move files into created directory
            move_file(file_name, dir_path + file_name)
            
            
            # Save signals to csv file
            cmd = ('export ' + csv_name + ', 0, %.2f, %.2f' % (self.time, self.params['step']))
            self.send_query(cmd)
            
            # ckeck if file exist
            while not os.path.isfile(csv_name):
                sleep(.1)
                            
            # move files into created directory
            move_file(csv_name, dir_path + csv_name)
                        
            threading.Timer(60.0, self.autosave).start()

        except Exception as error:
            logging.error(error)
            raise Exception(error)


    def find_nl5_drvs(self):
        # Instruments are of X_NL5 type
        self.drivers = {} # dict including driver blocks

        nl5_xml = self.nl5_xml
        for cmp_xml in nl5_xml.iter('Cmp'):
            if cmp_xml.get('type') == 'X_NL5':
                cmp_name = cmp_xml.get('name')
                cmp_name = unquote(cmp_name) # converts %xx characters
                
                if 'driver_' in cmp_name:
                    try:
                        drv_name = cmp_name          
                        drv_xml = cmp_xml
            
                        # search if there is a parameter comment
                        for cmp_pct in nl5_xml.iter('Pct'):
                            cmp_type = cmp_pct.get('type')
                            
                            if cmp_type == 'pict':
                                cmp_txt = cmp_pct.get('txt')
                                #cmp_txt = unquote(cmp_txt) # converts %xx characters
                                
                                header = cmp_name + '_params'
                                if header in cmp_txt :
                                    params_xml =  cmp_txt
                                    break
                                else:
                                    params_xml =  ''
            
                        driver = cmp_driver(drv_name, drv_xml, params_xml)
                        
                        self.drivers.update({drv_name: driver})
 
                    except Exception as error:
                        logging.warning(error)
                        raise Exception(error)
                
                               
    def update_nl5_drivers(self):
        # function to update the drivers in parallel

        proc_list = []
        for idx, drv_name in enumerate(self.drivers):
            self.update_driver(drv_name)
        '''
            proc_list.append(threading.Thread(target=self.update_driver, args=(drv_name,)))

        for pro in proc_list:
            pro.start()
        '''



    def update_driver(self, drv_name):
        # function to update the driver state
        init_time = time.time()
        try:
            driver = self.drivers[drv_name]
            
            logging.debug('updating %s cmp ports' % (drv_name))

            # first update the ports values
            driver.update_state()

            # then update the nl5 driver ports
            for port in driver.inputs:
                port_node = driver.inputs[port]['node']

                source_value = self.get_voltage_source(port_node)

                self.drivers[drv_name].inputs[port]['value'] = source_value
                
                #value = int(float(value.split(',')[-1]))
                #value = self.charger.write(addr, value, dec, sign)
            
            for port in driver.outputs:
                port_value = driver.outputs[port]['value'];
                
                self.send_cmd('%s.%s=%f' % (drv_name, port, port_value))                


        except Exception as error:
            logging.error('%s cmp can\'t be updated. %s' % (drv_name, error))

        end_time = time.time()-init_time
        logging.debug('%s update time %.3f' % (drv_name, end_time))


    def get_voltage_source(self, node):
        # gets source from node
        nl5_xml = self.nl5_xml

        for cmp_xml in nl5_xml.iter('Cmp'):
            cmp_type = cmp_xml.get('type')
            if cmp_type == 'label' and cmp_xml.get('node0') == node:
                # this is the label connected to the input
                cmp_name = cmp_xml.get('name')
                cmp_name = unquote(cmp_name) # converts %xx characters

                souce_value = self.send_query('V(%s)' % cmp_name)
                souce_value = souce_value.split(',')[-1]

                try:
                    return float(souce_value)
                except:
                    return 0
        else:
            return 0

class cmp_driver:
    # creates a generic driver object
    def __init__(self, drv_name, drv_xml, params_xml):
        
        self.drv_name = drv_name
        self.drv_xml = drv_xml
        self.drv_state = 'FAIL'
        
        self.params = {}
        
        self.inputs = {}
        self.outputs = {}


        self.load_parameters(params_xml)        
        self.find_ports()
        self.generate_subcir()
        self.load_driver_class()
        
        
    def find_ports(self):
        # create driver
        drv_xml = self.drv_xml
                
        # Find inputs 
        for node, cmp in enumerate(drv_xml.iter('In'), 0):
            cmp_xml = cmp.get('name')
            cmp_name = unquote(cmp_xml) # converts %xx characters
            
            cmp_node = drv_xml.get('node'+str(node))

            value = 0
            params = cmp_name.split('_')
            
            self.inputs.update({cmp_name:{'xml':cmp_xml,
                                          'value':value,
                                          'node': cmp_node,
                                          'params': params
                
                                        }
                                })      
        # Find outputs 
        for cmp in drv_xml.iter('Out'):
            cmp_xml = cmp.get('name')
            cmp_name = unquote(cmp_xml) # converts %xx characters
            value = 0
            params = cmp_name.split('_')
            
            self.outputs.update({cmp_name:{'xml':cmp_xml,
                                          'value':value,
                                          'params': params
                
                                        }
                                })        
        logging.info('%s ports mapped.' % self.drv_name)
        
        
    def load_parameters(self, params_xml):
        # load parameters passed through NL5 IC text box
        if params_xml == '': # no parameters to load
            return
        
        params_txt = params_xml.replace('+', ' ') # replace + by space
        params_txt = unquote(params_txt) # converts %xx characters
        
        # remove first line
        params_name, params_code = params_txt.split("\r\n",1)
        
        # verifi if params_txt is a valid python code
        import ast
        try:
            ast.parse(params_code)
        except SyntaxError:
            logging.error('NL5 %s comment does not contains valid python code.' % params_name)
            return

        old_keys = list(locals().keys())
        
        exec(params_code) # execute python code
        
        new_variables = dict(locals())
        # obtain new variables
        
        params_list = {k:v for k,v in new_variables.items() if k not in old_keys}
        params_list.pop('old_keys') # removes this aux variable
        
        logging.info('%s parameters loaded.' %  self.drv_name)
        self.params = params_list
            

    def generate_subcir(self):
        # generates a nl5 xml subcir file to interact with driver
        # it contains labels with the inputs and outputs
        base_xml = ET.parse('aux_files/base_subcir.nl5').getroot() # load base xml
            
        # find Cmps root
        for root_Cmps in base_xml.iter('Cmps'):
            pass
        
        # find Pcts root
        for root_Pcts in base_xml.iter('Pcts'):
            pass
    
        # create labels inside nl5 file for inputs
        idx = 1
        for idx, port in enumerate(self.inputs, 1):
            port_xml = self.inputs[port]['xml']
            cmp_x0 = 0
            
            cmp_name = "%s" % port_xml
            cmp_id = "%s" % idx
            
            cmp_p = "%s,%s" % (cmp_x0, (1 * idx))
            cmp_text0 = "-%s,0" % (len(cmp_name)*10 + 10)
            cmp_text1 = "30,0"  
            
            cmp_template = '<Cmp type="label" id="%s" name="%s" file_name="" view="0" node0="%s" model="Label" ic="" r="0" v="1" />' % (cmp_id, cmp_name, cmp_id)
    
            cmp_element = ET.fromstring(cmp_template)       
            root_Cmps.append(cmp_element)
            
    
            pct_template = '<Pct type="label" undo_id="%s" id="%s" p="%s" text0="%s" text1="%s" textdir="0" />' % (cmp_id, cmp_id, cmp_p, cmp_text0, cmp_text1)
            pct_element = ET.fromstring(pct_template)       
            root_Pcts.append(pct_element)
        
        next_id = idx # restart index
        # create labels inside nl5 file for outputs
        for idx, port in enumerate(self.outputs, 1):
            port_xml = self.outputs[port]['xml']

            cmp_x0 = 10
            
            cmp_name = "%s" % port_xml
            cmp_id = "%s" % (idx + next_id)
            
            cmp_p = "%s,%s" % (cmp_x0, (1 * idx))
            cmp_text0 = "-%s,0" % (len(cmp_name)*10 + 10)
            cmp_text1 = "30,0"  
            
            cmp_template = '<Cmp type="label" id="%s" name="%s" file_name="" view="0" node0="%s" model="V" ic="" r="0" v="1" />' % (cmp_id, cmp_name, cmp_id)
    
            cmp_element = ET.fromstring(cmp_template)       
            root_Cmps.append(cmp_element)
            
    
            pct_template = '<Pct type="label" undo_id="%s" id="%s" p="%s" text0="%s" text1="%s" textdir="0" />' % (cmp_id, cmp_id, cmp_p, cmp_text0, cmp_text1)
            pct_element = ET.fromstring(pct_template)       
            root_Pcts.append(pct_element) 
    

            #print(ET.tostring(base_xml).decode())
            
        # write nl5 auxiliar driver file
        tree = ET.ElementTree()
        tree._setroot(base_xml)
        file_name = self.drv_name + ".nl5"
        tree.write(file_name)

        logging.info('%s subcir file generated.' % self.drv_name)


    def load_driver_class(self):
        # import driver functions form driver python file
        import importlib
        try:
            driver_python_file = self.drv_name
            
            module = importlib.import_module(driver_python_file)

            driver_class = getattr(module, 'cmp_driver')

            self.cmp_driver = driver_class()
            self.drv_state = 'OK'
            
            # Ejecute init code
            logging.info('%s executing init code.' % self.drv_name)
            logging.info('%s functions loaded.' % self.drv_name)
            
        except Exception as error:
            logging.error('%s functions cant be loaded. %s' % (self.drv_name, error))


    def update_state(self):
        # this function should be runned every simulation step
        # each driver has three functions update_step, update_input, update_output and init, delete
        try:
            # check if driver is down and reinitialize
            if not self.drv_state == 'OK':
                self.load_driver_class()
                self.drv_state = 'OK'
            
        except Exception as error:
            logging.error('coudn\t initialize %s, check device status. %s' % (self.drv_name, error))
            return
        
        try:
            logging.info('%s updating state.' % self.drv_name)
    
            self.cmp_driver.update_step()
            
            for port in self.inputs:                
                self.cmp_driver.update_input(self.inputs[port])        
            
            for port in self.outputs:
                value = self.cmp_driver.update_output(self.outputs[port]) 
                self.outputs[port]['value'] = value
                    
        except Exception as error:
            self.drv_state = 'FAIL'
            logging.error('%s updating state fail. %s' % self.drv_name, error)            

     
if __name__ == '__main__':
    main()
    
    
    