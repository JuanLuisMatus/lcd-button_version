CHARGER_NUMBER = 'WB043017'
CHARGER_NUMBER = 'WB039476'

from QuasarClass import quasar
import socket
import logging
import time

class cmp_driver:
    def __init__(self, params = {}):
        logging.info('Running quasar driver init code.')  
        self.charger = quasar('TEST', 'TCP', socket.gethostbyname(CHARGER_NUMBER)) # to connect through hmi tcp
        

    def update_input(self, input_port):
        ''' {cmp_name:{'xml':cmp_xml,
                      'value':value,
                      'node': cmp_node,
                      'params': params

                    }
            }
        '''
        init_time = time.time()

        value = int(float(input_port['value']))
        addr = int(input_port['params'][0])
        dec = int(float(input_port['params'][2]))
                
        if input_port['params'][1] == 's':
            sign = True
        else:
            sign = False
            
        self.charger.write(addr, value, dec, sign)
        
        print('inputs',time.time()- init_time)

        
    def update_output(self, output_port):
        ''' {cmp_name:{'xml':cmp_xml,
                      'value':value,
                      'node': cmp_node,
                      'params': params
                      }
            }
        '''
        init_time = time.time()

        addr = int(output_port['params'][0])
        dec = int(float(output_port['params'][2]))
                
        if output_port['params'][1] == 's':
            sign = True
        else:
            sign = False        
        
        value = self.charger.read(addr, dec, sign)
        logging.debug('Quasar reg: %s, value:%s ' % (addr, value))

        print('output',time.time()- init_time)

        return value
    
    def update_step(self):
        logging.debug('Running quasar driver step code')
        
        
        