"""
# Testbench to test driver_yokogawa 
@author: EP
@date: 2020.04.30
@TODO:
"""
# Yokogawa read functions: URMS, IRMS, IDC, P, S, LAMBda PHI
import driver_yokogawa

yokogawa = driver_yokogawa.cmp_driver()


items_list =['URMS,3', 'URMS,4', 'IRMS,3', 'IRMS,4', 'P,3', 'P,4']


for function in items_list:
    # generate an output_port element
    ''' {cmp_name:{'xml':cmp_xml,
              'value':value,
              'node': cmp_node,
              'params': params
              }
    }
    '''
    output_port = {'params': [function]}
    
    yoko_value = yokogawa.update_output(output_port)
    
    print("Yokogawa %s value: %.4f" % (function, yoko_value)) 



