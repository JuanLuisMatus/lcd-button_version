"""
# Driver to use Yokogawa form from nl5
@author: EP
@date: 2020.04.30
@TODO:
"""

# User libraries import for console run
import sys
import os
script_path = os.path.dirname(__file__)
sys.path.append(script_path + '\..\..\lib')

import logging
from instruments import itech_source

# this shoud be replaced with parameters form nl5
ITECH_ADDRESS = 'USB0::0x2EC7::0x6000::803384022747230013::0::INSTR'


class cmp_driver:
    def __init__(self, params):
        # initalization function
        logging.info('Executing ITECH init code.')
        
        itech_instrument = itech_source(ITECH_ADDRESS)
        
        #itech_instrument.recall_default()
        
        itech_instrument.clear_errors()
        itech_instrument.set_remote()
        
        # define settings
        itech_instrument.source_mode('VOLT')
        
        CURRENT_LIMIT = 60
        itech_instrument.set_current_limit_pos(CURRENT_LIMIT)
        itech_instrument.set_current_limit_neg(-CURRENT_LIMIT)

        itech_instrument.output_state('ON')

        self.itech_instrument = itech_instrument
        
        
    def update_input(self, input_port):
        # yoko has no inputs
        ''' {cmp_name:{'xml':cmp_xml,
                      'value':value,
                      'node': cmp_node,
                      'params': params

                    }
            }
        '''
        logging.info('Executing ITECH port update code.')
        port_name =  input_port['params'][0]
        if port_name == 'output':
            value = float(input_port['value'])
            
            if 10 <= value <= 800:
                self.itech_instrument.set_voltage(value)
            else:
                logging.error("ITECH setting %.2f, out of range [10, 800]" % value)
            

            
    def update_output(self, output_port):
        # yoko read variables
        ''' {cmp_name:{'xml':cmp_xml,
                      'value':value,
                      'node': cmp_node,
                      'params': params
                      }
            }
        '''
        pass
        
        
    def update_step(self):
        logging.info('Executing ITECH step code.')
        try:
            self.itech_instrument.read_errors()
        except:
            self.__init__()

        
    def __delete__(self):
        # close unit
        logging.info('Executing ITECH delete code.')
        del self.itech_instrument
        
        
        
        
        
        
        