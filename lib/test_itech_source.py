from time import sleep, time
from instruments import itech_source

ITECH_ADDRESS = 'USB0::0x2EC7::0x6000::803384022747250002::0::INSTR'



itech_instrument = itech_source(ITECH_ADDRESS)

IBATT = abs(itech_instrument.read_magnitude('current'))


quit()



itech_instrument.recall_default()

itech_instrument.clear_errors()

itech_instrument.set_remote()

itech_instrument.set_voltage(10)

itech_instrument.set_current_limit_pos(1)

itech_instrument.set_current_limit_neg(-1.5)

itech_instrument.source_mode('CURR')

itech_instrument.set_current(1)

itech_instrument.set_voltage_limit_pos(1)

itech_instrument.output_state('ON')

sleep(2)

itech_instrument.read_errors()



