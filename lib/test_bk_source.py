from time import sleep, time
from instruments import bk_source

BK_ADDRESS = 'USB0::0xFFFF::0x7749::526E19103::0::INSTR'  # IP connection does not work using USB instead


    
bk_instrument = bk_source(BK_ADDRESS)


bk_instrument.read_magnitude('voltage')
bk_instrument.read_magnitude('current')
bk_instrument.read_magnitude('power')

quit()


bk_instrument.output_state('OFF')
sleep(.5)

bk_instrument.recall_default()


print(bk_instrument.set_current_limit(30))


#print(bk_instrument.set_ac_voltage(1))

print(bk_instrument.set_dc_voltage(0.1))

bk_instrument.output_state('ON')



