"""
# Driver to use with TC-08 Picolog from nl5
@author: EP
@date: 2020.04.27
@TODO:
"""
# for dependencies install instructions go to:
# https://github.com/picotech/picosdk-python-wrappers
# install the sdk from https://www.picotech.com/downloads
# Please take care to match the "bitness" of your python to the PicoSDK.

# git clone https://github.com/picotech/picosdk-python-wrappers.git
# cd picosdk-python-wrappers
# pip install -r requirements.txt
# python setup.py install

# good example https://github.com/bankrasrg/usbtc08/blob/master/source/usbtc08_logger.py
import ctypes
import logging
import os

if os.name == 'nt':
    tc08 = ctypes.windll.LoadLibrary('../aux_files/usbtc08.dll')
elif os.name == 'posix':
    tc08 = ctypes.CDLL('../aux_files/libusbtc08.so')
    

#from picosdk.functions import assert_pico2000_ok
#from picosdk.usbtc08 import usbtc08 as tc08


def assert_pico2000_ok(status):
    """
        assert_pico_ok(
                        status
                       )
    """
    # checks for PICO_OK status return
    if status > 0:
        errorCheck = True
    else:
        errorCheck = False
        raise Exception("Unsuccessful API call")


def make_enum(members):
    """All C enums with no specific values follow the pattern 0, 1, 2... in the order they are in source."""
    enum = {}
    for i, member in enumerate(members):
        keys = [member]
        if isinstance(member, tuple):
            # this member has multiple names!
            keys = member
        for key in keys:
            enum[key] = i
    return enum

USBTC08_UNITS = make_enum([
    "USBTC08_UNITS_CENTIGRADE",
    "USBTC08_UNITS_FAHRENHEIT",
    "USBTC08_UNITS_KELVIN",
    "USBTC08_UNITS_RANKINE",
])


class cmp_driver:
    def __init__(self, params):
               
        self.drv_sn = params['sn']
        
        # initalization function
        logging.debug('Executing Picolog init code.')
        # Create chandle and status ready for us
        status = {}
        chandle = []
        wrong_handle = []
        handles = {}
        try:
            # Try to open unit by serial number
            for attempt in range(5): # I dont spect more than 5 units connected
                for idx in range(5):
                
                    status["open_unit"] = tc08.usb_tc08_open_unit()
                    #assert_pico2000_ok(status["open_unit"])
                    chandle = status["open_unit"]

                    if chandle == 0:
                        continue 

                    ''' data structure
                    [("size", c_int16),
                    ("DriverVersion", c_int8),
                    ("PicoppVersion", c_int16),
                    ("HardwareVersion", c_int16),
                    ("Variant", c_int16),
                    ("szSerial[USBTC08_MAX_SERIAL_CHAR}", c_int8),
                    ("szCalDate[USBTC08_MAX_DATE_CHARS]", c_int8)]
                    '''
    
                    buffer_length = 20
                    string_buffer = (ctypes.c_int8 * buffer_length)()
                
                    line_number = 4
                    status["get_sn"] = tc08.usb_tc08_get_unit_info2(chandle, string_buffer, buffer_length, line_number)
                    #assert_pico2000_ok(status["get_sn"])
    
                    found_sn = ''.join(chr(i) for i in string_buffer[0:status["get_sn"]])
            
                    
                    handles.update({found_sn : status["open_unit"]})

            
            for handle in handles.keys():
                if handle == self.drv_sn:
                    chandle = handles[handle]
                else:
                    # close other units before going on
                    tc08.usb_tc08_close_unit(handles[handle])
            '''
            else:
                # Close the wrong unit
                logging.error('Coun\'t find picolog device with SN: %s, found this instead: %s' % (self.drv_sn, found_sn))
                return
            '''
            # set mains rejection to 50 Hz
            status["set_mains"] = tc08.usb_tc08_set_mains(chandle,0)
            assert_pico2000_ok(status["set_mains"])
    
    
            # set up channel
            # therocouples types and int8 equivalent
            # B=66 , E=69 , J=74 , K=75 , N=78 , R=82 , S=83 , T=84 , ' '=32 , X=88 
            for ch_idx in range (1, 9):
                typeK = ctypes.c_int8(75)
                status["set_channel"] = tc08.usb_tc08_set_channel(chandle, ch_idx, typeK)
                assert_pico2000_ok(status["set_channel"])
            
            # get minimum sampling interval in ms
            status["get_minimum_interval_ms"] = tc08.usb_tc08_get_minimum_interval_ms(chandle)
            assert_pico2000_ok(status["get_minimum_interval_ms"])
            
            
            self.chandle = chandle
            self.status = status
        except Exception as error:
            print(error)
            logging.error('Can\'t load picolog driver, check if connected.')        

        
    def update_input(self, input_port):
        # picolog has no inputs
        ''' {cmp_name:{'xml':cmp_xml,
                      'value':value,
                      'node': cmp_node,
                      'params': params

                    }
            }
        '''
        pass
            
    def update_output(self, output_port):
        # picolog read temperature
        ''' {cmp_name:{'xml':cmp_xml,
                      'value':value,
                      'node': cmp_node,
                      'params': params
                      }
            }
        '''       
        port_number = int(output_port['params'][0])
        
        logging.debug('Executing port update code.')
             
        port_temp = self.channel_temp[port_number]
        
        logging.debug("Picolog port %d temp: %.2f" % (port_number, port_temp)) 
        return '{:.2f}'.format(port_temp) 
    
    def update_step(self):
        logging.debug('Executing Picolog step code.')
            
        for attempt in range(3): # Retry
            try:
                chandle = self.chandle
                status = self.status
                # get single temperature reading
                temp = (ctypes.c_float * 9)()
                overflow = ctypes.c_int16(0)
                units = USBTC08_UNITS["USBTC08_UNITS_CENTIGRADE"]
                status["get_single"] = tc08.usb_tc08_get_single(chandle, ctypes.byref(temp), ctypes.byref(overflow), units)
                assert_pico2000_ok(self.status["get_single"])


                self.channel_temp = temp

            except Exception as error:
                # we fail an attemp - wait or print error and retry
                # reload driver
                self.__init__(self.drv_sn)
            else:
                # Every thing ok, continue
                break
        else:
            # we failed all the attempts - deal with the consequences.
            logging.error('Can\'t read picolog, check if connected.')        

        
    def __del__(self):
        # close unit
        logging.debug('Executing Picolog delete code.')
        try:
            chandle = self.chandle
            status = self.status
            status["close_unit"] = tc08.usb_tc08_close_unit(chandle)
            assert_pico2000_ok(status["close_unit"]) 
        except:
            pass

        
        
        
        
        
        
        
        