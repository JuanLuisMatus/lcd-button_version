# Instruments and communication functions and classes
from serial.tools import list_ports
from pyvisa import ResourceManager, Error
from time import sleep
from datetime import datetime
import os
import logging  # fixme: implement logging 

class keysight:
    def __init__(self, VISA_ADDRESS):
        self.VISA_ADDRESS = VISA_ADDRESS
        
        # Connect to Keysight instrument
        try:
            # Create a connection (session) to the instrument
            self.resourceManager = ResourceManager()
            self.session = self.resourceManager.open_resource(self.VISA_ADDRESS)
            self.session.timeout = 15000
            self.session.clear()
            
        except Error as error:
            print(error)
            raise Exception('Couldn\'t connect to \'%s\', exiting now...' % self.VISA_ADDRESS)
            
        # For Serial and TCP/IP socket connections enable the read Termination Character, or read's will timeout
        if self.session.resource_name.startswith('ASRL') or self.session.resource_name.endswith('SOCKET'):
            self.session.read_termination = '\n'
        
        # Send *IDN? and read the response
        response = self.session.query('*IDN?')
        
        print('*IDN? returned: %s' % response.rstrip('\n'))
    
    def save_capture(self, file_suffix = ''):
        # Download PNG capture from osciloscope.
        session = self.session
        
        # Disable inksaving format
        session.write(":HARDcopy:INKSaver OFF")
        session.write(":STOP")
        response = session.query_binary_values(":DISPlay:DATA? PNG, COLor", datatype='s')[0]
        session.write(":RUN")

        # Save display data values to file.
        now = datetime.now() # current date and time
        file_name = 'captures/' + now.strftime("%Y%m%dT%H%M%S") + file_suffix + '.png'
    
        if not os.path.exists(os.path.dirname(file_name)):
            os.makedirs(os.path.dirname(file_name))

    
        file = open(file_name, "wb+")
        file.write(response)
        file.close()
        print("MSG: PNG image written to: %s"% file_name)
      
    def __delete__(self):
        self.resourceManager.close()

class bk_source:
    def __init__(self, VISA_ADDRESS):
        # Comfigure Yokogawa remote programming

        self.VISA_ADDRESS = VISA_ADDRESS
       
        # Connect to Yokogawa instrument
        try:
            # Create a connection (session) to the instrument
            self.resourceManager = ResourceManager()
            self.session = self.resourceManager.open_resource(self.VISA_ADDRESS)
        except Error as error:
            print(error)
            raise Exception('Couldn\'t connect to \'%s\', exiting now...' % self.VISA_ADDRESS)
            
        # For Serial and TCP/IP socket connections enable the read Termination Character, or read's will timeout
        if self.session.resource_name.startswith('ASRL') or self.session.resource_name.endswith('SOCKET'):
            self.session.read_termination = '\n'
        
        # Send *IDN? and read the response
        response = self.session.query('*IDN?')
        
        print('MSG BK: *IDN? returned: %s' % response.rstrip('\n').rstrip('\t'))

    def __delete__(self):
        print('Instrument released.')
        self.resourceManager.close()
        
    def send_cmd(self, command):
        # Send a command to the equipment
        for attempt in range(5): # Retry
            try:
                response = self.session.write(command)
            except Exception as error:
                # we fail an attemp - wait or print error and retry
                raise Exception(error)
                
            else:
                # Everythig ok, continue
                print('MSG BK: CMD %s executed successfuly' % command)
                break 
        else:
            # we failed all the attempts - deal with the consequences.
            raise Exception("ERR")
        
        return response
        

    def recall_default(self):
        # Recall the default settings for the machine.
        command = 'SYStem:RECall:DEFault'
        response = self.session.write(command)
        
        if response[1].success == 0:
            print('MSG BK: %s successful' % command)
        else:
            print('ERR BK: %s failed' % command)
        
        return response
        
    def read_errors(self):
        # Query for equipment errors

        command = 'SYSTem:ERRor?'
        response = self.session.write(command)
    
        if response[1].success == 0:
            print('MSG BK: %s successful' % command)
            response = self.session.read()
            print('MSG BK: Returned error: %s' % response)            
        else:
            print('ERR BK: %s failed' % command)
        
        return response

    def set_current_limit(self, current_limit = 0.0):
        # Set or query the current limit in amps.
        command = ('OUTPut:LIMit:CURRent %.3f' % current_limit)
        response = self.session.write(command)
        
        if response[1].success == 0:
            print('MSG BK: %s successful' % command)
        else:
            print('ERR BK: %s failed' % command)
        
        return response

    def set_dc_voltage(self, dc_voltage = 0.0):
        # Set or query the current limit in amps.

        command = ("CONFigure:COUPling %s" % 'DC')
        response = self.session.write(command)
        
        if response[1].success == 0:
            print('MSG BK: %s successful' % command)
        else:
            raise Exception('ERR BK: %s failed' % command)
        
        
        command = ('VOLTage:DC %.3f' % dc_voltage)
        response = self.session.write(command)
        
        if response[1].success == 0:
            print('MSG BK: %s successful' % command)
        else:
            raise Exception('ERR BK: %s failed' % command)
        
        return response

    def set_ac_voltage(self, ac_voltage = 0.0):

        # Set or query the current limit in amps.
        command = ("CONFigure:COUPling %s" % 'AC')
        response = self.session.write(command)
        
        if response[1].success == 0:
            print('MSG: %s successful' % command)
        else:
            raise Exception('ERR: %s failed' % command)
                
        sleep(1)
        command = ('VOLTage:AC %.3f' % ac_voltage)
        response = self.session.write(command)
        
        if response[1].success == 0:
            print('MSG BK: %s successful' % command)
        else:
            raise Exception('ERR BK: %s failed' % command)
        
        return response

    def set_output_current_delay(self, delay = 10000):

        # Set or query the current limit in amps.
        command = ("OUTPut:LIMit:CURRent:DELay %d" % delay)
        response = self.session.write(command)
         
        if response[1].success == 0:
            print('MSG BK: %s successful' % command)
        else:
            raise Exception('ERR BK: %s failed' % command)
        
        return response


    def set_ac_freq(self, frequency = 50.0):
        # Set or query the current limit in amps.
        command = ("FREQuency %.2f" % frequency)
        response = self.session.write(command)
        
        if response[1].success == 0:
            print('MSG BK: %s successful' % command)
        else:
            raise Exception('ERR BK: %s failed' % command)
        
        return response
    

    def output_state(self, state = 'OFF'):
        # Turn the OUTPut on or off, or query the present state.
        if state not in ['OFF', 'ON']:
            raise Exception('ERR: Output state must be ON or OFF')
            
        command = ('OUTPut:STATe %s' % state)
        response = self.session.write(command)
        
        if response[1].success == 0:
            print('MSG BK: %s successful' % command)
        else:
            raise Exception('ERR BK: %s failed' % command)

    def read_magnitude(self, VARIABLE, instantaneous = False):
        if VARIABLE == 'voltage':
            command = ('FETCh:VOLTage:AC?')
            measurement_tol = 0.005

        elif VARIABLE == 'current':
            command = ('FETCh:CURRent:AC?')
            measurement_tol = 0.005

        elif VARIABLE == 'power':
            command = ('FETCh:POWer:AC:REAL?')
            measurement_tol = 0.03

        else:
            raise Exception('ERR BK: Measured variables must be voltage, current or power.')
            
        for attempt in range(5): # Retry
            try:

                self.session.write(command)
                magnitude_1 = float(self.session.read())

                sleep(1) # Wait some time
        
                # Read another measurement
                self.session.write(command)
                magnitude_2 = float(self.session.read())

                # Check if variables are in range
                if (abs(magnitude_1 - magnitude_2) <= abs(measurement_tol*max(magnitude_1, magnitude_2))) or \
                    instantaneous == True:
                    pass
                else:
                    raise Exception('MSG BK: %s are not stable, wating... try #: %s'% (VARIABLE, attempt))
                    
            except Exception as error:
                # we fail an attemp - wait or print error and retry
                print(error)
                sleep(1)
                
            else:
                # Everythig ok, continue
                magnitude = magnitude_1
                print('MSG BK: BK %s: %.3f' % (VARIABLE, magnitude))
                break
        else:
            # we failed all the attempts - deal with the consequences.
            raise Exception("ERR BK: Couldn't read %s." % VARIABLE)
        
        return magnitude

class bk_load:
    def __init__(self, VISA_ADDRESS):
        # Comfigure Yokogawa remote programming

        self.VISA_ADDRESS = VISA_ADDRESS
       
        # Connect to Yokogawa instrument
        try:
            # Create a connection (session) to the instrument
            self.resourceManager = ResourceManager()
            self.session = self.resourceManager.open_resource(self.VISA_ADDRESS)
        except Error as error:
            print(error)
            raise Exception('Couldn\'t connect to \'%s\', exiting now...' % self.VISA_ADDRESS)
            
        # For Serial and TCP/IP socket connections enable the read Termination Character, or read's will timeout
        if self.session.resource_name.startswith('ASRL') or self.session.resource_name.endswith('SOCKET'):
            self.session.read_termination = '\n'
        
        # Send *IDN? and read the response
        response = self.session.query('*IDN?')
        
        print('*IDN? returned: %s' % response.rstrip('\n'))

    def __delete__(self):
        print('Instrument released.')
        self.resourceManager.close()
        
    def send_cmd(self, command):
        # Send a command to the equipment
        for attempt in range(5): # Retry
            try:
                response = self.session.write(command)
            except Exception as error:
                # we fail an attemp - wait or print error and retry
                raise Exception(error)
                
            else:
                # Everythig ok, continue
                print('MSG: CMD %s executed successfuly' % command)
                break 
        else:
            # we failed all the attempts - deal with the consequences.
            raise Exception("ERR")
        
        return response
        

    def recall_default(self):
        # Recall the default settings for the machine.
        command = '*RST'
        response = self.session.write(command)
        
        if response[1].success == 0:
            print('MSG: %s successful' % command)
        else:
            print('ERR: %s failed' % command)
        
        return response


    def read_errors(self):
        # Query for equipment errors

        command = 'SYSTem:ERRor?'
        response = self.session.write(command)
    
        if response[1].success == 0:
            print('MSG: %s successful' % command)
            response = self.session.read()
            print('MSG: Returned error: %s' % response)            
        else:
            print('ERR: %s failed' % command)
        
        return response


    def clear_errors(self):
        # Clear all equipment errors
        command = 'SYSTem:CLEar'
        response = self.session.write(command)
    
        if response[1].success == 0:
            print('MSG: %s successful' % command)       
        else:
            print('ERR: %s failed' % command)
        
        return response
    
    
    def source_mode(self, mode = 'VOLT'):
        # This command is used to set the working mode of the power supply.
        if mode not in ['VOLT', 'CURR']:
            raise Exception('ERR: Output state must be VOLT or CURR')
            
        command = ('FUNCtion %s' % mode)
        response = self.session.write(command)
        
        if response[1].success == 0:
            print('MSG: %s successful' % command)
        else:
            raise Exception('ERR: %s failed' % command) 
    

    def set_remote(self):
        # Set the source in remote mode       
        
        command = ('SYSTem:REMote')
        response = self.session.write(command)
        
        if response[1].success == 0:
            print('MSG: %s successful' % command)
        else:
            raise Exception('ERR: %s failed' % command)
        
        return response


    def set_voltage(self, dc_voltage = 0.0):
        # Set the current limit in amps in CV mode.
        
        command = ('VOLTage %.3f' % dc_voltage)
        response = self.session.write(command)
        
        if response[1].success == 0:
            print('MSG: %s successful' % command)
        else:
            raise Exception('ERR: %s failed' % command)
        
        return response

    def set_current_limit_pos(self, current_pos = 0.0):
        # Set the current limit in amps in CV mode.
        command = ('VOLTage:HIGH %.3f' % current_pos)
        response = self.session.write(command)
        
        if response[1].success == 0:
            print('MSG: %s successful' % command)
        else:
            print('ERR: %s failed' % command)

        return response
        

    def set_current_limit_neg(self, current_neg = 0.0):
        # Set the current limit in amps in CV mode.
        command = ('VOLTage:LOW %.3f' % current_neg)
        response = self.session.write(command)
        
        if response[1].success == 0:
            print('MSG: %s successful' % command)
        else:
            print('ERR: %s failed' % command)

        return response

    def set_current(self, current_neg = 0.0):
        # Set the current limit in amps in CC mode.
        command = ('CURRent %.3f' % current_neg)
        response = self.session.write(command)
        
        if response[1].success == 0:
            print('MSG: %s successful' % command)
        else:
            print('ERR: %s failed' % command)

        return response
    
    
    def set_voltage_limit_pos(self, voltage_pos = 0.0):
        # Set the voltage limit in amps in CC mode.
        command = ('CURRent:HIGH %.3f' % voltage_pos)
        response = self.session.write(command)
        
        if response[1].success == 0:
            print('MSG: %s successful' % command)
        else:
            print('ERR: %s failed' % command)

        return response


    def set_voltage_limit_neg(self, voltage_neg = 0.0):
        # Set the voltage limit in amps in CC mode.
        command = ('CURRent:LOW %.3f' % voltage_neg)
        response = self.session.write(command)
        
        if response[1].success == 0:
            print('MSG ITECH: %s successful' % command)
        else:
            print('ERR ITECH: %s failed' % command)

        return response

    def output_state(self, state = 'OFF'):
        # Turn the OUTPut on or off, or query the present state.
        if state not in ['OFF', 'ON']:
            raise Exception('ERR: Output state must be ON or OFF')
            
        command = ('INPut %s' % state)
        response = self.session.write(command)
        
        if response[1].success == 0:
            print('MSG ITECH: %s successful' % command)
        else:
            raise Exception('ERR ITECH: %s failed' % command)


    def read_voltage(self):
        # Set or query the current limit in amps.        
        
        command = ('FETCh:VOLTage?')
        response = self.session.write(command)
        
        if response[1].success == 0:
            print('MSG ITECH: %s successful' % command)
            response = float(self.session.read())
            print('MSG ITECH: ITECH Voltage: %.3f' % response)  
        else:
            raise Exception('ERR ITECH: %s failed' % command)
        
        return response

    def read_current(self):
        # Set or query the current limit in amps.        
        
        command = ('FETCh:CURRent?')
        response = self.session.write(command)
        
        if response[1].success == 0:
            print('MSG ITECH: %s successful' % command)
            response = float(self.session.read())
            print('MSG ITECH: Current: %.3f' % response)  
        else:
            raise Exception('ERR: %s failed' % command)
        
        return response

    def read_power(self):
        # Set or query the current limit in amps.        
        
        command = ('FETCh:POWer?')
        response = self.session.write(command)
        
        if response[1].success == 0:
            print('MSG ITECH: %s successful' % command)
            response = float(self.session.read())
            print('MSG ITECH: Power: %.3f' % response)  
        else:
            raise Exception('ERR ITECH: %s failed' % command) 
    
class itech_source:
    def __init__(self, VISA_ADDRESS):
        # Comfigure Yokogawa remote programming

        self.VISA_ADDRESS = VISA_ADDRESS
       
        # Connect to Yokogawa instrument
        try:
            # Create a connection (session) to the instrument
            self.resourceManager = ResourceManager()
            self.session = self.resourceManager.open_resource(self.VISA_ADDRESS)
        except Error as error:
            print(error)
            raise Exception('Couldn\'t connect to \'%s\', exiting now...' % self.VISA_ADDRESS)
            
        # For Serial and TCP/IP socket connections enable the read Termination Character, or read's will timeout
        if self.session.resource_name.startswith('ASRL') or self.session.resource_name.endswith('SOCKET'):
            self.session.read_termination = '\n'
        
        # Send *IDN? and read the response
        response = self.session.query('*IDN?')
        
        print('MSG ITECH: *IDN? returned: %s' % response.rstrip('\n'))

    def __delete__(self):
        print('MSG ITECH: Instrument released.')
        self.resourceManager.close()
        
    def send_cmd(self, command):
        # Send a command to the equipment
        for attempt in range(5): # Retry
            try:
                response = self.session.write(command)
            except Exception as error:
                # we fail an attemp - wait or print error and retry
                raise Exception(error)
                
            else:
                # Everythig ok, continue
                print('MSG ITECH: CMD %s executed successfuly' % command)
                break 
        else:
            # we failed all the attempts - deal with the consequences.
            raise Exception("ERR")
        
        return response
        

    def recall_default(self):
        # Recall the default settings for the machine.
        command = '*RST'
        response = self.session.write(command)
        
        if response[1].success == 0:
            print('MSG ITECH: %s successful' % command)
        else:
            print('ERR ITECH: %s failed' % command)
        
        return response
    
    def read_errors(self):
        # Query for equipment errors

        command = 'SYSTem:ERRor?'
        response = self.session.write(command)
    
        if response[1].success == 0:
            print('MSG: %s successful' % command)
            response = self.session.read()
            print('MSG ITECH: Returned error: %s' % response)            
        else:
            print('ERR ITECH: %s failed' % command)
        
        return response


    def clear_errors(self):
        # Clear all equipment errors
        command = 'SYSTem:CLEar'
        response = self.session.write(command)
    
        if response[1].success == 0:
            print('MSG: %s successful' % command)
            response = self.session.read()
            print('MSG ITECH: %s successful' % command)
        else:
            print('ERR ITECH: %s failed' % command)
        
        return response


    def source_mode(self, mode = 'VOLT'):
        # This command is used to set the working mode of the power supply.
        if mode not in ['VOLT', 'CURR']:
            raise Exception('ERR: Output state must be ON or OFF')
            
        command = ('FUNCtion %s' % mode)
        response = self.session.write(command)
        
        if response[1].success == 0:
            print('MSG ITECH: %s successful' % command)
        else:
            raise Exception('ERR ITECH: %s failed' % command)


    def set_remote(self):
        # Set the source in remote mode       
        
        command = ('SYSTem:REMote')
        response = self.session.write(command)
        
        if response[1].success == 0:
            print('MSG ITECH: %s successful' % command)
        else:
            raise Exception('ERR ITECH: %s failed' % command)
        
        return response


    def set_voltage(self, dc_voltage = 0.0):
        # Set the current limit in amps in CV mode.
        
        command = ('VOLT %.3f' % dc_voltage)
        response = self.session.write(command)
        
        if response[1].success == 0:
            print('MSG ITECH: %s successful' % command)
        else:
            raise Exception('ERR ITECH: %s failed' % command)
        
        return response

        
    def set_current_limit_pos(self, current_pos = 0.0):
        # Set the current limit in amps in CV mode.
        command = ('CURRent:LIMit:POSitive %.3f' % current_pos)
        response = self.session.write(command)
        
        if response[1].success == 0:
            print('MSG ITECH: %s successful' % command)
        else:
            print('ERR ITECH: %s failed' % command)

        return response
        

    def set_current_limit_neg(self, current_neg = 0.0):
        # Set the current limit in amps in CV mode.
        command = ('CURRent:LIMit:NEGative %.3f' % current_neg)
        response = self.session.write(command)
        
        if response[1].success == 0:
            print('MSG ITECH: %s successful' % command)
        else:
            print('ERR ITECH: %s failed' % command)

        return response
    
    
    def set_current(self, current_neg = 0.0):
        # Set the current limit in amps in CC mode.
        command = ('CURRent %.3f' % current_neg)
        response = self.session.write(command)
        
        if response[1].success == 0:
            print('MSG ITECH: %s successful' % command)
        else:
            print('ERR ITECH: %s failed' % command)

        return response
    
    
    def set_voltage_limit_pos(self, voltage_pos = 0.0):
        # Set the voltage limit in amps in CC mode.
        command = ('VOLTage:LIMit:POSitive %.3f' % voltage_pos)
        response = self.session.write(command)
        
        if response[1].success == 0:
            print('MSG ITECH: %s successful' % command)
        else:
            print('ERR ITECH: %s failed' % command)

        return response


    def set_voltage_limit_neg(self, voltage_neg = 0.0):
        # Set the voltage limit in amps in CC mode.
        command = ('VOLTage:LIMit:NEGative %.3f' % voltage_neg)
        response = self.session.write(command)
        
        if response[1].success == 0:
            print('MSG ITECH: %s successful' % command)
        else:
            print('ERR ITECH: %s failed' % command)

        return response
    
    
    def output_state(self, state = 'OFF'):
        # Turn the OUTPut on or off, or query the present state.
        if state not in ['OFF', 'ON']:
            raise Exception('ERR: Output state must be ON or OFF')
            
        command = ('OUTPut:STATe %s' % state)
        response = self.session.write(command)
        
        if response[1].success == 0:
            print('MSG ITECH: %s successful' % command)
        else:
            raise Exception('ERR ITECH: %s failed' % command)

    def read_magnitude(self, VARIABLE, instantaneous = False):
        if VARIABLE == 'voltage':
            command = ('FETCh:VOLTage?')

        elif VARIABLE == 'current':
            command = ('FETCh:CURRent?')

        elif VARIABLE == 'power':
            command = ('FETCh:POWer?')
        else:
            raise Exception('ERR: Measured variables must be voltage, current or power.')
            
        for attempt in range(5): # Retry
            try:

                self.session.write(command)
                magnitude_1 = float(self.session.read())

                sleep(1) # Wait some time
        
                # Read another measurement
                self.session.write(command)
                magnitude_2 = float(self.session.read())

                # Check if variables are in range
                if (abs(magnitude_1 - magnitude_2) <= abs(0.01*max(magnitude_1, magnitude_2))) or \
                    instantaneous == True:
                    pass
                else:
                    pass
                    
                    raise Exception('MSG ITECH: %s are not stable, wating... try #: %s'% (VARIABLE, attempt))
                    
            except Exception as error:
                # we fail an attemp - wait or print error and retry
                print(error)
                sleep(1)
                
            else:
                # Everythig ok, continue
                magnitude = magnitude_1
                print('MSG ITECH: %s: %.3f' % (VARIABLE, magnitude))
                break
        else:
            # we failed all the attempts - deal with the consequences.
            raise Exception("ERR ITECH: Couldn't read %s." % VARIABLE)
        
        return magnitude


class yokogawa:
    def __init__(self, VISA_ADDRESS, GRID_ELEMENT, BATT_ELEMENT):
        # Comfigure Yokogawa remote programming

        self.VISA_ADDRESS = VISA_ADDRESS
        self.GRID_ELEMENT = GRID_ELEMENT
        self.BATT_ELEMENT = BATT_ELEMENT
        

        # Connect to Yokogawa instrument
        try:
            # Create a connection (session) to the instrument
            self.resourceManager = ResourceManager()
            self.session = self.resourceManager.open_resource(self.VISA_ADDRESS)
        except Error as error:
            print(error)
            
        # For Serial and TCP/IP socket connections enable the read Termination Character, or read's will timeout
        if self.session.resource_name.startswith('ASRL') or self.session.resource_name.endswith('SOCKET'):
            self.session.read_termination = '\n'
        
        # Send *IDN? and read the response
        response = self.session.query('*IDN?')
        
        print('*IDN? returned: %s' % response.rstrip('\n'))
        sleep(2)

    def __delete__(self):
        print('Instrument released.')
        self.resourceManager.close()

    def read_voltages(self):  #TODO: read individual elements
        session = self.session
        
        for attempt in range(5): # Retry
            try:
                # Set data format
                COMMAND = ':NUMERIC:FORMAT ASCII'
                session.write(COMMAND) 
        
                # Setting the items to be read
                # Send Command
                COMMAND = ':NUMeric:NORMal:ITEM1 URMS,%s' % self.GRID_ELEMENT
                session.write(COMMAND)
        
                # Send Command
                COMMAND = ':NUMeric:NORMal:ITEM2 URMS,%s' % self.BATT_ELEMENT
                session.write(COMMAND)
        
                # Read one measurement
                COMMAND = ':NUMERIC:NORMal:VALUE? 1'
                VGRID_1 = float(session.query(COMMAND))
        
                COMMAND = ':NUMERIC:NORMal:VALUE? 2'
                VBATT_1 = float(session.query(COMMAND))

                sleep(1) # Wait some time
        
                # Read another measurement
                COMMAND = ':NUMERIC:NORMal:VALUE? 1'
                VGRID_2 = float(session.query(COMMAND)) # Send Command
        
                COMMAND = ':NUMERIC:NORMal:VALUE? 2'
                VBATT_2 = float(session.query(COMMAND))

                # Check if variables are in range
                if abs(VGRID_1 - VGRID_2) <= (0.001*max(VGRID_1, VGRID_2)) and \
                   abs(VBATT_1 - VBATT_2) <= (0.001*max(VBATT_1, VBATT_2)): # Is within .1%
                    pass
                else:
                    print('MSG: Voltage are not stable, wating... try #: %s'% attempt)
                    # print('VGRID: %s' % VGRID_1)
                    # print('VBATT: %s' % VBATT_1)
                    
            except Exception as error:
                # we fail an attemp - wait or print error and retry
                raise Exception(error)
                sleep(1)
                
            else:
                # Everythig ok, continue
                VGRID = VGRID_1
                VBATT = VBATT_1
                break
        else:
            # we failed all the attempts - deal with the consequences.
            raise Exception("ERR: Couldn't read voltages.")
        
        return VGRID, VBATT

    def read_currents(self):
        session = self.session
        
        for attempt in range(5): # Retry
            try:
                # Set data format
                COMMAND = ':NUMERIC:FORMAT ASCII'
                session.write(COMMAND) 
                
                # Setting the items to be read
                # Send Command
                COMMAND = ':NUMeric:NORMal:ITEM3 IRMS,%s' % self.GRID_ELEMENT
                session.write(COMMAND)
        
                # Send Command
                COMMAND = ':NUMeric:NORMal:ITEM4 IRMS,%s' % self.BATT_ELEMENT
                session.write(COMMAND)
        
                # Send Command
                COMMAND = ':NUMERIC:NORMal:VALUE? 3'
                IGRID_1 = float(session.query(COMMAND))
        
                # Send Command
                COMMAND = ':NUMERIC:NORMal:VALUE? 4'
                IBATT_1 = float(session.query(COMMAND))
        
                sleep(1) # Wait some time
        
                # Read another measurement
                COMMAND = ':NUMERIC:NORMal:VALUE? 3'
                IGRID_2 = float(session.query(COMMAND))
        
                # Send Command
                COMMAND = ':NUMERIC:NORMal:VALUE? 4'
                IBATT_2 = float(session.query(COMMAND))
    

                if abs(IGRID_1 - IGRID_2) <= (0.001*max(IGRID_1, IGRID_2)) and \
                   abs(IBATT_1 - IBATT_2) <= (0.001*max(IBATT_1, IBATT_2)): # Is within .1%
                    pass
                else:
                    print('MSG: Currents are not stable, wating... try #: %s'% attempt)
                    # print('IGRID: %s' % IGRID_1)
                    # print('IBATT: %s' % IBATT_1)
                    
                    
            except Exception as error:
                # we fail an attemp - wait or print error and retry
                raise Exception(error)
                sleep(1)
                
            else:
                # Everythig ok, continue
                IGRID = IGRID_1
                IBATT = IBATT_1
                break
        else:
            # we failed all the attempts - deal with the consequences.
            raise Exception("ERR: Couldn't read currents.")

        return IGRID, IBATT

    def read_powers(self):
        session = self.session
        
        for attempt in range(5): # Retry
            try:
                # Set data format
                COMMAND = ':NUMERIC:FORMAT ASCII'
                session.write(COMMAND) 
                
                # Setting the items to be read
                # Send Command
                COMMAND = ':NUMeric:NORMal:ITEM3 P,%s' % self.GRID_ELEMENT
                session.write(COMMAND)
        
                # Send Command
                COMMAND = ':NUMeric:NORMal:ITEM4 P,%s' % self.BATT_ELEMENT
                session.write(COMMAND)
        
                # Send Command
                COMMAND = ':NUMERIC:NORMal:VALUE? 3'
                PGRID_1 = float(session.query(COMMAND))
        
                # Send Command
                COMMAND = ':NUMERIC:NORMal:VALUE? 4'
                PBATT_1 = float(session.query(COMMAND))
        
                sleep(1) # Wait some time
        
                # Read another measurement
                COMMAND = ':NUMERIC:NORMal:VALUE? 3'
                PGRID_2 = float(session.query(COMMAND))
        
                # Send Command
                COMMAND = ':NUMERIC:NORMal:VALUE? 4'
                PBATT_2 = float(session.query(COMMAND))
    

                if abs(PGRID_1 - PGRID_2) <= (0.01*max(PGRID_1, PGRID_2)) and \
                   abs(PBATT_1 - PBATT_2) <= (0.01*max(PBATT_1, PBATT_2)): # Is within .1%
                    pass
                else:
                    print('MSG: Currents are not stable, wating... try #: %s'% attempt)
                    # print('IGRID: %s' % IGRID_1)
                    # print('IBATT: %s' % IBATT_1)
                    
                    
            except Exception as error:
                # we fail an attemp - wait or print error and retry
                raise Exception(error)
                sleep(1)
                
            else:
                # Everythig ok, continue
                PGRID = PGRID_1
                PBATT = PBATT_1
                break
        else:
            # we failed all the attempts - deal with the consequences.
            raise Exception("ERR: Couldn't read currents.")

        return PGRID, PBATT

    def read_element(self, function):
        session = self.session
        
        for attempt in range(5): # Retry
            try:
                # Set data format
                COMMAND = ':NUMERIC:FORMAT ASCII'
                session.write(COMMAND) 
                
                # Setting the items to be read
                # Send Command
                COMMAND = ':NUMeric:NORMal:ITEM1 %s' % function
                session.write(COMMAND)
        
                # Get item value
                COMMAND = ':NUMERIC:NORMal:VALUE? 1'
                value_1 = float(session.query(COMMAND))
                
                #sleep(.2)
                
                # Get item value
                #COMMAND = ':NUMERIC:NORMal:VALUE? 1'
                #value_2 = float(session.query(COMMAND))
                value_2 = value_1

                # check if value is stable

                if abs(value_1 - value_2) <= abs(0.01*max(value_1, value_2)):
                    break
                else:
                    print('Yokogawa %s read is not stable, waiting... try #: %s'% (function, attempt))
                    sleep(1)
                    
            except Exception as error:
                # we fail an attemp - wait or print error and retry
                print(error)
                
        else:
            # we failed all the attempts - deal with the consequences.
            print("Yokokogawa can\'t read %s." % function)
            value_1 = 0

        return value_1


    def test_connection(self):
        session = self.session
        
        try:
            response = self.session.query('*IDN?')                    
            
        except Exception as error:
            # we fail an attemp - wait or print error and retry
            # reconect yokogawa
            self.__delete__()
            self.__init__(self.VISA_ADDRESS, self.GRID_ELEMENT, self.BATT_ELEMENT)


def detect_coms():
    """Detects the available COM ports."""
    print('MSG: Automatic COM port search...')
    ports = list(list_ports.comports())

    com_ports = []
    
    for port in ports:
        if port.description.find('USB Serial Port')>=0 or port.description.find('Silicon Labs CP210x USB')>=0:
            com_ports.append(port.device)
    
    if len(com_ports) == 0:
        raise('ERR: No COM ports detected.')

    else:
        print('MSG: Detected COMs ports: %s'% com_ports)
        return com_ports






