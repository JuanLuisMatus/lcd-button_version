from pyModbusTCP.client import ModbusClient
from serial.tools import list_ports
import minimalmodbus
from datetime import datetime
from time import sleep
import time
import csv
import os

START = 1
STOP = 2
RESET = 4

# Quasar registers
MODBUS_PS_ACTIONS_REGISTER = 20
MODBUS_PS_AC_CURRENT_SETPOINT = 21
MODBUS_PS_DC_CURRENT_SETPOINT = 22
MODBUS_PERCENTAGE_TO_NOMINAL_SETPOINT = 23

MODBUS_OPERATION_MODE = 46

GRID_FREQ_SETTING = 50
MODBUS_SYSTEM_BOOT_STATUS = 201
MODBUS_FIRMWARE_VERSION = 602

MODBUS_VEHICLE_CHARGE_PERMISSION = 141
MODBUS_VEHICLE_PROXIMITY_DETECTION = 142
MODBUS_GUN_STATUS = 147

MODBUS_CHARGER_STATUS = 180
MODBUS_UNRECOVERABLE_ERROR_HI = 205
MODBUS_G99_STOP_ORDER = 149
MODBUS_ROTARY_SWITCH = 152
MODBUS_GUN_SELECTOR = 154

MODBUS_L1_AC_VOLTAGE_RMS = 240
MODBUS_L1_AC_FREQUENCY = 242
MODBUS_L1_AC_CURRENT_RMS = 241
MODBUS_L1_DC_BUS_VOLTAGE = 243
MODBUS_L1_DC_VOLTAGE = 244
MODBUS_L1_DC_CURRENT = 245


MODBUS_L1_PFC_TEMP = 261
MODBUS_L1_PHx_TEMP = 262
MODBUS_L1_KC_LINK_TEMP = 263
MODBUS_L1_TRAFO_TEMP = 264
MODBUS_L1_AMBIENT_TEMP = 265

MODBUS_POWER_DERATING_STATUS = 213

MODBUS_TESTING_MODE_ORDER_LO = 3000
MODBUS_TESTING_MOSFETS_ORDERS = 3002
MODBUS_VEHICLE_CHARGE_PERMISSION = 141
MODBUS_VEHICLE_PROXIMITY_DETECTION = 142
MODBUS_GUN_STATUS = 147

MODBUS_CHARGER_MESSAGES = 181

TESTING_MODE_ORDER_DO_NOTHING        = 0x0000
TESTING_MODE_ORDER_SCALE_MODE        = 0x0001
TESTING_MODE_ORDER_SCALE_MODE_EOL    = 0x0002
TESTING_MODE_ORDER_PWR_DERAT_TUNE    = 0x0004
TESTING_MODE_ORDER_PERIPH_CHECK      = 0x0008
TESTING_MODE_ORDER_DUTY_0            = 0x0010
TESTING_MODE_ORDER_DUTY_25           = 0x0020
TESTING_MODE_ORDER_DUTY_50           = 0x0040
TESTING_MODE_ORDER_DUTY_75           = 0x0080
TESTING_MODE_ORDER_DUTY_100          = 0x0100
TESTING_MODE_PRECHARGE_RLY_CLOSE     = 0x0200
TESTING_MODE_GRD_CONNECT_RLY_CLS     = 0x0400
TESTING_MODE_START_CAN_COMMS_TX      = 0x0800
TESTING_MODE_D1_D2_RLY_CLOSE         = 0x1000
TESTING_MODE_SET_GUN_LOCK            = 0x2000
TESTING_MODE_SET_GUN_RELEASE         = 0x4000
TESTING_MODE_MOSFET_BRIDGES          = 0x8000


TESTING_MODE_MOSFET_CTRL_B1_1A = 0x0001
TESTING_MODE_MOSFET_CTRL_B1_1B = 0x0002
TESTING_MODE_MOSFET_CTRL_B1_2A = 0x0004
TESTING_MODE_MOSFET_CTRL_B1_2B = 0x0008
TESTING_MODE_MOSFET_CTRL_B2_1A = 0x0010
TESTING_MODE_MOSFET_CTRL_B2_1B = 0x0020
TESTING_MODE_MOSFET_CTRL_B2_2A = 0x0040
TESTING_MODE_MOSFET_CTRL_B2_2B = 0x0080
TESTING_MODE_MOSFET_CTRL_PFC_1A = 0x0100
TESTING_MODE_MOSFET_CTRL_PFC_1B = 0x0200 
TESTING_MODE_MOSFET_CTRL_PFC_2A = 0x0400
TESTING_MODE_MOSFET_CTRL_PFC_2B = 0x0800



class quasar:


    def port_id(self):
        print('Automatic COM port search...')
        ports = list(list_ports.comports())

        for port in ports:
            if port.description.find('USB Serial Port')>=0 or port.description.find('Silicon Labs CP210x USB')>=0:
                print('Detected com port: %s'% port.device)
                return port.device
        else:
            sys.exit('ERR: No COM port detected.')
    
    def connect(self):
        # This function tries to establish communication
        # with the charger
        if self.conn_mode == "TCP":
            print('Quasar connected to IP: %s' % self.host_ip)
            self.c = ModbusClient()
            self.c.host(self.host_ip)
            self.c.port(5020)
            self.c.unit_id(10)
            if not self.c.is_open():
                if not self.c.open():
                    self.isOnline = False
                    return
        
            try:
                regs = self.c.read_holding_registers(20, 1)
                if regs == None:
                    raise Exception('Port is not available, or charger is disconnected')  
                self.isOnline = True
                print('Port open, communication established with {:s}'.format(self.name))
            except Exception as error:
                self.isOnline = False
                raise ConnetionError("Unable to connect.Check Quasar connection.")
            
        elif self.conn_mode == "RS485":
            connect_retries = 0
            while connect_retries < 5:
                try:
                    self.c = minimalmodbus.Instrument(self.port,self.node)
                    self.c.serial.baudrate = 9600
                    self.c.serial.bytesize = 8
                    self.c.serial.parity = minimalmodbus.serial.PARITY_NONE
                    self.c.serial.stopbits = 1
                    self.c.serial.timeout = 0.1
                    self.c.serial.address = self.node
                    self.c.serial.mode = minimalmodbus.MODE_RTU
                    self.c.close_port_after_each_call = True
                    self.isOnline = True
                    break
                except:
                    self.isOnline = False
                    connect_retries += 1
            else:
                raise ConnectionError("Unable to connect. Check RS485 cable.")
        
    def __init__(self, *args):
        if args[1] == "TCP":
            self.name = args[0]
            self.host_ip = args[2]
            self.conn_mode = "TCP"
        elif args[1] == "RS485":
            self.name = args[0]
            self.port = self.port_id()
            self.node = args[2]
            self.conn_mode = args[1]
        else:
            raise ValueError("Protocol not supported. Try again with TCP or RS485.")
                        
        self.isOnline = False
        self.__error_flag = False
        self.__error_message = []
        self.__operation_sp_current = STOP
        self.__operation_sp_chademo = STOP
        self.__max_dc_current_sp = 0
        self.__max_ac_current_sp = 0
        self.__power_sp = 0
        self.history_data = []
        self.starting_time = time.time()
        self.__reset_counter = {'Counter' : 0, 'Time'   : 0.0, 'Code': hex(0)}
        self.__working_mode = 'undefined'
        self.SoC = 0
        self.connect()

    def raise_error(self,msg):
        self.__error_flag = True
        self.__error_message.append([msg])
        print(msg)
            
    def read(self, addr, dec, isSigned):
            # Reads one register
            tbr = 0
            readRetries = 0
            if self.isOnline == False: return tbr
            while readRetries < 15:  
                if self.conn_mode == "TCP":          
                    regs = self.c.read_holding_registers(addr, 1)
                    if (regs == None):
                        readRetries += 1
                        time.sleep(0.1)
                    self.isOnline = True
                    time.sleep(0.01)
                    break;
                elif self.conn_mode == "RS485": 
                    try:
                        tbr = self.c.read_register(addr, dec, 3, isSigned)
                        self.isOnline = True
                        time.sleep(0.01)
                        break;
                    except:
                        readRetries += 1
                        time.sleep(0.1)   
            if readRetries >= 15:
                self.isOnline = False
                self.raise_error("Unable to read Modbus {}, {}".format(addr,hex(addr)))
            
            if self.conn_mode == "TCP":
                try:
                    if not isSigned:
                        if dec == 0:
                            tbr = regs[0]
                        else:
                            tbr =  float(regs[0])/pow(10.0,dec)
                    else:
                        if regs[0] & (1 << (16 - 1)):
                            # do complement
                            regs[0] -= 1 << 16 
                        tbr = float(regs[0])/pow(10.0,dec)
                except Exception as error:
                    print(error)
            return tbr

    def write(self, addr, value, dec, isSigned):
        # writes one register
        tbr = 0
        writeRetries = 0
        if self.isOnline == False: return tbr
        if self.conn_mode == "TCP": 
            if isSigned:
                value = int(int(value * pow(10.0,dec)) & 0xffff)
            else:
                value = int(value * pow(10.0,dec))              
        while writeRetries < 15: 
            if self.conn_mode == "TCP":            
                if(self.c.write_single_register(addr, value)):
                    self.isOnline = True
                    time.sleep(0.01)
                    break
                else:
                    writeRetries += 1
                    time.sleep(0.1)                
            elif self.conn_mode == "RS485": 
                try:
                    self.c.write_register(addr, value, dec, 6, isSigned)
                    self.isOnline = True
                    time.sleep(0.1)
                    break;
                except Exception as error:
                    # we fail an attemp - wait or print error and retry
                    print(error)                    
                    writeRetries += 1
                    time.sleep(0.1)
        if writeRetries >= 15:
            self.isOnline = False
            self.raise_error("Unable to write Modbus {}, {}".format(addr,hex(addr)))


    # Procedures    
    def unlock_maintenance(self):
        # Unlocks the charger applying the password 87 66 68 67
        self.write(2000, ord('W'), 0, False)
        self.write(2000, ord('B'), 0, False)
        self.write(2000, ord('D'), 0, False)
        self.write(2000, ord('C'), 0, False)
        if self.read(2001, 0, False) == 1:
            print('{:s} unlocked!'.format(self.name))
        else:
            print('{:s} still locked, check FW version'.format(self.name))
            raise NameError('System was unable to unlock') # Blocks the execution. It is dangerous
    
    def unlock_calibration(self):
        # Puts the charger in calibration mode        
        self.write(2002, ord('C'), 0, False)
        self.write(2002, ord('S'), 0, False)

        sleep(2)
        response = self.read(2003, 0, False)
        if  response == 1:
            print('{:s} calibration unlocked!'.format(self.name))
        else:
            print('{:s} still locked, check FW version'.format(self.name))
            raise NameError('System was unable to unlock calibration') # Blocks the execution. It is dangerous
    
    
    def unlock_test_mode(self):
        # Puts the charger in calibration mode        
        self.write(2008, ord('D'), 0, False)
        self.write(2008, ord('C'), 0, False)
        self.write(2008, ord('T'), 0, False)
        self.write(2008, ord('M'), 0, False)

        sleep(1)
        if self.read(2009, 0, False) == 1:
            print('{:s} test mode unlocked!'.format(self.name))
        else:
            print('{:s} still locked, check FW version'.format(self.name))
            raise NameError('System was unable to unlock test mode') # Blocks the execution. It is dangerous    

        self.test_register = TESTING_MODE_ORDER_DO_NOTHING


    def test_mode_enable(self, command):
        # Enable a test mode function
        self.test_register |= command        
        
        self.write(MODBUS_TESTING_MODE_ORDER_LO, self.test_register, 0, False)

    def test_mode_disable(self, command):
        # Disables a test mode function
        self.test_register &= ~ command
                    
        self.write(MODBUS_TESTING_MODE_ORDER_LO, self.test_register, 0, False)


    def unlock_operator_mode(self):
        # Puts the charger in calibration mode        
        self.write(2006, ord('Q'), 0, False)
        self.write(2006, ord('S'), 0, False)
        self.write(2006, ord('D'), 0, False)
        self.write(2006, ord('C'), 0, False)
        self.write(2006, ord('M'), 0, False)
        self.write(2006, ord('P'), 0, False)

        sleep(1)
        if self.read(2007, 0, False) == 1:
            print('{:s} operator mode unlocked!'.format(self.name))
        else:
            print('{:s} still locked, check FW version'.format(self.name))
            raise NameError('System was unable to unlock operator mode') # Blocks the execution. It is dangerous    
    
    def lock_test_mode(self):
        self.write(2008, 0, 0, False)
        
        sleep(1)
        if self.read(2009, 0, False) == 0:
            print('{:s} test mode locked!'.format(self.name))
        else:
            print('{:s} still unlocked, check FW version'.format(self.name))
            raise NameError('System was unable to lock test mode') # Blocks the execution. It is dangerous        
    
    
    def write_calibration_voltages(self, VGRID, VBATT):
        # Write calibration readings
        self.write(1405, int(VGRID*100), 0, False)
        self.write(1403, int(VBATT*10), 0, False)


        for attempt in range(5): # Retry
            try:
                # Tell the charger VGRID is going to calibrate
                self.write(1401, 16, 0, False)
                sleep(1)
                
                vgrid_calibration_status = self.read(1401, 0, False) # Should return a 0
        
                if 0 != vgrid_calibration_status:
                    raise Exception('MSG: Voltage is calibrating, wating... try #: %s'% attempt)
                    
            except Exception as error:
                # we fail an attemp - wait or print error and retry
                print(error)
                sleep(1)    
            else:
                # Everythig ok, continue
                print("MSG: VGRID Calibrated.")
                break
                
        else:
            # we failed all the attempts - deal with the consequences.
            print("ERR: Couldn't calibrate VGRID.")

    
        for attempt in range(5): # Retry
            try:
                # Tell the charger VBATT is going to calibrate
                self.write(1401, 64, 0, False)
        
                # Should return a 0
                vbat_calibration_status = self.read(1401, 0, False)
        
                if 0 != vbat_calibration_status:
                    raise Exception('MSG: Voltage is calibrating, wating... try #: %s'% attempt)
        
            except Exception as error:
                # we fail an attemp - wait or print error and retry
                print(error)
                sleep(1)
            else:
                # Everythig ok, continue
                print("MSG: VBAT Calibrated.")
                break
        else:
            # we failed all the attempts - deal with the consequences.
            raise Exception("ERR: Couldn't calibrate VBAT.")
    
    
    def write_calibration_currents(self, IGRID, IBATT):
        # Write calibration readings
        self.write(1402, int(IGRID*100), 0, False)
        self.write(1410, int(IBATT*100), 0, False)
    

        for attempt in range(5): # Retry
            try:
                # Tell the charger IGRID is going to calibrate
                self.write(1401, 128, 0, False)
        
                igrid_calibration_status = self.read(1401, 0, False)
        
                if 0 != igrid_calibration_status:
                    raise Exception('MSG: IGRID Current is calibrating, wating... try #: %s'% attempt)
                    
            except Exception as error:
                # we fail an attemp - wait or print error and retry
                print(error)
                sleep(.5)    
            else:
                # Everythig ok, continue
                print("MSG: IGRID Calibrated.")
                break
                
        else:
            # we failed all the attempts - deal with the consequences.
            print("ERR: Couldn't calibrate IGRID.")

        
        for attempt in range(5): # Retry
            try:
                # Tell the charger IBAT is going to calibrate
                self.write(1401, 256, 0, False)
        
                # Should return a 0
                ibat_calibration_status = self.read(1401, 0, False)
                
                if 0 != ibat_calibration_status:
                    raise Exception('MSG: IBAT Current is calibrating, wating... try #: %s'% attempt)
            except Exception as error:
                # we fail an attemp - wait or print error and retry
                print(error)
                sleep(1)
            else:
                # Everythig ok, continue
                print("MSG: IBAT Calibrated.")
                break
        else:
            # we failed all the attempts - deal with the consequences.
            raise Exception("ERR: Couldn't calibrate IBAT.")

    
    def reset(self):
        print ("Reset sequence initiated")
        print ("Reseting in 10s...")
        time.sleep(10.0)
        self.__reset_counter['Code'] = hex(self.unrecoverable_err)
        self.__reset_counter['Counter'] += 1
        self.__reset_counter['Time'] = time.time()
        self.__error_message.append([hex(self.unrecoverable_err)])
        self.write(20,2,0,False)
        time.sleep(1.0) # wait additional time
        self.write(20,4,0,False)        
        time.sleep(1.0) # wait additional time
        self.write(20,2,0,False)
        time.sleep(1.0)
        self.unrecoverable_err = 0
        self.recoverable_err = 0
        if (self.__reset_counter['Counter'] >= 3):
            self.raise_error("Number of retries in one hour was exceeded.")
    
    def set_current_mode(self):
        self.write(46, 1, 0, False)
        self.__working_mode = 'current'

    def set_chademo_mode(self):
        self.write(46, 0, 0, False)
        self.__working_mode = 'chademo'
    
    def checklist(self):
        # Before starting, there is a checklist
        # Verify errors
        self.unrecoverable_err = (self.read(205,0,False) * 65536) + self.read(206,0,False)
        if (self.unrecoverable_err != 0):
            print('Error {:s} detected in {:s}'.format(hex(self.unrecoverable_err),self.name))
            self.reset()
            if (self.read(205, 0, False) != 0) or (self.read(206, 0, False) != 0):
                raise NameError('Charger {:s} is locked, the error cannot be cleared.'.format(self.name))
        # Verify the rotary swich
        if (self.read(152,0,False) != 32):
            raise NameError('Rotary switch is not set to 7, check that and restart the mains.')
            
        # Wait until calibration
        if (self.__working_mode == 'undefined'):
            raise ('Working mode must be selected before the checklist: p1.set_current_mode() or p1.set_chademo_mode()')
        if (self.__working_mode == 'current'):
            print('Waiting for calibration...')
            start_time = time.time()
            while (self.read(201, 0, False) != 50):
                time.sleep(1.0)
                if (time.time() - start_time > 20.0):
                    self.status = self.read(201, 1, False)
                    raise TimeoutError('Calibration failed, returned status {:3.0f}'.format(self.status))                    
            # Wait for DC voltage
            print('Calibration done. \nWaiting for DC voltage...')
            start_time = time.time()
            while (self.read(244, 1, False) < 150.0 or self.read(244, 1, False) > 510.0):
                time.sleep(1.0)
                if (time.time() - start_time > 60.0):
                    raise TimeoutError('There is no DC voltage in {:s}'.format(self.name))   
        self.update_values()
        print('{:s} ready to go!'.format(self.name))
        
    def close_DC_relays(self):
        self.test_mode_enable(TESTING_MODE_SET_GUN_LOCK)
        time.sleep(0.5)
        self.test_mode_enable(TESTING_MODE_D1_D2_RLY_CLOSE)   
        
    def open_DC_relays(self):
        self.test_mode_disable(TESTING_MODE_D1_D2_RLY_CLOSE)   
        time.sleep(0.5)
        self.test_mode_enable(TESTING_MODE_SET_GUN_RELEASE)
        time.sleep(0.5)
        self.test_mode_disable(TESTING_MODE_SET_GUN_RELEASE)   

    def update_values(self):
        # Global variables
        self.c.close_port_after_each_call = False
        self.current_operation_read = self.read(20, 0, False)
        self.status = self.read(201, 0, False)
        self.vrms = self.read(240, 2, False)
        self.irms = self.read(241, 2, False)
        self.freq = self.read(242, 2, False)
        self.vbat = self.read(244, 1, False)
        self.ibat = self.read(245, 2, True)
        
        
        self.temp1 = self.read(246, 0, True)
        self.temp2 = self.read(247,0, True)
        self.temp3 = self.read(248, 0, True)
        
        self.temp_pfc = self.read(MODBUS_L1_PFC_TEMP, 0, True)
        self.temp_phx = self.read(MODBUS_L1_PHx_TEMP,0, True)
        self.temp_kclinks = self.read(MODBUS_L1_KC_LINK_TEMP, 0, True)
        self.temp_xfmr = self.read(MODBUS_L1_TRAFO_TEMP, 0, True)
        self.temp_ambient = self.read(MODBUS_L1_AMBIENT_TEMP, 0, True)        
        
        self.sw_freq = self.read(251, 0, False)
        self.grid_code_err = (self.read(211,0,False) * 65536) + self.read(212,0,False)
        self.unrecoverable_err = (self.read(205,0,False) * 65536) + self.read(206,0,False)
        self.recoverable_err = (self.read(207,0,False) * 65536) + self.read(208,0,False)       
        
        self.write(20, self.__operation_sp_current, 0, False)
        self.write(38, self.__operation_sp_chademo, 0, False)
        self.write(23, self.__power_sp, 0, True)
        self.write(22, self.__max_dc_current_sp, 2, True)
        self.write(21, self.__max_ac_current_sp, 2, False)
        
        # Chademo variables        
        self.chademo_state = self.read(400, 0, False)
        self.chademo_sub_1 = self.read(401, 0, False)
        self.chademo_sub_2 = self.read(402, 0, False)
        self.chademo_sub_3 = self.read(403, 0, False)
        self.chademo_sub_4 = self.read(404, 0, False)
        self.chademo_sub_5 = self.read(405, 0, False)
        self.chademo_sub_6 = self.read(406, 0, False)
#        self.chademo_procedures = self.read(407, 0, False)
#        self.chademo_go_to_F = self.read(408, 0, False)
        self.chademo_timeout_To_1 = self.read(409, 0, False)
        self.chademo_timeout_To_3 = self.read(410, 0, False)
        self.chademo_timeout_To_5 = self.read(411, 0, False)
        self.chademo_timeout_To_6 = self.read(412, 0, False)
        self.chademo_timeout_To_9 = self.read(413, 0, False)
        self.chademo_timeout_To_11 = self.read(414, 0, False)
        self.chademo_timeout_To_15 = self.read(415, 0, False)
        self.chademo_charger_monitor = self.read(416, 0, False)
        self.chademo_vehicle_monitor = self.read(417, 0, False)
        self.chademo_can_timeouts = self.read(418, 0, False)
        self.chademo_request_I = self.read(419, 0, False)
        self.chademo_present_I = self.read(420, 0, False)
        self.chademo_V_dc = self.read(421, 0, False)
        
        self.chademo_emcy_stop = self.read(426, 0, False)
        self.SoC = self.read(427, 0, False)
        self.chademo_minimum_SoC_discharge = self.read(428, 0, False)
        
        self.chademo_fault_flag = self.read(460, 0, False)
        self.chademo_status_flag = self.read(461, 0, False)
        self.chademo_state_fault_flag = self.read(470, 0, False)
        
        self.chademo_flag1_regs = self.read(1000, 0, False)
        self.chademo_flag2_regs = self.read(1001, 0, False)
        self.chademo_flag4_regs = self.read(1002, 0, False)
        self.chademo_flag6_regs = self.read(1003, 0, False)
        self.chademo_flag10_regs = self.read(1004, 0, False)
        self.chademo_flag11_regs = self.read(1005, 0, False)
        self.chademo_flag4_V2G_regs = self.read(1006, 0, False)
        
        self.c.close_port_after_each_call = True

        
        if (time.time() - self.__reset_counter['Time'] > 3600.0) and (self.__reset_counter['Counter'] > 0):
            self.__reset_counter['Counter'] -= 1
            self.__reset_counter['Time'] = time.time()
        
        self.append_buffer()
        
    def set_op(self, op):
        if (self.__working_mode == 'current'): 
            if op == START:
                self.__operation_sp_current = START
            else:
                self.__operation_sp_current = STOP # Reset is ignored. Diferent procedure
        if (self.__working_mode == 'chademo'): 
            if op == START:
                self.__operation_sp_chademo = START
            else:
                self.__operation_sp_chademo = STOP # Reset is ignored. Diferent procedure
    
    def set_power(self,power):
        self.__power_sp = 0
        if (self.__working_mode == 'current'):
            if (power <= 100) and (power >= -100):
                self.__power_sp = power            
        elif (self.__working_mode == 'chademo'):
            if (power <= 100) and (power >= 0):
                self.__power_sp = power
    
    def set_dc_current (self,current):
        self.__max_dc_current_sp = 0.0
        if (self.__working_mode == 'current'):    
            if (current <= 20.0) and (current >= 0.0):
                self.__max_dc_current_sp = current
        elif (self.__working_mode == 'chademo'):
            if (current <= 20.0) and (current >= -20.0):
                self.__max_dc_current_sp = current      
    
    def set_ac_current (self,current):
        self.__max_ac_current_sp = 0.0
        if (current <= 32.0) and (current >= 0.0):
            self.__max_ac_current_sp = current      
    
    def append_buffer(self):
        self.heading = ["time(s)", "Operation", "Status", "Vrms", "Irms", "Frequency", "Vbat", "Ibat", "temp_pfc",
                       "temp_phx", "temp_kclinks", "temp_xfmr", "temp_ambient", "sw_freq", "grid_code_err", 
                       "unrecoverable_err", "recoverable_err", "SoC", "chademo_state", "chademo_sub_1",
                       "chademo_sub_2", "chademo_sub_3", "chademo_sub_4", "chademo_sub_5", "chademo_sub_6", "chademo_timeout_To_1",
                       "chademo_timeout_To_3", "chademo_timeout_To_5", "chademo_timeout_To_6", "chademo_timeout_To_9",
                       "chademo_timeout_To_11", "chademo_timeout_To_15", "chademo_charger_monitor", "chademo_vehicle_monitor",
                       "chademo_can_timeouts","chademo_request_I", "chademo_present_I", "chademo_V_dc", "chademo_emcy_stop",
                       "chademo_minimum_SoC_discharge", "chademo_fault_flag", "chademo_status_flag", "chademo_state_fault_flag",
                       "chademo_flag1_regs", "chademo_flag2_regs", "chademo_flag4_regs", "chademo_flag6_regs", "chademo_flag10_regs",
                       "chademo_flag11_regs", "chademo_flag4_V2G_regs"]
        

        try:
            self.heading.extend(self.heading_instruments)
        except: 
            pass
        
        now = datetime.now() # current date and time
        time_stamp_excel = now.strftime("%d/%m/%Y %H:%M:%S")
        
        time_stamp_seconds = time.time() - self.starting_time # time in seconds since start
        
        time_stamp_seconds = int(time_stamp_seconds)
        
        
        new_data = [time_stamp_seconds, self.current_operation_read, self.status, 
                  self.vrms, self.irms, self.freq, self.vbat, self.ibat, self.temp_pfc, self.temp_phx, 
                  self.temp_kclinks, self.temp_xfmr, self.temp_ambient,    
                  self.sw_freq, self.grid_code_err, self.unrecoverable_err, 
                  self.recoverable_err, self.SoC, self.chademo_state, self.chademo_sub_1, self.chademo_sub_2,
                  self.chademo_sub_3, self.chademo_sub_4, self.chademo_sub_5, self.chademo_sub_6, 
                  self.chademo_timeout_To_1, self.chademo_timeout_To_3,
                  self.chademo_timeout_To_5, self.chademo_timeout_To_6, self.chademo_timeout_To_9, 
                  self.chademo_timeout_To_11, self.chademo_timeout_To_15, self.chademo_charger_monitor, 
                  self.chademo_vehicle_monitor, self.chademo_can_timeouts, self.chademo_request_I,
                  self.chademo_present_I, self.chademo_V_dc, self.chademo_emcy_stop, self.chademo_minimum_SoC_discharge,
                  self.chademo_fault_flag, self.chademo_status_flag, self.chademo_state_fault_flag, 
                  self.chademo_flag1_regs, self.chademo_flag2_regs, self.chademo_flag4_regs, 
                  self.chademo_flag6_regs, self.chademo_flag10_regs, self.chademo_flag11_regs, 
                  self.chademo_flag4_V2G_regs]
        
        try:
            new_data.extend(self.data_instruments)       
        except: 
            pass
        
        self.history_data.append(new_data)
    
    def append_instruments_buffer(self, data):       
        self.heading_instruments = data.keys()
        self.data_instruments = data.values()
 
    
    def buffer_2_file(self):
        # NL5 compatible format
        folder_name = "data_{:s}".format(self.name)
        if not os.path.exists(folder_name):
            os.makedirs(folder_name)
        now = datetime.now() # current date and time
        filename = now.strftime("%y%m%d %H%M")
        filename = folder_name + "\\" +filename+" {:d}s endurance {:s}.csv".format(int(time.time()-self.starting_time),self.name)
        f = open(filename, "w", newline='')
        data_writer = csv.writer(f, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        data_writer.writerow(self.heading)
        for elements in self.history_data:
            data_writer.writerow(elements)
        f.close()
        
    def terminate(self):
        print("Terminating {:s}...".format(self.name))
        self.write(20, STOP, 0, False)
        self.write(23, 0, 0, True)
        self.write(22, 0, 2, False)
        if (self.__error_flag):
            print("Process {:s} terminated in error".format(self.name))
            for elements in self.__error_message:
                print (elements)
            print("Charger returned error code {:s}".format(hex(self.unrecoverable_err)))
        print("Saving file...")
        self.buffer_2_file()
        self.open_DC_relays()
        print("Terminated, {:s} should be stopped, disconnect mains.".format(self.name))
        return (self.history_data)
    
    def is_in_error(self):
        if (self.unrecoverable_err > 0) or (self.recoverable_err > 0) or (self.__error_flag):
            return (True)
        else:
            return (False)
    
    def print_stats(self):
        print('Data from {:s}:'.format(self.name))
        print ('AC voltage: {:3.1f} V'.format(self.vrms))
        print ('AC current: {:3.1f} A'.format(self.irms))
        print ('DC voltage: {:3.1f} V'.format(self.vbat))
        print ('DC current: {:3.1f} A'.format(self.ibat))
        print ('SoC:        {:3.0f} %'.format(self.SoC))
        print ('Maximum temperature is: {:3.0f}ºC\n'.format(max([self.temp1, self.temp2, self.temp3])))
        print ('Current state:          {:3.0f}'.format(self.current_operation_read))
        print ('Reported {} errors, last was {:6.0f}s ago with code {:s}'.format(self.__reset_counter['Counter'],
                time.time() - self.__reset_counter['Time'], self.__reset_counter['Code']))
        print ('Running time: {:8.0f} s'.format(time.time() - self.starting_time))
        
    def __delete__(self, instance):
        self.write(20, 0, 0, False)
        
        
        