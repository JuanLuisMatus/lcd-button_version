"""
# Testbench to test driver_picolog 
@author: EP
@date: 2020.04.27
@TODO:
"""

import driver_picolog


DRV_SN = 'A0066/294'

picolog = driver_picolog.cmp_driver(DRV_SN)

picolog.update_step()


for port in range(0, 8):
    # generate a port element to pass the driver
    output_port = {'params': [port]}
    
    print('Temp port {}: {}'.format(port, picolog.update_output(output_port)))
    
    
del picolog

DRV_SN = 'A0061/748'

picolog = driver_picolog.cmp_driver(DRV_SN)

picolog.update_step()


for port in range(0, 8):
    # generate a port element to pass the driver
    output_port = {'params': [port]}
    
    print('Temp port {}: {}'.format(port, picolog.update_output(output_port)))
