"""
# Driver to use Yokogawa form from nl5
@author: EP
@date: 2020.04.30
@TODO:
"""

# User libraries import for console run
import sys
import os
script_path = os.path.dirname(__file__)
sys.path.append(script_path + '\..\..\lib')

import logging
from instruments import yokogawa

# this shoud be replaced with parameters form nl5
VISA_ADDRESS = 'TCPIP::192.168.2.241::inst0::INSTR'


class cmp_driver:
    def __init__(self, params):
        # initalization function
        logging.info('Executing Yokogawa init code.')
        GRID_ELEMENT = 3 # for backward compatibility, should be removed 
        BATT_ELEMENT = 4 # for backward compatibility, should be removed
        
        self.yoko_instrument = yokogawa(VISA_ADDRESS, GRID_ELEMENT, BATT_ELEMENT)
        
        
    def update_input(self, input_port):
        # yoko has no inputs
        ''' {cmp_name:{'xml':cmp_xml,
                      'value':value,
                      'node': cmp_node,
                      'params': params

                    }
            }
        '''
        pass
            
    def update_output(self, output_port):
        # yoko read variables
        ''' {cmp_name:{'xml':cmp_xml,
                      'value':value,
                      'node': cmp_node,
                      'params': params
                      }
            }
        '''
        
        # string containing the variable and element to read
        # ex: URMS,1 or P,3 as presented in yoko manual
        logging.info('Executing Yokogawa port update code.')

        cmd_text = output_port['params'][0] # str like  URMS
        
        yoko_value = self.yoko_instrument.read_element(cmd_text)
        
        logging.debug("Yokogawa %s value: %.2f" % (cmd_text, yoko_value)) 
        return yoko_value
        
        
    def update_step(self):
        logging.info('Executing Yokogawa step code.')
        try:
            self.yoko_instrument.test_connection()
        except:
            pass
        
    def __delete__(self):
        # close unit
        logging.info('Executing Yokogawa step code.')
        
        del self.yoko_instrument
        
        
        
        
        
        
        