from paramiko import SSHClient, AutoAddPolicy, RSAKey
from scp import SCPClient, SCPException
from paramiko.auth_handler import AuthenticationException, SSHException


class ssh_client():
    """Client to interact with a remote host via SSH & SCP."""
    def __init__(self, host):
        self.host = host
        self.user = 'root'
        self.password = 'WallBox2015JardenCore'
        self.key_file = 'aux_files/wubuntucomm'
        self.remote_path = '/'
        self.client = None
        self.scp = None
                
    def __connect(self):
        """Open connection to remote host."""
        try:
            self.client = SSHClient()
            self.client.set_missing_host_key_policy(AutoAddPolicy())
            self.client.connect(self.host,
                             username=self.user,
                             password=self.password, 
                             key_filename=self.key_file,
                             timeout=5000)
            
            self.scp = SCPClient(self.client.get_transport())
            
            
            print('MSG: SSH connection stablished.')
        except Exception as error:
            print(error)
            raise error
        finally:
            return self.client
        
    def __delete__(self):
        """Close ssh connection."""
        self.client.close()
        self.scp.close()

    def exec_command(self, cmd):
        """Execute multiple commands in succession."""
        print (f'MSG: SSH cmd: {cmd}')
        
        if self.client is None:
            self.client = self.__connect()
        try:
            if 'sci_programer' in cmd:
                timeout = 60*4
            else:
                timeout = 20
            stdin, stdout, stderr = self.client.exec_command(cmd, timeout=timeout)
            
            # Print stdout in live mode
            line_buf = " "
            while not stdout.channel.exit_status_ready():
                line_buf = stdout.readline()
                if line_buf.endswith('\n'):
                    print (line_buf.encode("utf-8"))
                    line_buf = ''

            # Print stderr in live mode
            stderr_lines = stderr.readlines()
            if len(stderr_lines):
                for line in stderr_lines:
                    print ('MSG: SSH stderr: %s' % line.encode("utf-8"))
            
            return stdin, stdout, stderr
        except SSHException as error:
            print(f'ERR: {error}')
            self.client = self.__connect()

    def upload_single_file(self, file, remote_path):
        """Upload a single file to a remote directory."""
        if self.client is None:
            self.client = self.__connect()
            
        try:
            self.scp.put(file,
                         recursive=True,
                         remote_path=remote_path)
            print(f'MSG: Uploaded {file} to {remote_path}')

        except SCPException as error:
            print(f'ERR: {error}')
            self.client = self.__connect()


def reset_dsp(ssh_client):
    """ Makes a hardware reset of the DSP."""
    print('MSG: Reseting DSP.')
    cmd = 'echo 503 > /sys/class/gpio/export'
    stdin, stdout, stderr = ssh_client.exec_command(cmd)
       
    cmd = 'echo out > /sys/class/gpio/gpio503/direction'
    stdin, stdout, stderr = ssh_client.exec_command(cmd)
    
    cmd = 'echo 0 > /sys/class/gpio/gpio503/value'
    stdin, stdout, stderr = ssh_client.exec_command(cmd)
       
    cmd = 'echo 1 > /sys/class/gpio/gpio503/value'
    stdin, stdout, stderr = ssh_client.exec_command(cmd)   
    
    cmd = 'echo 503 > /sys/class/gpio/unexport'
    stdin, stdout, stderr = ssh_client.exec_command(cmd)  
    

def install_ethtool(ssh_client):
    """Install the ethtol service into de Raspi, to fix the eth speed to 10Mbps"""
    print('MSG: Installing Ethtool service...')

    # Upload ethtool.service
    file = 'aux_files/ethtool.service'
    remote_path = '/etc/systemd/system/'
    ssh_client.upload_single_file(file, remote_path)
    
    # enable mbus service
    cmd = 'systemctl enable ethtool.service'
    stdin, stdout, stderr = ssh_client.exec_command(cmd)

    cmd = 'systemctl start ethtool.service'
    stdin, stdout, stderr = ssh_client.exec_command(cmd)
    print('MSG: ethtool service installed.')

    # Remove WiFi AP configurations
    cmd = 'rm -rf /etc/NetworkManager/system-connections/*'
    stdin, stdout, stderr = ssh_client.exec_command(cmd)
    print('MSG: Remove WiFi AP configurations.')
        

def connect_wifi_to_wallbox(ssh_client):
    """ Connects to Wallbox wifi network"""
    print('MSG: Connecting to Wallbox WiFi...')

    # Remove WiFi AP configurations
    cmd = 'nmcli dev wifi con WallboxEng password WALLBOX2015!'
    stdin, stdout, stderr = ssh_client.exec_command(cmd)
    
    # Remove WiFi AP configurations
    cmd = "find /etc/NetworkManager/system-connections/ -type f -not -name 'WallboxEng' -delete"
    stdin, stdout, stderr = ssh_client.exec_command(cmd)
    print('MSG: Remove WiFi AP configurations.')


def remove_wifi_aps(ssh_client):
    """ Removes all WiFi APs configurations"""
    print('MSG: Removing WiFi APs configurations...')
    
    # Remove WiFi AP configurations
    cmd = 'rm -rf /etc/NetworkManager/system-connections/*'
    stdin, stdout, stderr = ssh_client.exec_command(cmd)
    print('MSG: Remove WiFi AP configurations.')


def install_mbus(ssh_client):
    """Install the mbus service into de Raspi, required for testing and calibration"""
    print('MSG: Installing MBUS service...')
    
    # Disable micro2wallbox
    cmd = 'systemctl disable micro2wallbox'
    stdin, stdout, stderr = ssh_client.exec_command(cmd)

    cmd = 'systemctl stop micro2wallbox'
    stdin, stdout, stderr = ssh_client.exec_command(cmd)
    
    # Kill micro2wallbox
    cmd = 'killall micro2wallbox'
    stdin, stdout, stderr = ssh_client.exec_command(cmd)
    
    # make testing dir
    cmd = 'mkdir /testing'
    stdin, stdout, stderr = ssh_client.exec_command(cmd)

    # Upload mbusd.service
    file = 'aux_files/mbusd.service'
    remote_path = '/etc/systemd/system/'
    ssh_client.upload_single_file(file, remote_path)

    # Upload mbusd.service
    file = 'aux_files/mbusd.bin'
    remote_path = '/testing/mbusd'
    ssh_client.upload_single_file(file, remote_path)

    # Give execution permission
    cmd = 'chmod u+x /testing/mbusd'
    stdin, stdout, stderr = ssh_client.exec_command(cmd)

    # enable mbus service
    cmd = 'systemctl enable mbusd.service'
    stdin, stdout, stderr = ssh_client.exec_command(cmd)

    cmd = 'systemctl start mbusd.service'
    stdin, stdout, stderr = ssh_client.exec_command(cmd)
    print('MSG: MBUS service installed.')

def remove_mbus(ssh_client):
    """Removes the mbus service into de Raspi and enables the micro2wallbox service"""
    print('MSG: Removing MBUS service...')
    # Disable mbus
    cmd = 'systemctl disable mbusd.service'
    stdin, stdout, stderr = ssh_client.exec_command(cmd)
    
    cmd = 'systemctl stop mbusd.service'
    stdin, stdout, stderr = ssh_client.exec_command(cmd)
    
    # Remove some files
    cmd = 'rm -rf /testing'
    stdin, stdout, stderr = ssh_client.exec_command(cmd)

    cmd = 'rm /etc/systemd/system/mbusd.service'
    stdin, stdout, stderr = ssh_client.exec_command(cmd)

    # Enable micro2wallbox and reboot
    cmd = 'systemctl enable micro2wallbox'
    stdin, stdout, stderr = ssh_client.exec_command(cmd)

    cmd = 'systemctl start micro2wallbox'
    stdin, stdout, stderr = ssh_client.exec_command(cmd)

    cmd = '/sbin/reboot'
    stdin, stdout, stderr = ssh_client.exec_command(cmd)

    print('MSG: MBUS service removed.')


def flash_dsp(ssh_client, firmware_file):
    """Load a firmware file into the DSP, it takes 3 min aprox."""
    print('MSG: Loading Firmware into DSP...')
    # Disable and stop the mbus service
    cmd = 'systemctl disable mbusd.service && systemctl stop mbusd.service'
    stdin, stdout, stderr = ssh_client.exec_command(cmd)
    
    # Kills process blocking serial port
    cmd = 'fuser -k /dev/ttyS0'
    stdin, stdout, stderr = ssh_client.exec_command(cmd)
    cmd = 'fuser -k /dev/ttyS0'
    stdin, stdout, stderr = ssh_client.exec_command(cmd)
    
    # Load the required files
    remote_path = '/lib/firmware/wallbox/dc_charger_ct_firmware/'
    ssh_client.upload_single_file(firmware_file, remote_path)

    # Kill sci_programer 
    cmd = 'killall sci_programer'
    stdin, stdout, stderr = ssh_client.exec_command(cmd)
  
    # Program DSP
    cmd = 'sci_programer /lib/firmware/wallbox/dc_charger_ct_firmware/bootloader_v100.txt /lib/firmware/wallbox/dc_charger_ct_firmware/uCharger_v2002_final_v1.txt'
    print('MSG: Starting flashing, FANs must turn on.')
    stdin, stdout, stderr = ssh_client.exec_command(cmd)
    
    # Enable required services 
    cmd = 'systemctl enable mbusd.service'
    stdin, stdout, stderr = ssh_client.exec_command(cmd)

    cmd = 'systemctl start mbusd.service'
    stdin, stdout, stderr = ssh_client.exec_command(cmd)

    print('MSG: Finish loading Firmware into DSP.')



if __name__ == '__main__':
    ssh_client = ssh_client(host)
    
    install_mbus(ssh_client)
    
    reset_dsp(ssh_client)
    
    firmware_file = 'aux_files/uCharger_v2002_final_v1.txt'
    flash_dsp(ssh_client, firmware_file)




