from time import sleep
import time
import sys
from termcolor import colored
import socket
from getmac import get_mac_address
import argparse
from ssh_over_python import ssh_client, install_mbus, reset_dsp, flash_dsp, remove_mbus,\
    install_ethtool, connect_wifi_to_wallbox, remove_wifi_aps
from custom_settings import get_devices_addresses
from pathlib import Path
import re

'''
import atexit

def exit_handler():
    try:
        print('MSG: The end.')
    except:
        pass
        

atexit.register(exit_handler)
'''

from QuasarClass import quasar
from instruments import yokogawa, bk_source, itech_source

# from wbn_to_ip import wbn_to_ip
STATION_NUMBER = 2
SCRIPT_VERSION = '3.6'
FW_VER = 2002 # fw  version to check

BK_ADDRESS, ITECH_ADDRESS = get_devices_addresses(STATION_NUMBER)

INIT_AC = True
INIT_DC = True


class Logger(object):
    def __init__(self):
        self.terminal = sys.stdout
        self.log = open(LOG_FILE_NAME, "a")

    def write(self, message):
        self.terminal.write(message)
        self.log.write(message)  

    def flush(self):
        #this flush method is needed for python 3 compatibility.
        #this handles the flush command by doing nothing.
        #you might want to specify some extra behavior here.
        pass    


# Power sources settings
EOL_SCALE_FACTOR = 230/100
GRID_VOLTAGE_SETTING = 230 / EOL_SCALE_FACTOR
GRID_FREQ_SETTING = 50
GRID_CURRENT_LIMIT = 33

BAT_VOLTAGE_SETTING = 360 / EOL_SCALE_FACTOR
BAT_CURRENT_LIMIT = 20


# Registers definitions
MODBUS_PS_ACTIONS_REGISTER = 20
MODBUS_SYSTEM_BOOT_STATUS = 201
MODBUS_FIRMWARE_VERSION = 602

MODBUS_CHARGER_STATUS = 180
MODBUS_UNRECOVERABLE_ERROR_HI = 205
MODBUS_G99_STOP_ORDER = 149
MODBUS_ROTARY_SWITCH = 152
MODBUS_GUN_SELECTOR = 154

MODBUS_L1_AC_VOLTAGE_RMS = 240
MODBUS_L1_AC_FREQUENCY = 242
MODBUS_L1_AC_CURRENT_RMS = 241
MODBUS_L1_DC_BUS_VOLTAGE = 243
MODBUS_L1_DC_VOLTAGE = 244
MODBUS_L1_DC_CURRENT = 245

MODBUS_POWER_DERATING_STATUS = 213

MODBUS_TESTING_MODE_ORDER_LO = 3000
MODBUS_TESTING_MOSFETS_ORDERS = 3002
MODBUS_VEHICLE_CHARGE_PERMISSION = 141
MODBUS_VEHICLE_PROXIMITY_DETECTION = 142
MODBUS_GUN_STATUS = 147

MODBUS_CHARGER_MESSAGES = 181

TESTING_MODE_ORDER_DO_NOTHING        = 0x0000
TESTING_MODE_ORDER_SCALE_MODE        = 0x0001
TESTING_MODE_ORDER_SCALE_MODE_EOL    = 0x0002
TESTING_MODE_ORDER_PWR_DERAT_TUNE    = 0x0004
TESTING_MODE_ORDER_PERIPH_CHECK      = 0x0008
TESTING_MODE_ORDER_DUTY_0            = 0x0010
TESTING_MODE_ORDER_DUTY_25           = 0x0020
TESTING_MODE_ORDER_DUTY_50           = 0x0040
TESTING_MODE_ORDER_DUTY_75           = 0x0080
TESTING_MODE_ORDER_DUTY_100          = 0x0100
TESTING_MODE_PRECHARGE_RLY_CLOSE     = 0x0200
TESTING_MODE_GRD_CONNECT_RLY_CLS     = 0x0400
TESTING_MODE_START_CAN_COMMS_TX      = 0x0800
TESTING_MODE_D1_D2_RLY_CLOSE         = 0x1000
TESTING_MODE_SET_GUN_LOCK            = 0x2000
TESTING_MODE_SET_GUN_RELEASE         = 0x4000
TESTING_MODE_MOSFET_BRIDGES          = 0x8000


TESTING_MODE_MOSFET_CTRL_B1_1A = 0x0001
TESTING_MODE_MOSFET_CTRL_B1_1B = 0x0002
TESTING_MODE_MOSFET_CTRL_B1_2A = 0x0004
TESTING_MODE_MOSFET_CTRL_B1_2B = 0x0008
TESTING_MODE_MOSFET_CTRL_B2_1A = 0x0010
TESTING_MODE_MOSFET_CTRL_B2_1B = 0x0020
TESTING_MODE_MOSFET_CTRL_B2_2A = 0x0040
TESTING_MODE_MOSFET_CTRL_B2_2B = 0x0080
TESTING_MODE_MOSFET_CTRL_PFC_1A = 0x0100
TESTING_MODE_MOSFET_CTRL_PFC_1B = 0x0200 
TESTING_MODE_MOSFET_CTRL_PFC_2A = 0x0400
TESTING_MODE_MOSFET_CTRL_PFC_2B = 0x0800



def init_ac_source(BK_ADDRESS):
    # Setup the BK connected to the AC side
    bk_instrument = bk_source(BK_ADDRESS)
    
    bk_instrument.read_errors()
    
    # Set the BK Power Source
    bk_instrument.set_ac_voltage(GRID_VOLTAGE_SETTING)
    bk_instrument.set_ac_freq(GRID_FREQ_SETTING)
    bk_instrument.set_current_limit(GRID_CURRENT_LIMIT)
    #bk_instrument.set_output_current_delay()

    
    bk_instrument.output_state('ON')
    sleep(1) # Wait voltage to rise

    return bk_instrument


def init_dc_source(ITECH_ADDRESS):
    # Setup the BK connected to the DC side
    itech_instrument = itech_source(ITECH_ADDRESS)
    
    itech_instrument.output_state('OFF')

    itech_instrument.recall_default()
    itech_instrument.clear_errors()
    itech_instrument.set_remote()
        
    # Set the BK Power Source
    itech_instrument.set_voltage(BAT_VOLTAGE_SETTING)
    itech_instrument.set_current_limit_pos(BAT_CURRENT_LIMIT)
    itech_instrument.set_current_limit_neg(-BAT_CURRENT_LIMIT)

    sleep(2) # Wait voltage to rise
    
    # itech_instrument.read_errors()

    return itech_instrument



def test_communication():
    # TEST: Test communication, by writing reading a value
    value = int(0x2) # Some value to write
    p1.write(MODBUS_PS_ACTIONS_REGISTER, value, 0, False)


    fw_version = p1.read(MODBUS_FIRMWARE_VERSION, 0, False)
    if fw_version == FW_VER:
        print(colored('MSG: FW Version: %s' % fw_version, 'green'))
    else:
        raise Exception('ERR: FW Version: %s' % fw_version)

    if p1.read(MODBUS_PS_ACTIONS_REGISTER, 0, False) == value:
        print(colored('MSG: RS485 communication OK', 'green'))
    else:
        print(colored('ERR: RS485 communication FAIL', 'red'))
        
        
def test_dsp_integrity():
    # TEST: DSP integrity check
    value = int(0x4) # Reset errors
    p1.write(MODBUS_PS_ACTIONS_REGISTER, value, 0, False)
    value = int(0x2) # Goto stop
    p1.write(MODBUS_PS_ACTIONS_REGISTER, value, 0, False)

    for attempt in range(10): # Retry 10 times
        try:
            charger_status = p1.read(MODBUS_CHARGER_STATUS, 0, False) 
            charger_unrecoverable_errors_hi = p1.read(MODBUS_UNRECOVERABLE_ERROR_HI, 0, False)
            if charger_status == 30 and \
                (charger_unrecoverable_errors_hi >> 2 & 1) == 0: # check bit 2
                print(colored('MSG: DSP Integrity OK', 'green'))
            else:
                print('MSG: MODBUS_CHARGER_STATUS: %d' % charger_status)
                print('MSG: MODBUS_UNRECOVERABLE_ERROR_HI: %d' % charger_unrecoverable_errors_hi)
                raise Exception('WAR: Wainting DSP to Start... try #: %s'% attempt)
    
        except Exception as error:
            # we fail an attemp - wait or print error and retry
            print(error)
            sleep(10)
            
        else:
            # Everythig ok, continue
            break
    else:
        # we failed all the attempts - deal with the consequences.
        raise Exception(colored('ERR: DSP Integrity FAIL', 'red'))


##############################################################################
# TEST: EEPROM check and part number write TODO: under development 


def test_rotary_switch():
    # TEST: Rotary Switch    
    # Enable some command to read the rotary in real time
    # modbus_testing_mode.enable(TESTING_MODE_ORDER_SCALE_MODE_EOL)
    
    print('USER: Set the rotary switch to 7.')
    
    for attempt in range(12): # Retry 12 times
        try:
            rotary_position = p1.read(MODBUS_ROTARY_SWITCH, 0, False)
            
            if  rotary_position == 32:
                print(colored('MSG: Rotary position OK. rotary_position: %d' % rotary_position, 'green'))
            else:
                print('MSG: Rotary position WRONG. rotary_position: %d' % rotary_position)
                raise Exception('WAR: Set the rotary switch to 7... try #: %s' % attempt)
    
        except Exception as error:
            # we fail an attemp - wait or print error and retry
            print(error)
            sleep(10)
            
        else:
            # Everythig ok, continue
            break
    else:
        # we failed all the attempts - deal with the consequences.
        raise Exception(colored('ERR: Rotary position FAIL. Value: %s', rotary_position, 'red'))


def test_uk99():
    # UK G99 Stop pin check
    for attempt in range(10): # Retry 10 times
        try:        
            if  p1.read(MODBUS_G99_STOP_ORDER, 0, False) == 1:
                print(colored('MSG: G99 stop order OK', 'green'))
            else:
                raise Exception('WAR: Check G99 jumper... try #: %s'% attempt)
    
        except Exception as error:
            # we fail an attemp - wait or print error and retry
            print(error)
            sleep(2)
            
        else:
            # Everythig ok, continue
            break
    else:
        # we failed all the attempts - deal with the consequences.
        raise Exception(colored('ERR: G99 jumper check FAIL.', 'red'))


def test_measurement_grid_voltage():
    # TEST: Grid voltage measure
    # fixme: Corregir documentación del test. No se usa tester
    modbus_testing_mode.safe_state() # Send the DSP to safe state    
    
    for attempt in range(10): # Retry 10 times
        try:
            grid_voltage = p1.read(MODBUS_L1_AC_VOLTAGE_RMS, 2, False) 
            grid_freq = p1.read(MODBUS_L1_AC_FREQUENCY, 2, False)
            
            # Check if voltage and freq is within range
            if  abs(GRID_VOLTAGE_SETTING*EOL_SCALE_FACTOR-grid_voltage) < 20 and \
                abs(GRID_FREQ_SETTING-grid_freq) < 5:
                print(colored('MSG: Grid voltage measurements OK. grid_volt: %.2f, grid_freq: %.2f' % (grid_voltage, grid_freq), 'green'))
            else:
                raise Exception('WAR: Wrong grid voltage measurements... try #: %s, grid_volt: %.2f, grid_freq: %.2f' % (attempt, grid_voltage, grid_freq))
    
        except Exception as error:
            # we fail an attemp - wait or print error and retry
            print(error)
            sleep(2)
            
        else:
            # Everythig ok, continue
            break
    else:
        # we failed all the attempts - deal with the consequences.
        raise Exception(colored('ERR: Wrong grid voltage measurements. grid_volt: %.2f, grid_freq: %.2f' % (grid_voltage, grid_freq), 'red'))


def test_gun_selector(GUN_MODEL = 1):    
    # TEST: Check that gun selector input works properly.
    for attempt in range(10): # Retry 10 times
        try:        
            gun_selector_value = p1.read(MODBUS_GUN_SELECTOR, 0, False)
            if gun_selector_value == GUN_MODEL:
                print(colored('MSG: Installed GUN and selector OK', 'green'))
            else:
                raise Exception('WAR: Wrong GUN or selector... try #: %s , value: %d'% (attempt, gun_selector_value))
    
        except Exception as error:
            # we fail an attemp - wait or print error and retry
            print(error)
            sleep(2)
            
        else:
            # Everythig ok, continue
            break
    else:
        # we failed all the attempts - deal with the consequences.
        raise Exception(colored('ERR: Wrong GUN or selector. FAIL, value: %d'% gun_selector_value, 'red'))


def test_gun_lock():
    # TEST: CHAdeMO gun release relay check TODO: ask how gun feedback works
    print('USER: Plug the GUN into the socket.')
    modbus_testing_mode.enable(TESTING_MODE_SET_GUN_LOCK)
    
    user_response = 'y' # input('USER: Is the GUN locked??????? [y/n]')
    
    if user_response == 'y':
        for attempt in range(10): # Retry 10 times
            try:        
                value = p1.read(MODBUS_GUN_STATUS, 0, False)
                if value  == 1:
                    print(colored('MSG: GUN feedback OK', 'green'))
                else:
                    raise Exception('WAR: GUN feedback wrong. Plug the gun... try #: %s , value: %d'% (attempt, value))
        
            except Exception as error:
                # we fail an attemp - wait or print error and retry
                print(error)
                sleep(2)
                
            else:
                # Everythig ok, continue
                break
        else:
            # we failed all the attempts - deal with the consequences.
            raise Exception('ERR: GUN feedback wrong. FAIL')
    
    else:
        raise Exception(colored('ERR: GUN doesn\'t LOCK FAIL. user_response: %s: ' % user_response, 'red'))


def test_gun_unlock():
    # TEST: CHAdeMO gun release relay check
    modbus_testing_mode.enable(TESTING_MODE_SET_GUN_RELEASE)
    sleep(1)
    print('MSG: The GUN must be unlocked.')
    
    for attempt in range(10): # Retry 10 times
        try:        
            value = p1.read(MODBUS_GUN_STATUS, 0, False)
            if value  == 0:
                print('MSG: GUN unlocked OK')
            else:
                raise Exception('WAR: GUN unlocked wrong... try #: %s , value: %d'% (attempt, value))
    
        except Exception as error:
            # we fail an attemp - wait or print error and retry
            print(error)
            sleep(2)
            
        else:
            # Everythig ok, continue
            break
    else:
        # we failed all the attempts - deal with the consequences.
        raise Exception(colored('ERR: GUN unlocked wrong. FAIL', 'red'))
    
    
def test_chademo_proximity():
    # TEST: CHAdeMO proximity detection check
    modbus_testing_mode.enable(TESTING_MODE_SET_GUN_LOCK)
    
    for attempt in range(10): # Retry 10 times
        try:        
            value = p1.read(MODBUS_VEHICLE_PROXIMITY_DETECTION, 0, False)
            if value  == 0:
                print('MSG: Vehicle proximity detection OK. value must be 0, value: %s' % value)
            else:
                raise Exception('WAR: Vehicle proximity detection wrong. value must be 0... try #: %s , value: %d'% (attempt, value))
    
            # Close D1 and D2
            modbus_testing_mode.enable(TESTING_MODE_D1_D2_RLY_CLOSE)
    
            value = p1.read(MODBUS_VEHICLE_PROXIMITY_DETECTION, 0, False)
            if value  == 1:
                modbus_testing_mode.safe_state() # Send the DSP to set state
                print('MSG: Vehicle proximity detection OK. value must be 1, value: %s' % value)
    
            else:
                raise Exception('WAR: Vehicle proximity detection wrong. value must be 1... try #: %s , value: %d'% (attempt, value))
    
            
        except Exception as error:
            # we fail an attemp - wait or print error and retry
            print(error)
            sleep(2)
            
        else:
            # Everythig ok, continue
            break
    else:
        # we failed all the attempts - deal with the consequences.
        modbus_testing_mode.safe_state() # Send the DSP to set state
        raise Exception(colored('ERR: Vehicle proximity detection wrong. FAIL', 'red'))



def test_chademo_vehicle_permission():
    # TEST: CHAdeMO vehicle charge permission check TODO: No importa el estado de D1 D2 si la manguera está connectada lee un 1
    modbus_testing_mode.enable(TESTING_MODE_SET_GUN_LOCK)

    for attempt in range(10): # Retry 10 times
        try:    
            value = p1.read(MODBUS_VEHICLE_CHARGE_PERMISSION, 0, False)
            if value  == 0:
                print('MSG: Vehicle charge permission detection OK. value must be 0, value: %s' % value)
            else:
                raise Exception('WAR: Vehicle charge permission detection wrong. value must be 0... try #: %s , value: %d'% (attempt, value))
    
            # Close D1 and D2
            modbus_testing_mode.enable(TESTING_MODE_D1_D2_RLY_CLOSE)
    
            value = p1.read(MODBUS_VEHICLE_CHARGE_PERMISSION, 0, False)
            if value  == 1:
                modbus_testing_mode.safe_state() # Send the DSP to set state
                print('MSG: Vehicle charge permission detection OK. value must be 1, value: %s' % value)
            else:
                raise Exception('WAR: Vehicle charge permission detection wrong. value must be 1... try #: %s , value: %d'% (attempt, value))
    
            
        except Exception as error:
            # we fail an attemp - wait or print error and retry
            print(error)
            sleep(2)
            
        else:
            # Everythig ok, continue
            break
    else:
        # we failed all the attempts - deal with the consequences.
        modbus_testing_mode.safe_state() # Send the DSP to set state
        raise Exception(colored('ERR: Vehicle charge permission detection wrong. FAIL', 'red'))



##############################################################################
# TEST: CHAdeMO CAN Tx check TODO: Tooling required

##############################################################################
# TEST: CHAdeMO CAN Rx check TODO: Tooling required
def test_measurement_batery_voltage():
    # TEST: CHAdeMO D1 and D2 and Battery voltage measure
    # modbus_testing_mode.safe_state() # Send the DSP to set state

    # Lower the grid voltage
    BAT_VOLTAGE_SETTING = 150/EOL_SCALE_FACTOR
    
    itech_instrument.set_voltage(BAT_VOLTAGE_SETTING)
    itech_instrument.set_current_limit_pos(1)
    itech_instrument.set_current_limit_neg(-1)
    itech_instrument.output_state('ON')
    sleep(1) # Wait voltage to raise
    
    #TODO: config itech output resistance to protect charger
    
    # Close D1 and D2
    modbus_testing_mode.enable(TESTING_MODE_SET_GUN_LOCK)
    modbus_testing_mode.enable(TESTING_MODE_D1_D2_RLY_CLOSE)
 
    for attempt in range(10): # Retry 10 times
        try:        
            dc_bat_voltage = p1.read(MODBUS_L1_DC_VOLTAGE, 1, False) 
            # Check if value is within 10% of error
            if abs(dc_bat_voltage - BAT_VOLTAGE_SETTING*EOL_SCALE_FACTOR)  <= 20:
                itech_instrument.recall_default()
                modbus_testing_mode.safe_state() # Send the DSP to set state
                print('MSG: Battery voltage reading OK. should be: %.2f, value: %.2f' %  (BAT_VOLTAGE_SETTING*EOL_SCALE_FACTOR, dc_bat_voltage))
                
            else:
                raise Exception('WAR: Battery voltage error... try #: %s , value: %.2f' % (attempt, dc_bat_voltage))
    
        except Exception as error:
            # we fail an attemp - wait or print error and retry
            print(error)
            sleep(2)
            
        else:
            # Everythig ok, continue
            break
    else:
        # we failed all the attempts - deal with the consequences.
        itech_instrument.recall_default()
        modbus_testing_mode.safe_state() # Send the DSP to set state

        raise Exception(colored('ERR: Battery voltage reading FAIL. should be: %.2f, value: %.2f' %  (BAT_VOLTAGE_SETTING*EOL_SCALE_FACTOR, dc_bat_voltage), 'red'))
    

def test_precharge_relay():
    # TEST: Precharge relay test and DC Link voltage measure
    #modbus_testing_mode.enable(TESTING_MODE_ORDER_SCALE_MODE_EOL)

    for attempt in range(30): # Retry 30 times
        try:        
            dc_bus_value = p1.read(MODBUS_L1_DC_BUS_VOLTAGE, 1, False)
            # Check if value is lower than 10V
            if dc_bus_value  <= 50:
                print('MSG: Bus voltage within safe range. OK. value: %.2f' %  dc_bus_value)
            else:
                raise Exception('WAR: Bus voltage to high... try #: %s , value: %.2f' % (attempt, dc_bus_value))
    
        except Exception as error:
            # we fail an attemp - wait or print error and retry
            print(error)
            sleep(2)
            
        else:
            # Everythig ok, continue
            break
    else:
        # we failed all the attempts - deal with the consequences.
        raise Exception(colored('ERR: Battery voltage to high. Can\'t go on. FAIL. value: %.2f' % dc_bus_value, 'red'))

    modbus_testing_mode.enable(TESTING_MODE_PRECHARGE_RLY_CLOSE)    
    print('MSG: The Precharge relay must be closed.')
    sleep(2)
    
    dc_bus_value = p1.read(MODBUS_L1_DC_BUS_VOLTAGE, 1, False)
    timeout_cnt = 0
    while (abs(dc_bus_value - GRID_VOLTAGE_SETTING*1.41))  > 50:
        timeout_cnt += 1
        dc_bus_value = p1.read(MODBUS_L1_DC_BUS_VOLTAGE, 1, False)
        modbus_testing_mode.enable(TESTING_MODE_PRECHARGE_RLY_CLOSE)    

        sleep(1)

        print('WAR: Bus voltage error to big... try #: %s , value: %.2f' % (timeout_cnt, dc_bus_value))
        if timeout_cnt > 10:
            raise Exception(colored('ERR: Precharge relay or bus voltage measurement error. Can\'t go on. FAIL. value: %.2f' % dc_bus_value, 'red'))
    
    else:
        print('MSG: Precharge relay closed. Bus voltage within range. OK. value: %.2f' %  dc_bus_value)
    
    
    # Check if precharge relay opens and voltage decreases
    modbus_testing_mode.disable(TESTING_MODE_PRECHARGE_RLY_CLOSE)
    print('MSG: The Precharge relay must be open.')
    sleep(1)
    
    ok_count = 0
    for attempt in range(10): # Retry 10 times
        try:
            while ok_count <= 4:
                dc_bus_value_new = p1.read(MODBUS_L1_DC_BUS_VOLTAGE, 1, False)
                # Check if value is within 10%
                if dc_bus_value_new < dc_bus_value:
                    print('MSG: Bus voltage is decreasing. OK %d/4. value: %.2f' %  (ok_count, dc_bus_value_new))
                    ok_count += 1
                else:   
                    print('WAR: Bus voltage is not decreasing... try #: %s , value: %.2f' % (attempt, dc_bus_value_new))

                dc_bus_value = dc_bus_value_new
                sleep(1)
                
                if attempt >= 10:
                    raise Exception(colored('ERR: Precharge relay or bus voltage measurement error. Can\'t go on. FAIL. value: %.2f' % dc_bus_value_new, 'red'))
                
            else:
                modbus_testing_mode.safe_state()
                print('MSG: Precharge relay. OK.')
                    
        except Exception as error:
            # we fail an attemp - wait or print error and retry
            print(error)
            sleep(2)
            
        else:
            # Everythig ok, continue
            break
    else:
        # we failed all the attempts - deal with the consequences.
        modbus_testing_mode.safe_state()
        raise Exception(colored('ERR: Precharge relay or bus voltage measurement error. Can\'t go on. FAIL. value: %.2f' % dc_bus_value, 'red'))


def test_main_relay():
    # TEST: Grid connection relay test, should be carried out right after the previous test
    #modbus_testing_mode.enable(TESTING_MODE_ORDER_SCALE_MODE_EOL)
    
    for attempt in range(10): # Retry 10 times
        try:        
            dc_bus_value = p1.read(MODBUS_L1_DC_BUS_VOLTAGE, 1, False)
            # Check if bus voltage is not to low
            if dc_bus_value  >= GRID_VOLTAGE_SETTING * 1.41 * .8:
                print('MSG: Bus voltage within safe range. OK. value: %.2f' %  dc_bus_value)
            else:
                modbus_testing_mode.enable(TESTING_MODE_PRECHARGE_RLY_CLOSE)
                print('MSG: Closing the precharge relay...') 
                raise Exception('WAR: Bus voltage to low... try #: %s , value: %.2f' % (attempt, dc_bus_value))
    
            modbus_testing_mode.disable(TESTING_MODE_PRECHARGE_RLY_CLOSE)
            # Wait and update the dc_bus_value
            sleep(2)
            dc_bus_value = p1.read(MODBUS_L1_DC_BUS_VOLTAGE, 1, False)

        except Exception as error:
            # we fail an attemp - wait or print error and retry
            print(error)
            sleep(2)
            
        else:
            # Everythig ok, continue
            break
    else:
        # we failed all the attempts - deal with the consequences.
        modbus_testing_mode.safe_state()
        raise Exception(colored('ERR: Battery voltage to low. Can\'t go on. FAIL. value: %.2f' % dc_bus_value, 'red'))
    
    
    # Close main relay
   
    modbus_testing_mode.enable(TESTING_MODE_GRD_CONNECT_RLY_CLS)
    print('MSG: The Main relay must be closed.')
    sleep(1)
    
    for attempt in range(10): # Retry 10 times
        try:        
            dc_bus_value_new = p1.read(MODBUS_L1_DC_BUS_VOLTAGE, 1, False)
            # Check if value is within 10%
            if dc_bus_value_new > dc_bus_value:
                print('MSG: Bus voltage within range. OK. value: %.2f' %  dc_bus_value)
            else:
                modbus_testing_mode.enable(TESTING_MODE_GRD_CONNECT_RLY_CLS)
                raise Exception('WAR: Bus voltage error... try #: %s , value: %.2f' % (attempt, dc_bus_value))
    
        except Exception as error:
            # we fail an attemp - wait or print error and retry
            print(error)
            sleep(2)
            
        else:
            # Everythig ok, continue
            break
    else:
        # we failed all the attempts - deal with the consequences.
        modbus_testing_mode.safe_state()
        raise Exception(colored('ERR: Main relay or bus voltage measurement error. Can\'t go on. FAIL. dc_bus_value: %.2f' % dc_bus_value, 'red'))
    
    
    # Check if precharge relay opens and voltage decreases
    modbus_testing_mode.disable(TESTING_MODE_GRD_CONNECT_RLY_CLS)
    print('MSG: The Main relay must be open.')
    
    ok_count = 0
    for attempt in range(10): # Retry 10 times
        try:
            while ok_count <= 4:
                dc_bus_value_new = p1.read(MODBUS_L1_DC_BUS_VOLTAGE, 1, False)
                # Check if value is within 10%
                if dc_bus_value_new < dc_bus_value:
                    print('MSG: Bus voltage is decreasing. OK %d/4. value: %.2f' %  (ok_count, dc_bus_value_new))
                    ok_count += 1
                else:   
                    print('WAR: Bus voltage is not decreasing... try #: %s , value: %.2f' % (attempt, dc_bus_value_new))

                dc_bus_value = dc_bus_value_new
                sleep(1)
                
                if attempt >= 10:
                    raise Exception(colored('ERR: Main relay or bus voltage measurement error. Can\'t go on. FAIL. value: %.2f' % dc_bus_value_new, 'red'))
            else:
                modbus_testing_mode.safe_state()
                print('MSG: Main relay. OK.')
                    
        except Exception as error:
            # we fail an attemp - wait or print error and retry
            print(error)
            sleep(2)
            
        else:
            # Everythig ok, continue
            break
    else:
        # we failed all the attempts - deal with the consequences.
        modbus_testing_mode.safe_state()
        raise Exception(colored('ERR: Main relay or bus voltage measurement error. Can\'t go on. FAIL. dc_bus_value: %.2f' % dc_bus_value, 'red'))


def test_fan():
    # TEST: FAN control check
    # modbus_testing_mode.enable(TESTING_MODE_ORDER_SCALE_MODE_EOL)

    # Read the idle consumption
    idle_power = bk_instrument.read_magnitude('power', False)
    print('MSG: Power consumption without FANs: %.2f' % idle_power)
    
    for attempt in range(10): # Retry 10 times
        try:
            # Read the consumed power
            # Turn ON Fan to 100%
            modbus_testing_mode.enable(TESTING_MODE_ORDER_DUTY_100)
            print('MSG: The FANs must be on.')
            sleep(5)
    
            new_power = bk_instrument.read_magnitude('power')
            
            # Measure the delta 
            if (3  < new_power - idle_power < 10): # Fans consume 8W apox
                print(colored('MSG: FANs seem to be OK', 'green'))
                print('MSG: FANs consumption: %.2f' % (new_power-idle_power))
                modbus_testing_mode.safe_state() 
                
            else:
                raise Exception('WAR: FANs consumption to high or to low... try #: %s , power: %.2f' % (attempt, new_power))
    
        except Exception as error:
            # we fail an attemp - wait or print error and retry
            print(error)
            sleep(2)
            
        else:
            # Everythig ok, continue
            break
    else:
        # we failed all the attempts - deal with the consequences.
        modbus_testing_mode.safe_state() 
        raise Exception(colored('ERR: FANs consumption to high or to low. FAIL, power: %d'% new_power, 'red'))

##############################################################################
# TEST: Bridge 1 temperature measure check TODO

##############################################################################
# TEST: Bridge 2 temperature measure check TODO

##############################################################################
# TEST: KC link temperature measure check TODO


def test_primary_bridge(leg_to_test):
    # TEST: PFC and Primary B1 MOSFET test and grid current test
   
    if leg_to_test == 'pfc_leg1':
        fet_modbus_testing_mode = TESTING_MODE_MOSFET_CTRL_PFC_1A + TESTING_MODE_MOSFET_CTRL_PFC_1B # Close FETs
    
    elif leg_to_test == 'pfc_leg2':
        fet_modbus_testing_mode = TESTING_MODE_MOSFET_CTRL_PFC_2A + TESTING_MODE_MOSFET_CTRL_PFC_2B # Close FETs
        
    elif leg_to_test == 'dcdc_primary_leg1':
        fet_modbus_testing_mode = TESTING_MODE_MOSFET_CTRL_B1_1A + TESTING_MODE_MOSFET_CTRL_B1_1B # Close FETs

    elif leg_to_test == 'dcdc_primary_leg2':
        fet_modbus_testing_mode = TESTING_MODE_MOSFET_CTRL_B1_2A + TESTING_MODE_MOSFET_CTRL_B1_2B # Close FETs
         
    else:
        raise Exception('ERR: Wrong leg to test, should be pfc_leg1/pfc_leg2/dcdc_primary_leg1/dcdc_primary_leg2.')

    
    for attempt in range(10): # Retry 10 times
        try:
            modbus_testing_mode.safe_state()
            # modbus_testing_mode.enable(TESTING_MODE_ORDER_SCALE_MODE_EOL)
            
            dc_bus_value = p1.read(MODBUS_L1_DC_BUS_VOLTAGE, 1, False)
            # Check if bus is discharged
            if dc_bus_value  <= 30:
                print('MSG: Bus voltage within safe range. OK. value: %.2f' %  dc_bus_value)
            else:
                raise Exception('WAR: Bus voltage to high... try #: %s , value: %.2f' % (attempt, dc_bus_value))
    
            # Read idle power
            idle_power = bk_instrument.read_magnitude('power')
            
            # Enter in PFC FETs test mode
            modbus_testing_mode.enable(TESTING_MODE_MOSFET_BRIDGES)
            print('MSG: Entering in PFC mosfet test mode')

            p1.write(MODBUS_TESTING_MOSFETS_ORDERS, fet_modbus_testing_mode, 0, False) 
            print('MSG: %s Closed' % leg_to_test)
            
            # Close precharge relay
            modbus_testing_mode.enable(TESTING_MODE_PRECHARGE_RLY_CLOSE)
            sleep(1)
            
            timeout_cnt = 0
            new_power = bk_instrument.read_magnitude('power', True)

            while (new_power - idle_power) < 7:
                new_power = bk_instrument.read_magnitude('power', True)
                sleep(1)
                timeout_cnt += 1
                print('MSG: Waiting for Precharge relay to close...')
                if timeout_cnt > 5:
                    raise Exception('WAR: The Precharge relay is not closing.')
    
            else:
                print('MSG: The Precharge relay must be closed.')
    
            # Open the precharge relay
            modbus_testing_mode.disable(TESTING_MODE_PRECHARGE_RLY_CLOSE)
            
            # Wait until precharge relay opens
            timeout_cnt = 0
            while (bk_instrument.read_magnitude('power', True) - idle_power) > 3:
                sleep(1)
                timeout_cnt += 1
                print('MSG: Waiting for Precharge relay to open...')
    
                if timeout_cnt > 5:
                    raise Exception('WAR: The Precharge relay is not opening.')
    
            else:
                print('MSG: The Precharge relay must be open.')
            
            
            # Short circuit consumes 4W aprox
            delta_power = new_power - idle_power
    
            if delta_power > 5:
                print('MSG: %s OK. delta_power: %.2f' % (leg_to_test, delta_power))
                modbus_testing_mode.safe_state() # Send the DSP to set state
                
            else:
                raise Exception('WAR: %s close seem to be failing... try #: %s , delta_power: %.2f' % (leg_to_test, attempt, delta_power))
    
        except Exception as error:
            # we fail an attemp - wait or print error and retry
            print(error)
            sleep(2)
            
        else:
            # Everythig ok, continue
            break
    else:
        # we failed all the attempts - deal with the consequences.
        modbus_testing_mode.safe_state() # Send the DSP to set state
        raise Exception(colored('ERR: %s FAIL.' %  leg_to_test, 'red'))


def test_secondary_bridge(leg_to_test):
    # TEST: Secondary B2 MOSFET test and battery current measure test  
    BAT_VOLTAGE_SETTING = 5
    BAT_CURRENT_LIMIT = 1
    
    itech_instrument.set_voltage(BAT_VOLTAGE_SETTING)
    itech_instrument.set_current_limit_pos(BAT_CURRENT_LIMIT)
    itech_instrument.set_current_limit_neg(-BAT_CURRENT_LIMIT)
    
    itech_instrument.output_state('OFF')
    sleep(1)
    
    if leg_to_test == 'dcdc_secondary_leg1':
        fet_modbus_testing_mode = TESTING_MODE_MOSFET_CTRL_B2_1A + TESTING_MODE_MOSFET_CTRL_B2_1B # Close FETs
    
    elif leg_to_test == 'dcdc_secondary_leg2':
        fet_modbus_testing_mode = TESTING_MODE_MOSFET_CTRL_B2_2A + TESTING_MODE_MOSFET_CTRL_B2_2B # Close FETs
    
    else:
        raise Exception('ERR: Wrong leg to test, should be dcdc_secondary_leg1 or dcdc_secondary_leg2.')
    
    
    for attempt in range(10): # Retry 10 times
        try:
            modbus_testing_mode.safe_state()
            # modbus_testing_mode.enable(TESTING_MODE_ORDER_SCALE_MODE_EOL)
    
            dc_bat_voltage = p1.read(MODBUS_L1_DC_VOLTAGE, 2, True)
            # Check if bus is discharged
            if dc_bat_voltage  <= 20:
                print('MSG: DC voltage within safe range. OK. value: %.2f' %  dc_bat_voltage)
            else:
                raise Exception('WAR: DC voltage to high... try #: %s , value: %.2f' % (attempt, dc_bat_voltage))
    
           
            modbus_testing_mode.enable(TESTING_MODE_MOSFET_BRIDGES)
            print('MSG: Entering in FETs test mode')
    
            p1.write(MODBUS_TESTING_MOSFETS_ORDERS, fet_modbus_testing_mode, 0, False) 
            print('MSG: %s Closed' % leg_to_test)
    
            # Lock gun and close D1 D2
            modbus_testing_mode.enable(TESTING_MODE_SET_GUN_LOCK)
            modbus_testing_mode.enable(TESTING_MODE_D1_D2_RLY_CLOSE)
            print('MSG: Closing output relays')
            
            # Apply output voltage
            itech_instrument.output_state('ON')
            sleep(1)
    
            timeout_cnt = 0
            new_current = abs(itech_instrument.read_magnitude('current', True))

            while new_current < BAT_CURRENT_LIMIT*.8:
                new_current = abs(itech_instrument.read_magnitude('current', True))

                sleep(1)
                timeout_cnt += 1
                print('MSG: Waiting for output relays relay to close...')
                if timeout_cnt > 5:
                    raise Exception('WAR: The output relays relay is not closing.')
    
            else:
                print('MSG: The output relays relay must be closed.')
    
            
            # Remove output voltage and open relays
            itech_instrument.output_state('OFF')
            modbus_testing_mode.safe_state()
            print('MSG: Open FETs')
    
            # Read idle power
            idle_current = abs(itech_instrument.read_magnitude('current'))
            timeout_cnt = 0
            while idle_current > BAT_CURRENT_LIMIT*.1:
                idle_current = abs(itech_instrument.read_magnitude('current'))
                sleep(1)
                timeout_cnt += 1
                print('MSG: Waiting for output relays relay to open...')
    
                if timeout_cnt > 5:
                    raise Exception('WAR: The output relays relay is not opening.')
    
            else:
                print('MSG: The output relays relay must be open.')
            
            
            delta_current = new_current - idle_current
    
            if delta_current > BAT_CURRENT_LIMIT*.9:
                print('MSG: %s OK. delta_current: %.2f' % (leg_to_test, delta_current))
                modbus_testing_mode.safe_state() # Send the DSP to set state

            else:
                raise Exception('WAR: %s close seem to be failing... try #: %s , delta_power: %.2f' % (leg_to_test, attempt, delta_current))
    
        except Exception as error:
            # we fail an attemp - wait or print error and retry
            print(error)
            sleep(2)
            
        else:
            # Everythig ok, continue
            break
    else:
        # we failed all the attempts - deal with the consequences.
        
        modbus_testing_mode.safe_state() # Send the DSP to set state
        raise Exception(colored('ERR: %s close FAIL. delta_power: %.2f' %  (leg_to_test, delta_current), 'red'))


def calibration():
    itech_instrument.output_state('OFF')
    #modbus_testing_mode.safe_state()
    
    sleep(1) # Wait voltage to go down
    modbus_testing_mode.disable(TESTING_MODE_ORDER_SCALE_MODE_EOL)
    
    
    GRID_VOLTAGE_SETTING = 230
    GRID_FREQ_SETTING = 50
    GRID_CURRENT_LIMIT = 33
    
    BAT_VOLTAGE_SETTING = 360 
    BAT_CURRENT_LIMIT = 20
  
    # Set the BK Power Source
    bk_instrument.set_ac_voltage(GRID_VOLTAGE_SETTING)
    bk_instrument.set_ac_freq(GRID_FREQ_SETTING)
    #bk_instrument.set_current_limit(GRID_CURRENT_LIMIT)
    
    #bk_instrument.read_errors()
    sleep(2)
    
    # Set the ITECH Power Source
    itech_instrument.set_voltage(BAT_VOLTAGE_SETTING)
    itech_instrument.set_current_limit_pos(BAT_CURRENT_LIMIT)
    itech_instrument.set_current_limit_neg(-BAT_CURRENT_LIMIT)

    while True:
        if p1.read(MODBUS_SYSTEM_BOOT_STATUS, 0, False) == 50:
            itech_instrument.output_state('ON')
            break
        else:
            sleep(5)
            print('MSG: Waiting for offset calibration.')


    sleep(2)
    itech_instrument.output_state('ON')

    p1.unlock_calibration()

    p1.unlock_operator_mode()
    p1.write(50, 1, 0, False)

    #modbus_testing_mode = test_mode()
    #p1.unlock_test_mode()
    modbus_testing_mode.enable(TESTING_MODE_SET_GUN_LOCK)
    modbus_testing_mode.enable(TESTING_MODE_D1_D2_RLY_CLOSE)

    
    # Checklist verifies that there is DC voltage and no errors
    for attempt in range(10): # Retry 10 times
        try:
            
            # Tell the charger the calibration process is going to start
            p1.write(MODBUS_PS_ACTIONS_REGISTER, 64, 0, False)
    
            # Should return a 71
            charger_status = p1.read(MODBUS_SYSTEM_BOOT_STATUS, 0, False)
    
            print('Charger status (Should be 71): %s'% charger_status)
    
            if 71 != (charger_status): # Fail if conditions aren't met
                raise Exception('MSG: Charging initializing... try #: %s'% attempt)
    
        except Exception as error:
            # we fail an attemp - wait or print error and retry
            print(error)
            sleep(5)
            
        else:
            # Everythig ok, continue
            break
    else:
        # we failed all the attempts - deal with the consequences.
        raise Exception(colored("ERR: Charger unready.", 'red'))
    
    
    for attempt in range(10): # Retry 10 times
        try:
            # Check if eprom was written
            calibration_status = p1.read(MODBUS_CHARGER_MESSAGES, 0, False) >> 3 & 1

            if calibration_status == 1:
                print(colored('MSG: Calibration init succesfull (reg#182): must be 1, result: %d' % calibration_status,'green'))
            else:
                raise Exception('MSG: Calibration start failed (reg#182): must be 1, result: %d' % calibration_status)      

        except Exception as error:
            # we fail an attemp - wait or print error and retry
            print(error)
            sleep(1)
            
        else:
            # Everythig ok, continue
            break
    else:
        # we failed all the attempts - deal with the consequences.
        raise Exception(colored('ERR: Calibration failed (reg#182): must be 0, result: %d' % calibration_status, 'red'))
    
    
    # Calibrate Charger Voltages
    VGRID = bk_instrument.read_magnitude('voltage')
    VBATT = itech_instrument.read_magnitude('voltage')
    
    # Check the measured values are within range
    if abs(VGRID - GRID_VOLTAGE_SETTING) <= .05* GRID_VOLTAGE_SETTING and\
        abs(VBATT - BAT_VOLTAGE_SETTING) <= .05* BAT_VOLTAGE_SETTING:
        pass
    else:
        raise Exception('ERR: The calibration Voltages are out of range.')
    
    p1.write_calibration_voltages(VGRID, VBATT)
    
    sleep(2) # Wait some time before reading
    '''
    bk_instrument.set_ac_voltage(100)
    bk_instrument.set_ac_freq(GRID_FREQ_SETTING)

    itech_instrument.set_voltage(195)

    modbus_testing_mode.enable(TESTING_MODE_ORDER_SCALE_MODE_EOL)
    '''
    
    # Prepare instrument to calibrate current
    p1.write(23, 40, 0, False)
    p1.write(21, 3200, 0, False)
    p1.write(46, 1, 0, False)
    p1.write(MODBUS_PS_ACTIONS_REGISTER, 65, 0, False)

    sleep(5) # Wait some time before reading
    
    IGRID = abs(bk_instrument.read_magnitude('current'))
    IBATT = abs(itech_instrument.read_magnitude('current'))

    
    p1.write_calibration_currents(IGRID, IBATT)
      
    # Check the measured values are within range
    if abs(IGRID) <= 2 or abs(IBATT) <= 2:
        raise Exception('ERR: The calibration Currents are to low.')
    
    
    p1.write_calibration_currents(IGRID, IBATT)

    sleep(2)
    
    for attempt in range(10): # Retry 10 times
        try:
            # Check if eprom was written
            calibration_status = p1.read(MODBUS_CHARGER_MESSAGES, 0, False) >> 3 & 1

            if calibration_status == 0:
                print(colored('MSG: Calibration succesfull (reg#182): must be 0, result: %d' % calibration_status, 'green'))
                itech_instrument.output_state('OFF')
                modbus_testing_mode.safe_state() # Send the DSP to safe state    

            else:
                raise Exception('WAR: Calibration failed (reg#182): must be 0, result: %d' % calibration_status)      

        except Exception as error:
            # we fail an attemp - wait or print error and retry
            print(error)
            sleep(1)
            
        else:
            # Everythig ok, continue
            break
    else:
        # we failed all the attempts - deal with the consequences.
        itech_instrument.output_state('OFF')
        raise Exception(colored('ERR: Calibration start failed (reg#182): must be 0, result: %d' % calibration_status, 'red'))
    
    
    '''
    # Read if calibration is successful
    # Should return a 0
    calibration_result = p1.read(MODBUS_POWER_DERATING_STATUS, 0, False)
    if calibration_result == 1:
        print('ERR: Calibration failed (reg#213): must be 0, result: %d' % calibration_result)
    else:
        print('MSG: Calibration succesfull (reg#213): must be 0, result: %d' % calibration_result)
        p1.set_op(START) # Basically, start and stop
        p1.set_current(17)    # No limit on current
        p1.set_power(40)  # Set the power according to the table
        p1.update_values() # Get values from the converter
    
    sleep(2)
    print('MSG: Calibration result: must be 0, result: %d' % p1.read(MODBUS_SYSTEM_BOOT_STATUS, 0, False))
    '''
    # itech_instrument.output_state('OFF')

    

'''
##############################################################################
# TEST: Charge and discharge bus test
# TODO: This test can oly be run if MOSFETS are ok an precharge relay
for attempt in range(10): # Retry 10 times
    try:
        p1.write(MODBUS_TESTING_MODE_ORDER_LO, 0x0000, 0, False) # Go to safe state
        p1.write(MODBUS_TESTING_MOSFETS_ORDERS, 0x0000, 0, False) # Go to safe state
        sleep(1)

        modbus_testing_mode = 0x0200 # Close the precharge relay and mosfet PFC mosfet test mode
        p1.write(MODBUS_TESTING_MODE_ORDER_LO, modbus_testing_mode, 0, False) # Close precharge relay
        sleep(1)
        
        timeout_cnt = 0
        dc_bus_value = p1.read(MODBUS_L1_DC_BUS_VOLTAGE, 0, False) / 10.0
        while (abs(dc_bus_value- GRID_VOLTAGE_SETTING * 1.41)  >= GRID_VOLTAGE_SETTING  * .5):
            
            sleep(1)
            timeout_cnt += 1
            dc_bus_value = p1.read(MODBUS_L1_DC_BUS_VOLTAGE, 0, False) / 10.0
            print('MSG: Bus voltage charging. value: %.2f' %  dc_bus_value)
            
            print('MSG: Waiting for Bus to charge...')
            if timeout_cnt > 20:
                raise Exception('WAR: The Bus is not charging.')

        else:
            dc_bus_value = p1.read(MODBUS_L1_DC_BUS_VOLTAGE, 0, False) / 10.0
            print('MSG: Bus voltage. OK. value: %.2f' %  dc_bus_value)
        
        # Open Precharge relay but stay in PFC mosfet test mode
        modbus_testing_mode = 0x8000
        p1.write(MODBUS_TESTING_MODE_ORDER_LO, modbus_testing_mode, 0, False)
        sleep(5)

        # Try to discharge the bus by switching PFC FETs
        print('MSG: Discharging BUS, start time %s' % datetime.now())
        while (p1.read(MODBUS_L1_DC_BUS_VOLTAGE, 0, False) / 10.0) > 10:
            modbus_testing_mode = TESTING_MODE_MOSFET_CTRL_PFC_1A + TESTING_MODE_MOSFET_CTRL_B1_1A + TESTING_MODE_MOSFET_CTRL_B1_2A
            p1.write(MODBUS_TESTING_MOSFETS_ORDERS, modbus_testing_mode, 0, False) 

            modbus_testing_mode = TESTING_MODE_MOSFET_CTRL_PFC_1B + TESTING_MODE_MOSFET_CTRL_B1_1B + TESTING_MODE_MOSFET_CTRL_B1_2B
            p1.write(MODBUS_TESTING_MOSFETS_ORDERS, modbus_testing_mode, 0, False)

            # dc_bus_value = p1.read(MODBUS_L1_DC_BUS_VOLTAGE, 0, False) / 10.0
            # print('MSG: Bus voltage. OK. value: %.2f' %  dc_bus_value)
        
        else:
            print('MSG: Discharging BUS, end time %s' % datetime.now())
                          


    except Exception as error:
        # we fail an attemp - wait or print error and retry
        print(error)
        sleep(2)
        
    else:
        # Everythig ok, continue
        break
else:
    # we failed all the attempts - deal with the consequences.
    modbus_testing_mode = TESTING_MODE_ORDER_DO_NOTHING
    p1.write(MODBUS_TESTING_MODE_ORDER_LO, modbus_testing_mode, 0, False) # Go to safe state
    p1.write(MODBUS_TESTING_MOSFETS_ORDERS, modbus_testing_mode, 0, False) # Open PFC 1B and PFC 2B
    raise Exception('ERR: Bus charge discharge. FAIL')

'''


if __name__ == '__main__':
    # initiate the parser
    parser = argparse.ArgumentParser()
    parser.add_argument("-V", "--version", help="show program version", action="store_true")
    parser.add_argument("-T", "--testing", help="enables the charger testing", action="store_true")
    parser.add_argument("-C", "--calibration", help="enables the charger calibration", action="store_true")
    parser.add_argument("-c", "--charger", help="charger number, ex: WB012345", default='WB037823')
    parser.add_argument("-F", "--flash", help="enables DSP flashing", action="store_true")
    parser.add_argument("-I", "--install", help="Install Mbus service and disables micro2wallbox", action="store_true")
    parser.add_argument("-E", "--ethtool", help="Install ethtool service to fix eth speed to 10Mbps", action="store_true")
    parser.add_argument("-W", "--wifi", help="Connect charger to Wallbox WiFi", action="store_true")
    parser.add_argument("-R", "--remove", help="Removes Mbus service and enables micro2wallbox", action="store_true")
    parser.add_argument("-RW", "--remove_wifi", help="Removes WiFi APs configurations", action="store_true")

    
    # read arguments from the command line
    args = parser.parse_args()
    # print(args)
    
    if args.version:
        print("MSG: This is Version %s." % SCRIPT_VERSION)
    
    CHARGER_NUMBER = args.charger


    # Check if is an ip or a valid wb number
    if re.match("^(\d{0,3})\.(\d{0,3})\.(\d{0,3})\.(\d{0,3})$", CHARGER_NUMBER):
        HOST_IP = CHARGER_NUMBER
    elif args.charger == 'WB000000' or not(len(args.charger) == 8): # TODO: improve verification
        raise Exception('ERR: No valid Wallbox charger number especified.')
    else:
        HOST_IP = socket.gethostbyname(HOST_IP)

    HOST_MAC = get_mac_address(ip=HOST_IP)



    LOGS_FOLDER = './logs_files/'
    LOG_FILE_NAME = LOGS_FOLDER + CHARGER_NUMBER + time.strftime("_%Y%m%d%H%M") + '.log'
    Path(LOGS_FOLDER).mkdir(parents=True, exist_ok=True)
    sys.stdout = Logger()


    
    print('Script version: %s' % SCRIPT_VERSION)
    print('Start time %s' % time.strftime("%Y-%m-%d %H:%M"))
    print('MSG: HOST IP: %s' % HOST_IP)
    print('MSG: HOST MAC: %s' % HOST_MAC)


    ssh_cli = ssh_client(HOST_IP)

    if args.remove:
        remove_mbus(ssh_cli)

    if args.ethtool:
        install_ethtool(ssh_cli)

    if args.wifi:
        connect_wifi_to_wallbox(ssh_cli)

    if args.install:
        install_mbus(ssh_cli)
        reset_dsp(ssh_cli)


    if args.flash:
        firmware_file = 'aux_files/uCharger_v2002_final_v1.txt'
        flash_dsp(ssh_cli, firmware_file)
    
        reset_dsp(ssh_cli)
        
           

    if args.testing or args.calibration:
        bk_instrument = init_ac_source(BK_ADDRESS)
        itech_instrument = init_dc_source(ITECH_ADDRESS)

        class test_mode:
            def enable(self, command):
                # Enable and Send command 
                self.testing_mode |= command
                
                if command == TESTING_MODE_D1_D2_RLY_CLOSE: # TODO: fix me
                    itech_instrument.output_state('ON')
        
                
                p1.write(MODBUS_TESTING_MODE_ORDER_LO, self.testing_mode, 0, False)
                
        
            def disable(self, command):
                # Disable and send command 
                self.testing_mode &= ~ command
                
                if command == TESTING_MODE_D1_D2_RLY_CLOSE: # TODO: fix me
                    itech_instrument.output_state('OFF')
                    
                p1.write(MODBUS_TESTING_MODE_ORDER_LO, self.testing_mode, 0, False)
            
            def safe_state(self):
                self.testing_mode = TESTING_MODE_ORDER_DO_NOTHING
                
                itech_instrument.output_state('OFF') # TODO: fix me
                    
                p1.write(MODBUS_TESTING_MODE_ORDER_LO, TESTING_MODE_SET_GUN_RELEASE, 0, False) # Unlock gun
                p1.write(MODBUS_TESTING_MODE_ORDER_LO, TESTING_MODE_ORDER_DO_NOTHING, 0, False) # Go to safe state
                p1.write(MODBUS_TESTING_MOSFETS_ORDERS, TESTING_MODE_ORDER_DO_NOTHING, 0, False) # Open FETs
                self.enable(TESTING_MODE_ORDER_SCALE_MODE_EOL)
         
            def __init__(self):
                self.safe_state()
        
        
        print(colored('\nMSG: START INITIALIZING CHARGER.', 'blue'))
        sleep(2)
        p1 = quasar('TEST', HOST_IP)
        
        p1.unlock_maintanance()
        
        
        while True:
            if p1.read(MODBUS_SYSTEM_BOOT_STATUS, 0, False) > 20:
                itech_instrument.output_state('ON')
                break
            else:
                sleep(5)
                print('MSG: Waiting for offset calibration.')
        
        
        #p1.checklist()
        p1.unlock_test_mode()
        modbus_testing_mode = test_mode()
        
        
        p1.set_current_mode()
        p1.set_current(17)    # No limit on current
        p1.set_power(40)  # Set the power according to the table
        print(colored('\nMSG: FINISH INITIALIZING CHARGER.\n', 'blue'))


    if args.testing == True:
        print(colored('\nMSG: STARTING CHARGER TESTING.', 'blue'))

        print('####################################################')
        test_communication()
        
        print('####################################################')
        test_dsp_integrity()
        
        print('####################################################')
        test_rotary_switch()
        
        print('####################################################')
        test_uk99()
        
        print('####################################################')
        test_fan()   
        
        print('####################################################')
        test_gun_selector()
        
        print('####################################################')
        test_gun_lock()
        
        print('####################################################')
        test_gun_unlock()
        
        print('####################################################')
        test_chademo_proximity()
        
        print('####################################################')
        test_chademo_vehicle_permission()
        
        print('####################################################')
        test_primary_bridge('pfc_leg1')
        sleep(5)
        
        print('####################################################')
        test_primary_bridge('pfc_leg2')
        sleep(5)
        
        print('####################################################')
        test_primary_bridge('dcdc_primary_leg1')
        sleep(5)
        
        print('####################################################')
        test_primary_bridge('dcdc_primary_leg2')
    
        print('####################################################')
        test_secondary_bridge('dcdc_secondary_leg1')
        
        print('####################################################')
        test_secondary_bridge('dcdc_secondary_leg2')
        
        print('####################################################')
        test_measurement_grid_voltage()
        
        print('####################################################')
        test_measurement_batery_voltage()
    
        print('####################################################')
        #test_precharge_relay()
           
        print('####################################################')
        #test_main_relay() 
        
        print(colored('\nMSG: FINISH CHARGER TESTING. \n', 'blue'))

    if args.calibration == True:
        print(colored('\nMSG: STARTING CHARGER CALIBRATION.', 'blue'))
        calibration()
        print(colored('\nMSG: FINISH CHARGER CALIBRATION.\n', 'blue'))

    if args.remove_wifi == True:
        remove_wifi_aps(ssh_client)

    del itech_instrument
    del bk_instrument


    

    print('End time %s' % time.strftime("%Y-%m-%d %H:%M"))
    
 
    
    
    
