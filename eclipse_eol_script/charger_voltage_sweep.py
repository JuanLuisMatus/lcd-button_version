from QuasarClass import quasar, START, STOP
from time import sleep, time
from instruments import yokogawa, keysight, detect_coms

# Change this variable to the address of your instrument
KEYSIGHT_ADDRESS = 'USB0::0x2A8D::0x1776::MY58262486::0::INSTR'
YOKOGAWA_ADDRESS = 'TCPIP::192.168.2.241::inst0::INSTR'
GRID_ELEMENT = '4'
BATT_ELEMENT = '3'
    
yoko_instrument = yokogawa(YOKOGAWA_ADDRESS, GRID_ELEMENT, BATT_ELEMENT)
keysight_instrument = keysight(KEYSIGHT_ADDRESS)


# Third number is power
endurance_ops = []
for x in range(10):
    endurance_ops.append(["charge", 10, 10])
    endurance_ops.append(["charge", 10, 20])
    endurance_ops.append(["charge", 10, 30])
    endurance_ops.append(["charge", 10, 40])
    endurance_ops.append(["charge", 10, 50])
    endurance_ops.append(["charge", 10, 60])
    endurance_ops.append(["charge", 10, 70])
    endurance_ops.append(["charge", 10, 80])
    endurance_ops.append(["charge", 10, 90])
    endurance_ops.append(["rest", 10, 0])

# Initialize the quasar. Arguments are: Name of the converter, port and slave number (keep 10)
p1 = quasar('TEST', detect_coms()[0], 10)

# Put the password to unlock 
p1.unlock()

# Set the quasar in current mode
p1.set_current_mode()

# Checklist verifies that there is DC voltage and no errors
p1.checklist()

# From now on, executes the script
# Transform table in something machine friendly
machine_ops = []
acum_time = 0
for element in endurance_ops:
    acum_time += element[1]
    if element[0] == "charge":
        op = START
        multiplier = 1.0
    elif element[0] == "discharge":
        op = START
        multiplier = -1.0
    else:
        op = STOP
        multiplier = 0.0
    machine_ops.append([op,acum_time,multiplier*abs(element[2])])
# End of transformation


# Run the endurance
start_time = time()
for element in machine_ops:
    while (time() - start_time < element[1]) and (not(p1.is_in_error())):
        p1.set_op(element[0]) # Basically, start and stop
        p1.set_current(17)    # No limit on current
        p1.set_power(element[2])  # Set the power according to the table
        p1.update_values() # Get values from the converter
        p1.print_stats() # Some stats are printed using the class
        
        file_suffix = element[2]
        keysight_instrument.save_capture(file_suffix)

        print ("Remaining time: {:6.0f} s".format(machine_ops[-1][1] - (time() - start_time)))
        print ("\nGenerate a failure to stop, i.e. disconnect usb cable (then wait 10s) or trip the AC breaker.")
        try:
            sleep(5)
            if p1.is_in_error():
                p1.reset() # Reset manager is autonomous in the class. Resets are limited in number. 
        except KeyboardInterrupt:
            p1.raise_error("Terminated by user")
            
data = p1.terminate() # Stop the quasar
del p1 # Destroy the instance and free allocated memory






