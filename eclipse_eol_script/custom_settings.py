
def get_devices_addresses(STATION_NUMBER):
    if STATION_NUMBER == 1:
        # Change this variable to the address of your instrument
        BK_ADDRESS = 'USB0::0xFFFF::0x7749::526E19103::0::INSTR'
        ITECH_ADDRESS = 'USB0::0x2EC7::0x6000::803384022747250002::0::INSTR'    
    elif STATION_NUMBER == 2:
        # Change this variable to the address of your instrument
        BK_ADDRESS = 'USB0::0xFFFF::0x7749::526G19103::0::INSTR'
        ITECH_ADDRESS = 'USB0::0x2EC7::0x6000::803384022747230007::0::INSTR' 
    return BK_ADDRESS, ITECH_ADDRESS