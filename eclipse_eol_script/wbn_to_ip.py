import sys

NET_PREFIX = '10.0.'

def wbn_to_ip(wallbox_num):
    if wallbox_num[0:3] != 'WB0' or len(wallbox_num) != 8:
        raise Exception('Wrong Wallbox number, check it.')
    else:
        wallbox_num = int(wallbox_num[-6:])
        bin_addr = '{0:16b}'.format(wallbox_num)
        low_addr = bin_addr[-8:]
        high_addr = bin_addr[:-8]
        
        low_addr =  str(int(low_addr, 2))
        high_addr = str(int(high_addr, 2))
        
        ip_addr = (NET_PREFIX + high_addr + '.' + low_addr)
            
        return ip_addr


if __name__ == '__main__':
    #print(sys.argv[1])
    print(wbn_to_ip(str(sys.argv[1])), end = '')
