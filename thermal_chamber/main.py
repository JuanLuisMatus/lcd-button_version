#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Thermal chamber controler

@author: Emiliano Penovi
@company: WallBox
@date: 2020.06.04

Modified by J. Matus 2021.12.14
"""
import sys
sys.path.append('../lib') # Inlcudes the path to the user libraries folder
sys.path.append('../aux_files') # Inlcudes the path to the user libraries folder

# std library imports
import os
import csv
import time
import signal
import ctypes
import logging
import threading

# local imports
import driver_picolog
import drivers

# third party imports
import plac
from gpiozero import PWMLED as PWM
from gpiozero import CPUTemperature
from simple_pid import PID

# Load the driver and set it to "display"
# If you use something from the driver library use the "display." prefix first
display = drivers.Lcd()


class ChamberDriver(threading.Thread):
    """
    Class for temperature controller of Climatic Chamber.

    It runs the control loop as a background thread, so the main application must block
    execution while the driver is running.
    """

    def __init__(self, fan_gpio, heater_gpio, update_frequency, setpoint=[43.0,]):
        # Initialize parent class
        super().__init__()

        self.name = 'Climatic Chamber'
        self.starting_time = time.time()
        self.last_save_time = 0
        self.save_file_period = 60
         # dictionary to store data
        self.data = {}

        # data needed to iterate setpoint profile at startup
        if setpoint:
            self._setp = setpoint
            self._setp_k = 0
        else:
            raise ValueError("Setpoint must be a non empty list")

        # store temeperature data internally
        self.temps = [0.0, 0.0, 0.0, 0.0]

        # peripherals initialization code
        self.picolog_drv = driver_picolog.cmp_driver({'sn':'A0059/260'})

        # to wait for event to happen
        self.stop_event = threading.Event()
        self._st = 0.0

        # continuous time PID controller gains
        KP = 0.05
        KI = 0.001
        KD = 0.0005
        self._fs = float(update_frequency)
        self._Ts = 1.0 / self._fs

        # setup PID controller
        self.pid = PID(KP, KI, KD)
        self.pid.sample_time = self._Ts
        self.pid.output_limits = (0, 2)    # output ranges from -100% to 100%

        self.pid.set_auto_mode(False)
        self.pid.set_auto_mode(True, last_output=1.5) # set initial condition


        # setup PWM output
        self.pwm_heater = PWM(int(heater_gpio))
        self.pwm_heater.frequency = self._fs
        self.pwm_heater.value = 0
        self.pwm_heater.off()
                
        self.pwm_fan = PWM(int(fan_gpio))
        self.pwm_fan.frequency = self._fs
        self.pwm_fan.value = 0
        self.pwm_fan.off()
        
        self.control = 0

    def run(self):
        """
        This method creates a periodic time base using a wait on event strategy,
        don't call this method directly, call start() instead
        """
        # try to improve time accuracy using perf_counter, 
        # time accuracy will depend on the underlaying operating system
        while True:
            self._st = time.perf_counter()

            # run periodic user code
            self.control_loop()

            # wait for a stop event, compensate time required for code execution
            if self.stop_event.wait(self._Ts - (time.perf_counter() - self._st)):
                break

    def stop(self):
        """Stop all threads to exit controller"""
        self.stop_event.set()
        self.join()

    def control_loop(self):
        """Control main loop"""

         # update temperature measurement
        self.picolog_drv.update_step()

        # read inputs
        for i in range(0, 4):
            self.temps[i] = self.picolog_drv.channel_temp[i+1]

        # follow setpoint profile if present
        if len(self._setp) > 1:
            if self._setp_k < len(self._setp):
                self.pid.setpoint = self._setp[self._setp_k]
                # increment reference pointer
                self._setp_k = self._setp_k + 1
            else:
                # use last value of setpoint profile
                self._setp[-1]
        else:
            self.pid.setpoint = self._setp[0]

        # execute control algorithm
        self.control = 1.0 - self.pid(self.temps[1])
                
        # update control output
        if self.control >= 0:
            self.pwm_fan.value = self.control
            self.pwm_heater.value = 0
            
        else:
            self.pwm_fan.value = 0
            self.pwm_heater.value = - self.control
    
        #self.pwm_heater.value = 1
    
    
    def restart_setpoint(self):
        self._setp_k = 0

    def __str__(self):
        """returns internal information to print to screen"""
        temps_str = ", ".join([f"T{i+1} ={temp: 5.2f}°C" for i, temp in enumerate(self.temps)])
        return f"{self.name}: SP ={self.pid.setpoint: 6.2f}°C, CA ={self.control * 100.0: 6.2f}%, {temps_str}"


def setup_pydev_dbg_new(rmt_addr='localhost'):
    """
    Use new method to conect to pydevd with python 3.7

    see: https://github.com/fabioz/PyDev.Debugger/
    """
    import pydevd
    
    try:
        #pydevd.settrace(host=rmt_addr)
        #breakpoint()
        pass
    except Exception as error:
        logging.warning('Can\'t connect to remote eclipse debug server. %s' % error)


def setup_pydev_dbg(rmt_addr='localhost'):
    """
    Setup remote debuggin session from pydev

    rmt_ip: IP address of remote machine
    more info:
    - https://sites.google.com/site/programmersnotebook/remote-development-of-python-scripts-on-raspberry-pi-with-eclipse

    Instead of copying pydevd souce files manually, these files can be installed on the remote
    machine using pip, see: https://github.com/fabioz/PyDev.Debugger/
    """
    # append pydev folder to path
    sys.path.append('./pysrc')

    # this is to remotely debug, it has to be removed
    try:
        import pydevd

        # use address for remote machine
        pydevd.settrace(rmt_addr)
    except Exception as error:
        # update path as it is not needed anymore
        sys.path.remove('./pysrc')
        logging.warning('Can\'t connect to remote eclipse debug server. %s' % error)


def init_logger(fmt_str, init_level=logging.INFO, file_name='logfile.log'):
    """
    Initialize the application logger

    DEBUG    Detailed information, typically of interest only when diagnosing problems.
    INFO     Confirmation that things are working as expected.
    WARNING  An indication that something unexpected happened, or indicative of some problem in the near future (e.g. disk space low). The software is still working as expected.
    ERROR    Due to a more serious problem, the software has not been able to perform some function.
    CRITICAL A serious error, indicating that the program itself may be unable to continue running.
    """
    # setup handlers, log to console also...
    handlers = [
        logging.FileHandler(filename=file_name),
        logging.StreamHandler(sys.stdout),
    ]

    logging.basicConfig(format=fmt_str, level=init_level, handlers=handlers)


def read_nl5_csv(csv_file):
    log = logging.getLogger()

    csv_data = {}

    try:
        with open(csv_file, mode='r') as f:
            for row in csv.DictReader(f, delimiter=','):
                for key in row:
                    value = float(row[key])
                    csv_data.setdefault(key, []).append(value)
    except IOError:
        log.warning(f'"{csv_file}" file not found on disk')

    return csv_data


def handle_signals(signum, frame):
    sys.exit(0)


@plac.annotations(
    setpoint_profile=plac.Annotation(
        'Name of a .csv file supplying a time dependent setpoint profile', 'option', 'c', str
    ),
    fan_gpio=plac.Annotation(
        'RPi gpio number for pwm fan driver', 'option', 'g', int
    ),
    heater_gpio=plac.Annotation(
        'RPi gpio number for pwm heater driver', 'option', 'p', int
    ),
    def_temp=plac.Annotation(
        'Temperature [ºC] to control if setpoint_profile.csv is not present', 'option', 't', float
    ),
    ctrl_freq=plac.Annotation(
        'Frequency [Hz] setting for pwm fan driver', 'option', 'f', float
    ),
    verbose=plac.Annotation(
        'Show debugging data to console', 'flag', 'v'
    )
)

def main(setpoint_profile='setpoint_profile.csv', fan_gpio=17, heater_gpio=26, ctrl_freq=5.0, verbose=False, def_temp=40):
    """
    Run thermal chamber controller
    """
    signal.signal(signal.SIGQUIT, handle_signals)
    signal.signal(signal.SIGTERM, handle_signals)

    fmt = '%(asctime)s\t%(levelname)s -- %(filename)s:%(lineno)s -- %(message)s'
    init_logger(fmt)

    if verbose:
        print('MSG: Starting script')

    # Initialized pydev remote debugging
    setup_pydev_dbg_new()

    # load reference profile from csv file
    csv_file = setpoint_profile

    # read setpoint profile from disk
    csv_data = read_nl5_csv(csv_file)

    # Create the climatic chamber object
    sp_key = "V(setpoint)"
    setpoint = csv_data.get(sp_key, [def_temp, ])

    drv = ChamberDriver(fan_gpio, heater_gpio, ctrl_freq, setpoint=setpoint)

    try:
        # run climatic chamber control loop
        drv.start()

        # block this thread
        while True:
            # show evolution of data to user
            if verbose:
                print(str(drv), end="\r", flush=True)
                display.lcd_display_string(str(drv), 1)  # Write line of text to first line of display
            # wait for some time to display new data
            time.sleep(1)

    except (KeyboardInterrupt, SystemExit):
        pass
    finally:
        drv.stop()

    if verbose:
        print()
        print('MSG: Stopping script')

    return 0


if __name__ == '__main__':
    plac.call(main)
