#!/usr/bin/sh
export PYTHONBREAKPOINT=pydevd.settrace
export PATHS_FROM_ECLIPSE_TO_PYTHON="[[\"/home/jf/liclipse/workspace/thermal_chamber/\",\"/home/alarm/thermal_chamber\"]]"

python3 $1 ${@:2}

