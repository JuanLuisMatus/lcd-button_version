# Climatic Chamber control system
This script is a command line program that allows controlling temperature in Awesome Penovi's Climatic Chamber in a Box setup.

## How to run
Make sure to give execution permissions to the script (`chmod +x main.py` on a console). Then run
```
    ~$ ./main.py -h
```
to show the help screen. Available commands and their default values are printed as a help message.

If the user wants to set a setpoint profile, just copy a csv file generated by NL5 named `setpoint_profile.csv` in the same folder that this script is located. If the name is different run the script as:
```
    ~$ ./main.py -c data.csv
```
Remember that the script search data in a variable named `"V(setpoint)"` inside the csv file.

## Dependencies
The script is written in Python 3 for the Raspberry Pi platform. It requieres the following third party modules:

1. *plac*: library to aid the implementation of command line interfaces.
```
    ~$ pip3 install plac
```
2. *gpiozero*: for using hardware features on the Raspberry Pi.
```
    ~$ pip3 install gpiozero
```
3. *simple-pid*: for implementation of PID controllers in python.
```
    ~$ pip3 install simple-pid
```
4. *driver_picolog*: Wallbox's custom driver for TC08 device, located in `../lib` and `../aux_files folders` (TODO: we have to find a method to run a setup.py file that installs this library in a python accessible location in file system)

5. *pydevd*: optional, for remote debugging. See installation instructions below.

### How to install *pydevd*
1. Install cython (this allows build pydevd files on the raspberry pi to improve performance): 
```
    ~$ sudo apt install cython
```
2. Install pydevd: 
```
    ~$ pip3 install pydevd
```

### Set up server and client machines to debug using Eclipse
After installing *pydevd* launch Eclipse on the client machine and use a ssh console to connect to the server machine (on windows use PuTTY).

#### On client machine (where Eclipse IDE runs):
1. Open Debug Perspective in Eclipse(Windows), and start PyDev Server (Pydev->Start Debug Server).
2. Wait to the debugged script to be attached to the running server in Eclipse.

#### On server machine (where code runs):
On a ssh console, run the follwing commands. Export environment variables to set up pydevd, assuming a Raspberry Pi linux device:

```
    export PYTHONBREAKPOINT=pydevd.settrace
    export PATHS_FROM_ECLIPSE_TO_PYTHON="[[\"/home/jf/liclipse/workspace/thermal_chamber/\",\"/home/alarm/thermal_chamber\"]]"
```

The list order for folder replacements is [["client_location", "server_location"]].

Run the script to be debugged

```
    ~$ python3 main.py
```

Example python code (both files are the same on client and server):

```python
    import time

    if __name__ == '__main__':
        # Use Python 3 awesome breakpoint() function to connect to debug server
        # IP address of client machine (the one that runs Eclipse)
        breakpoint('192.168.7.1')

        print("Hello world!")

        while True:
            print("another hello world!")
            time.sleep(1)
```

When running the script locally (i.e. outside a debug session), do:

```
    ~$ export PYTHONBREAKPOINT=0
``` 

to avoid problems with parameters passed to `breakpoint()` function.

To automate the steps required to run the scrip to be debugged, a shell script is provided. After updating folder locations inside the `remote_debug.sh` script, just run:

```
    ~$ ./remote_debug.sh main.py -v
```
