from QuasarClass import quasar, START, STOP
from time import sleep
from instruments import yokogawa, bk_source, itech_source, detect_coms
from statistics import mean, stdev


# Change this variable to the address of your instrumen
'''
VISA_ADDRESS = 'TCPIP::192.168.2.241::inst0::INSTR'
GRID_ELEMENT = '4'
BATT_ELEMENT = '3'
    
yoko_instrument = yokogawa(VISA_ADDRESS, GRID_ELEMENT, BATT_ELEMENT)
'''

INIT_AC = True
INIT_DC = True

# Change this variable to the address of your instrument
BK_ADDRESS_input = 'USB0::0xFFFF::0x7749::526E19103::0::INSTR'  # IP connection does not work using USB instead
ITECH_ADDRESS = 'USB0::0x2EC7::0x6000::803384022747250002::0::INSTR'


GRID_VOLTAGE_SETTING = 230
GRID_FREQ_SETTING = 50
GRID_CURRENT_LIMIT = 30

BAT_VOLTAGE_SETTING = 360
BAT_CURRENT_LIMIT = 20

# Setup the BK connected to the DC side
itech_instrument = itech_source(ITECH_ADDRESS)
itech_instrument.output_state('OFF')
sleep(1) # Wait voltage to go down


# Setup the BK connected to the AC side
bk_instrument = bk_source(BK_ADDRESS_input)


if INIT_AC:
    bk_instrument.output_state('OFF')
    bk_instrument.recall_default()
    sleep(1) # Wait voltage to fall
    
    # Set the BK Power Source
    bk_instrument.set_ac_voltage(GRID_VOLTAGE_SETTING)
    bk_instrument.set_ac_freq(GRID_FREQ_SETTING)
    bk_instrument.set_current_limit(GRID_CURRENT_LIMIT)

    
    bk_instrument.output_state('ON')
    sleep(2) # Wait voltage to rise


# Initialize the quasar. Arguments are: Name of the converter, port and slave number (keep 10)
p1 = quasar('TEST', detect_coms()[0], 10)

# Put the password to unlock 
p1.unlock_maintanance()

while True:
    if p1.read(241, 0, False) < 50:
        print(p1.read(241, 0, False))
        break
    else:
        sleep(1)
        print('MSG: Waiting for offset calibration.')
        print(p1.read(241, 0, False))

# Setup the BK connected to the DC side
itech_instrument = itech_source(ITECH_ADDRESS)

if INIT_DC:
    itech_instrument.recall_default()
    itech_instrument.clear_errors()
    itech_instrument.set_remote()
        
    # Set the BK Power Source
    itech_instrument.set_voltage(BAT_VOLTAGE_SETTING)
    itech_instrument.set_current_limit_pos(BAT_CURRENT_LIMIT)
    itech_instrument.set_current_limit_neg(-BAT_CURRENT_LIMIT)
    
    itech_instrument.output_state('ON')
    sleep(2) # Wait voltage to rise
    
    itech_instrument.read_errors()

# Set the quasar in current mode
p1.set_current_mode()

# Checklist verifies that there is DC voltage and no errors
p1.checklist()

# Initialize the quasar. Arguments are: Name of the converter, port and slave number (keep 10)
p1 = quasar('TEST', detect_coms()[0], 10)

# Put the password to unlock 
p1.unlock_maintanance()

# Set the quasar in current mode
p1.set_current_mode()

p1.unlock_test_mode()


MODBUS_TESTING_MODE_ORDER_LO = 3000
TESTING_MODE_SET_GUN_LOCK    = 0x2000
TESTING_MODE_D1_D2_RLY_CLOSE = 0x1000

p1.write(MODBUS_TESTING_MODE_ORDER_LO, TESTING_MODE_D1_D2_RLY_CLOSE + TESTING_MODE_SET_GUN_LOCK, 0, False)

# Checklist verifies that there is DC voltage and no errors
p1.checklist()

p1.set_op(START) # Basically, start and stop
p1.set_current(17)    # No limit on current
p1.set_power(40)  # Set the power according to the table
p1.update_values() # Get values from the converter



vrms = []
irms = []
vbat = []
ibat = []

while True:
    # Read charger variables
    for i in range(10):
            vrms.append(p1.read(240, 2, False))
            irms.append(p1.read(241, 2, False))
            vbat.append(p1.read(244, 1, False))
            ibat.append(p1.read(245, 2, True))
    '''
    # Read yokogawa variables
    vrms_yoko, vbat_yoko = yoko_instrument.read_volages()
    irms_yoko, ibat_yoko = yoko_instrument.read_currents()

    vrms_error = (mean(vrms)-vrms_yoko)/vrms_yoko*100
    irms_error = (mean(irms)-irms_yoko)/irms_yoko*100
    vbat_error = (mean(vbat)-vbat_yoko)/vbat_yoko*100
    ibat_error = (mean(ibat)-ibat_yoko)/ibat_yoko*100
            
    print('Vrms mean:\t %.2f\t stdev: %.2f \t Vrms_yoko: %.2f \t error %%: %.3f' % (mean(vrms), stdev(vrms), vrms_yoko, vrms_error))   
    print('Irms mean:\t %.2f \t stdev: %.2f \t Irms_yoko: %.2f \t error %%: %.3f' % (mean(irms), stdev(irms), irms_yoko , irms_error))        
    print('Vbat mean:\t %.2f\t stdev: %.2f \t Vbat_yoko: %.2f \t error %%: %.3f' % (mean(vbat), stdev(vbat), vbat_yoko, vbat_error))       
    print('Ibat mean:\t %.2f \t stdev: %.2f \t Vbat_yoko: %.2f \t error %%: %.3f' % (mean(ibat), stdev(ibat), ibat_yoko, ibat_error))
    
    power_input = mean(vrms) * mean(irms)
    power_output = mean(vbat) * mean(ibat)
    
    power_input_yoko = vrms_yoko * irms_yoko
    power_output_yoko = vbat_yoko * ibat_yoko
    
    power_input_error = (power_input_yoko-power_input)/power_input_yoko*100.0
    power_output_error = (power_output_yoko-power_output)/power_output_yoko*100.0

    print('Power input:\t %.2f \t Power input Yoko %.2f \t error %%: %.3f' % (power_input, power_input_yoko, power_input_error))
    print('Power output:\t %.2f \t Power output Yoko %.2f \t error %%: %.3f' % (power_output, power_output_yoko, power_output_error))
    
    power_eff = power_output/power_input*100
    power_eff_yoko = power_output_yoko/power_input_yoko*100
    
    
    print('Power eff:\t %.2f \t %.2f' % (power_eff, power_eff_yoko))
        
    print('--------------------------------------')
    '''
        
    print('Vrms mean:\t %.2f\t stdev: %.2f' % (mean(vrms), stdev(vrms)))
    print('Irms mean:\t %.2f \t stdev: %.2f' % (mean(irms), stdev(irms)))     
    print('Vbat mean:\t %.2f\t stdev: %.2f' % (mean(vbat), stdev(vbat)))   
    print('Ibat mean:\t %.2f \t stdev: %.2f' % (mean(ibat), stdev(ibat)))




