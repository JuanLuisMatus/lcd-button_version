"""
# Script for basic Quasar configuration
Author: EP
Date: 2020.02.19
"""
import sys
sys.path.append('../lib') # Inlcudes the path to the user libraries folder

from time import sleep
import socket

from QuasarClass import *

CHARGER_NUMBER = 'WB043017'
CHARGER_NUMBER = 'WB039476'
CHARGER_NUMBER = 'WB031989'
CHARGER_NUMBER = 'WB039484'
CHARGER_NUMBER = 'WB041018'
CHARGER_NUMBER = 'WB042824'
CHARGER_NUMBER = 'WB042824'


#p1 = quasar('TEST', 'RS485', 10) # to connect through modbus
p1 = quasar('TEST', 'TCP', socket.gethostbyname(CHARGER_NUMBER)) # to connect through hmi tcp

p1.unlock_maintenance()
        
p1.set_current_mode()
p1.set_dc_current(17)    # No limit on current
p1.set_ac_current(32)    # No limit on current
p1.set_power(0)  # Set the power according to the table
print('Setting operation parameters...')
p1.update_values()

p1.unlock_operator_mode()
p1.write(GRID_FREQ_SETTING, 1, 0, False) # Set grid codes

p1.unlock_test_mode()
# Disable communication alarm
p1.write(3001, 4, 0, False)  
p1.write(3008, 0xfdff, 0, False)


while True:
    if p1.read(MODBUS_SYSTEM_BOOT_STATUS, 0, False) > 20:
        break
    else:
        sleep(5)
        print('MSG: Waiting for offset calibration.')
        

print('Locking GUN.')
p1.close_DC_relays()

print('Ready to rock...')


 
















