"""
# Basic endurance script, this is highly based on Txema Jupiter script
@author: Emiliano Penovi
@company: WallBox 
@date: 2020.05.05
"""
# Base Txema Jupiter project
# https://bitbucket.org/wallbox/power_electronics_scripts/commits/a535849a1ddbf5bb87a4bef9cf02702102bf8e17

# stimulus data generated with NL5 export data function, see

import sys
sys.path.append('../lib') # Inlcudes the path to the user libraries folder

from time import sleep
from datetime import timedelta
import socket
import csv


from QuasarClass import *

# Configuration settings
GUN_CABLE = True
INIT_QUASAR = True

CHARGER_NUMBER = 'WB043014'
CHARGER_NUMBER = 'WB043017'
CHARGER_NUMBER = 'WB031989'
CHARGER_NUMBER = 'WB039476'
CHARGER_NUMBER = 'WB041018'


# Load and parse the stimulus signals
# creates a dictionary where key are first column csv names
with open('stimulus_data.csv', mode='r') as stimulus_file:
    reader = csv.DictReader(stimulus_file, delimiter=',')
    # generate dictionary with stimulus_data
    stimulus_data = {}
    for row in reader:
        for key in row:
            value = row[key]
            stimulus_data.setdefault(key,[]).append(value)
            

# Initialize the quasar.
#p1 = quasar('TEST', 'RS485', 10) # to connect through modbus
p1 = quasar(CHARGER_NUMBER, 'TCP', socket.gethostbyname(CHARGER_NUMBER)) # to connect through hmi tcp

if INIT_QUASAR:
    p1.unlock_maintenance()
            
    p1.set_current_mode()
    p1.set_dc_current(17)    # No limit on current
    p1.set_ac_current(32)    # No limit on current
    p1.set_power(0)  # Set the power according to the table
    print('Setting operation parameters...')
    p1.update_values()
    
    if p1.is_in_error():
        p1.reset() # Reset manager is autonomous in the class. Resets are limited in number. 
  
    p1.unlock_operator_mode()
    p1.write(GRID_FREQ_SETTING, 1, 0, False) # Set grid codes
    
    p1.unlock_test_mode()
    # Disable communication alarm
    p1.write(3001, 4, 0, False)  
    p1.write(3008, 0xfdff, 0, False)
    
    
    if GUN_CABLE:
        while True:
            if p1.read(MODBUS_SYSTEM_BOOT_STATUS, 0, False) > 20:
                break
            else:
                sleep(5)
                print('MSG: Waiting for offset calibration.')
                
    
        print('Locking GUN.')
        p1.close_DC_relays()

print('Ready to rock...')


# From now on, executes the script
start_time = time.time()
for idx, data_time in enumerate(stimulus_data['time(s)'][:-1]):
    next_step_time = float(stimulus_data['time(s)'][(idx+1)])
    
    while ((time.time() - start_time) < next_step_time) and (not(p1.is_in_error())):
        # parse NL5 valiables 
        setting_op = int(float(stimulus_data['V(ACTIONS_REG)'][idx]))
        setting_power = int(float(stimulus_data['V(pw_setting)'][idx]))
        
        p1.set_op(setting_op) # Basically, start and stop
        p1.set_power(setting_power)  # Set the power according to the table
        p1.update_values() # Get values from the converter
        
        p1.print_stats() # Some stats are printed using the class
        
        remaining_time = float(stimulus_data['time(s)'][-1]) - (time.time() - start_time)
        
        print ("Remaining time:\t{:s}".format(str(timedelta(seconds = remaining_time)).split('.')[0]))
        
        print ("\nGenerate a failure to stop, i.e. disconnect usb cable (then wait 10s) or trip the AC breaker.")

        print('Next step in:\t{:.2f} s'.format(next_step_time- (time.time() - start_time)))
        print ("--------------------------------------------------------------------------\n")

        p1.buffer_2_file()

        try:
            time.sleep(max(0, min(10, remaining_time)))
            if p1.is_in_error():
                p1.reset() # Reset manager is autonomous in the class. Resets are limited in number. 
        except KeyboardInterrupt:
            p1.raise_error("Terminated by user")    


print('Exiting script and saving data.')
data = p1.terminate() # Stop the quasar
del p1 # Destroy the instance and free allocated memory



