"""
# Basic script to get the important data from the Yokogawa
@author: Emiliano Penovi
@company: WallBox 
@date: 2020.02.19
"""
from pathlib import Path
import sys
import time
from time import sleep

from instruments import yokogawa
from QuasarClass import quasar
from numpy import abs


class Logger(object):
    # This class is used for logging to a file the console printcouts
    def __init__(self, log_file):
        self.terminal = sys.stdout
        self.log = open(log_file, "a")

    def write(self, message):
        self.terminal.write(message)
        self.log.write(message)  

    def flush(self):
        #this flush method is needed for python 3 compatibility.
        #this handles the flush command by doing nothing.
        #you might want to specify some extra behavior here.
        pass  

# Settings variables
YOKO_ADDRESS = 'TCPIP::192.168.2.241::inst0::INSTR'

GRID_ELEMENT = '3'
BATT_ELEMENT = '4'


# Initialize log file
LOGS_FOLDER = './'
LOG_FILE_NAME = LOGS_FOLDER+ 'yoko_data' + time.strftime("_%Y%m%d%H%M%S") + '.log'
Path(LOGS_FOLDER).mkdir(parents=True, exist_ok=True)
sys.stdout = Logger(LOG_FILE_NAME)

# Create instruments to use
yoko_instrument = yokogawa(YOKO_ADDRESS, GRID_ELEMENT, BATT_ELEMENT)

while True:

    # Read yokogawa variables
    vrms_yoko, vbat_yoko = yoko_instrument.read_voltages()
    irms_yoko, ibat_yoko = yoko_instrument.read_currents()
    
    pin_yoko, pbat_yoko = yoko_instrument.read_powers()
    
    pin_yoko = abs(pin_yoko)
    pbat_yoko = abs(pbat_yoko)
    
    if pin_yoko > pbat_yoko:
        power_eff_yoko = pbat_yoko/pin_yoko
    else:
        power_eff_yoko = pin_yoko/pbat_yoko
    
    
    
    print('\nYokogawa data:\n')
    
    print(time.strftime("%Y/%m/%d %H:%M:%S")+'\n')
    print('In Voltage RMS: \t%.2f [V]' % vrms_yoko)
    print('Out Voltage RMS:\t%.2f [V]' % vbat_yoko)
    print('In Current RMS: \t%.2f [A]' % irms_yoko)
    print('Out Current RMS:\t%.2f [A]' % ibat_yoko)
    
    
    print('In Power:  \t\t%.2f [W]' % pin_yoko)
    print('Out Power: \t\t%.2f [W]' % pbat_yoko)
    
    print('Efficiency: \t\t%.2f [%%]' % (power_eff_yoko * 100))
    print('Losses: \t\t%.2f [W]' % abs(pin_yoko - pbat_yoko))
    print('Losses 7kW in:   \t%.2f [W] (estimation)' % (7000 * (1-power_eff_yoko)))
    
    try:
        delta_eff = (power_eff_yoko - last_power_eff_yoko)/last_power_eff_yoko
        print('Delta eff:: \t\t%.2f [%%]' % (delta_eff * 100))
        last_power_eff_yoko = power_eff_yoko
        sleep (600) # Wait 10 min 
    except:
        last_power_eff_yoko = power_eff_yoko
    
    print('\n')



