"""
# This script check quasar errors and restart if the test stops
Author: EP
Date: 2020.04.17
"""
from time import sleep
from datetime import datetime
import winsound
import socket

from QuasarClass import quasar


CHARGER_NUMBER = 'WB043017'
CHARGER_NUMBER = 'WB039476'

MODBUS_UNRECOVERABLE_ERROR_HI = 205
MODBUS_UNRECOVERABLE_ERROR_LO = 206
MODBUS_RECOVERABLE_ERROR_HI = 207
MODBUS_RECOVERABLE_ERROR_LO = 208

MODBUS_PS_ACTIONS_REGISTER = 20


MODBUS_L1_AC_VOLTAGE_RMS = 240
MODBUS_L1_AC_CURRENT_RMS = 241
MODBUS_L1_DC_BUS_VOLTAGE = 243
MODBUS_L1_DC_VOLTAGE = 244
MODBUS_L1_DC_CURRENT = 245


MODBUS_L1_PFC_TEMP = 261
MODBUS_L1_PHx_TEMP  = 262
MODBUS_L1_KC_LINK_TEMP = 263
MODBUS_L1_TRAFO_TEMP = 264
MODBUS_L1_AMBIENT_TEMP = 265


MODBUS_L1_DC_DC_SWITCHING_FREC = 251

#p1 = quasar('TEST', 'RS485', 10) # to connect through modbus
p1 = quasar('TEST', 'TCP', socket.gethostbyname(CHARGER_NUMBER)) # to connect through hmi tcp


while True:

    now = datetime.now() # current date and time
    print('Checking errors. Time: %s' % now.strftime("%Y.%m.%d %H.%M.%S"))

    # Read errors registers
    UNRECOVERABLE_ERROR_HI = p1.read(MODBUS_UNRECOVERABLE_ERROR_HI, 0, False)
    UNRECOVERABLE_ERROR_LO = p1.read(MODBUS_UNRECOVERABLE_ERROR_LO, 0, False)
    RECOVERABLE_ERROR_HI = p1.read(MODBUS_RECOVERABLE_ERROR_HI, 0, False)
    RECOVERABLE_ERROR_LO = p1.read(MODBUS_RECOVERABLE_ERROR_LO, 0, False)

    L1_AC_VOLTAGE_RMS = p1.read(MODBUS_L1_AC_VOLTAGE_RMS, 2, False)
    L1_AC_CURRENT_RMS = p1.read(MODBUS_L1_AC_CURRENT_RMS, 2, False)
    L1_DC_BUS_VOLTAGE = p1.read(MODBUS_L1_DC_BUS_VOLTAGE, 1, False) 
    L1_DC_VOLTAGE = p1.read(MODBUS_L1_DC_VOLTAGE, 1, False)
    L1_DC_CURRENT = p1.read(MODBUS_L1_DC_CURRENT, 2, False) 

    L1_PFC_TEMP = p1.read(MODBUS_L1_PFC_TEMP, 0, False)
    L1_PHx_TEMP = p1.read(MODBUS_L1_PHx_TEMP, 0, False)
    L1_KC_LINK_TEMP = p1.read(MODBUS_L1_KC_LINK_TEMP, 0, False)
    L1_TRAFO_TEMP = p1.read(MODBUS_L1_TRAFO_TEMP, 0, False)
    L1_AMBIENT_TEMP = p1.read(MODBUS_L1_AMBIENT_TEMP, 0, False)

    L1_DC_DC_SWITCHING_FREC = p1.read(MODBUS_L1_DC_DC_SWITCHING_FREC, 1, False)



    if UNRECOVERABLE_ERROR_HI + UNRECOVERABLE_ERROR_LO + RECOVERABLE_ERROR_HI + RECOVERABLE_ERROR_LO:
        print('MSG: System in error.')
        print('UNRECOVERABLE_ERROR_HI: %d' % UNRECOVERABLE_ERROR_HI)
        print('UNRECOVERABLE_ERROR_LO: %d' % UNRECOVERABLE_ERROR_LO)
        print('RECOVERABLE_ERROR_HI: %d' % RECOVERABLE_ERROR_HI)
        print('RECOVERABLE_ERROR_LO: %d' % RECOVERABLE_ERROR_LO)

        # Emit sound
        sound_duration = 3000  # milliseconds
        sound_freq = 500  # Hz
        winsound.Beep(sound_freq, sound_duration) 
        
        # Reset errors
        p1.write(MODBUS_PS_ACTIONS_REGISTER, 4, 0, False)
        sleep(1)
        # Restart quasar
        p1.write(MODBUS_PS_ACTIONS_REGISTER, 1, 0, False)

    else:
        print('MSG: NO errors.')
        print('{:<20}{:.2f}'.format('L1_AC_VOLTAGE_RMS:', L1_AC_VOLTAGE_RMS))
        print('{:<20}{:.2f}'.format('L1_AC_CURRENT_RMS:', L1_AC_CURRENT_RMS))
        print('{:<20}{:.2f}'.format('L1_DC_BUS_VOLTAGE:', L1_DC_BUS_VOLTAGE))
        print('{:<20}{:.2f}'.format('L1_DC_VOLTAGE:', L1_DC_VOLTAGE))
        print('{:<20}{:.2f}'.format('L1_DC_CURRENT:', L1_DC_CURRENT))
        print('\n')
        print('{:<20}{:.2f}'.format('L1_PFC_TEMP:', L1_PFC_TEMP))
        print('{:<20}{:.2f}'.format('L1_PHx_TEMP:', L1_PHx_TEMP))
        print('{:<20}{:.2f}'.format('L1_KC_LINK_TEMP:', L1_KC_LINK_TEMP))
        print('{:<20}{:.2f}'.format('L1_TRAFO_TEMP:', L1_TRAFO_TEMP))
        print('{:<20}{:.2f}'.format('L1_AMBIENT_TEMP:', L1_AMBIENT_TEMP))
        print('\n')
        print('{:<20}{:.2f}Khz'.format('L1_DCDC_SW_FREC:', L1_DC_DC_SWITCHING_FREC))
        print('\n')
        power_in = L1_AC_VOLTAGE_RMS * L1_AC_CURRENT_RMS
        power_out = L1_DC_VOLTAGE * L1_DC_CURRENT
        print('{:<20}{:.2f}'.format('PIN (aprox):', power_in))
        print('{:<20}{:.2f}'.format('POUT (aprox):', power_out))
        print('{:<20}{:.2f}'.format('Losses (aprox):', abs(power_out-power_in)))
        print('{:<20}{:.2f}'.format('Efficiency (aprox):', power_out/power_in*100.0))

        
        
        print('-------------------------------------\n')
        
        sound_duration = 100  # milliseconds
        sound_freq = 8000  # Hz
        winsound.Beep(sound_freq, sound_duration) 
        
    sleep(10)



