from QuasarClass import quasar, START, STOP
from time import sleep
from instruments import yokogawa, bk_source, itech_source, detect_coms



INIT_AC = True
INIT_DC = True

# Change this variable to the address of your instrument
BK_ADDRESS_input = 'USB0::0xFFFF::0x7749::526E19103::0::INSTR'  # IP connection does not work using USB instead
ITECH_ADDRESS = 'USB0::0x2EC7::0x6000::803384022747250002::0::INSTR'



GRID_VOLTAGE_SETTING = 230
GRID_FREQ_SETTING = 50
GRID_CURRENT_LIMIT = 30

BAT_VOLTAGE_SETTING = 360
BAT_CURRENT_LIMIT = 20

# Setup the BK connected to the DC side
itech_instrument = itech_source(ITECH_ADDRESS)
itech_instrument.output_state('OFF')
sleep(1) # Wait voltage to go down


# Setup the BK connected to the AC side
bk_instrument = bk_source(BK_ADDRESS_input)


if INIT_AC:
    bk_instrument.output_state('OFF')
    bk_instrument.recall_default()
    sleep(10) # Wait voltage to fall
    
    # Set the BK Power Source
    bk_instrument.set_ac_voltage(GRID_VOLTAGE_SETTING)
    bk_instrument.set_ac_freq(GRID_FREQ_SETTING)
    bk_instrument.set_current_limit(GRID_CURRENT_LIMIT)

    
    bk_instrument.output_state('ON')
    sleep(5) # Wait voltage to rise


# Initialize the quasar. Arguments are: Name of the converter, port and slave number (keep 10)
p1 = quasar('TEST', detect_coms()[0], 10)

# Put the password to unlock 
p1.unlock_maintanance()

while True:
    if p1.read(241, 0, False) < 50:
        print(p1.read(241, 0, False))
        break
    else:
        sleep(1)
        print('MSG: Waiting for offset calibration.')
        print(p1.read(241, 0, False))

# Setup the BK connected to the DC side
itech_instrument = itech_source(ITECH_ADDRESS)

if INIT_DC:
    itech_instrument.recall_default()
    itech_instrument.clear_errors()
    itech_instrument.set_remote()
        
    # Set the BK Power Source
    itech_instrument.set_voltage(BAT_VOLTAGE_SETTING)
    itech_instrument.set_current_limit_pos(BAT_CURRENT_LIMIT)
    itech_instrument.set_current_limit_neg(-BAT_CURRENT_LIMIT)
    
    itech_instrument.output_state('ON')
    sleep(2) # Wait voltage to rise
    
    itech_instrument.read_errors()

p1.unlock_calibration()

p1.unlock_test_mode()


MODBUS_TESTING_MODE_ORDER_LO = 3000
TESTING_MODE_SET_GUN_LOCK    = 0x2000
TESTING_MODE_D1_D2_RLY_CLOSE = 0x1000

p1.write(MODBUS_TESTING_MODE_ORDER_LO, TESTING_MODE_D1_D2_RLY_CLOSE + TESTING_MODE_SET_GUN_LOCK, 0, False)


# Set the quasar in current mode
p1.set_current_mode()

# Checklist verifies that there is DC voltage and no errors
p1.checklist()


for attempt in range(5): # Retry 10 times
    try:
        
        # Tell the charger the calibration process is going to start
        p1.write(20, 64, 0, False)

        # Should return a 71
        charger_status = p1.read(201, 0, False)

        print('Charger status (Should be 71): %s'% charger_status)

        if 71 != (charger_status): # Fail if conditions aren't met
            raise Exception('MSG: Charging initializing... try #: %s'% attempt)

    except Exception as error:
        # we fail an attemp - wait or print error and retry
        print(error)
        sleep(1)
        
    else:
        # Everythig ok, continue
        break
else:
    # we failed all the attempts - deal with the consequences.
    raise Exception("ERR: Charger unready.")


sleep(2) # Wait some time before reading
# Calibrate Charger Voltages
VGRID = bk_instrument.read_magnitude('voltage')
VBATT = itech_instrument.read_magnitude('voltage')

print('Measured VGRID: %s' % VGRID)
print('Measured VBATT: %s' % VBATT)

# Check the measured values are within range
if abs(VGRID - GRID_VOLTAGE_SETTING) <= .05* GRID_VOLTAGE_SETTING and\
    abs(VBATT - BAT_VOLTAGE_SETTING) <= .05* BAT_VOLTAGE_SETTING:
    pass
else:
    raise Exception('ERR: The calibration Voltages are out of range.')

p1.write_calibration_voltages(VGRID, VBATT)

sleep(2) # Wait some time before reading


# Prepare instrument to calibrate current
p1.set_op(START) # Basically, start and stop
p1.set_current(17)    # No limit on current
p1.set_power(40)  # Set the power according to the table
p1.update_values() # Get values from the converter

sleep(5) # Wait some time before reading
IGRID = abs(bk_instrument.read_magnitude('current'))
IBATT = abs(itech_instrument.read_magnitude('current'))

IGRID_aprox = abs(p1.read(241, 2, False))
IBATT_aprox = abs(p1.read(245, 2, True))

print('Measured IGRID: %s' % IGRID)
print('Measured IBATT: %s' % IBATT)
'''
print('Measured IGRID_aprox: %s' % IGRID_aprox)
print('Measured IBATT_aprox: %s' % IBATT_aprox)

# Check the measured values are within range
if abs(IGRID - IGRID_aprox) <= .05* IGRID and\
    abs(IBATT - IBATT_aprox) <= .05* IBATT:
    pass
else:
    raise Exception('ERR: The calibration Voltages are out of range.')

'''
p1.write_calibration_currents(IGRID, IBATT)

# Read if calibration is successful
# Should return a 0
sleep(2)
print(p1.read(201, 0, False))

    
    
    
    
    
    
    
    
    
    
    