"""
This script cicles the itech voltage to test the chargers at all operatins points

"""
from instruments import itech_source
from numpy import linspace, concatenate
from time import sleep


ITECH_ADDRESS = 'USB0::0x2EC7::0x6000::0123456789ABCEDF::0::INSTR'    

BAT_VOLTAGE_MAX = 520
BAT_VOLTAGE_MIN = 150

BAT_CURRENT_LIMIT = 60

BAT_STEP_PERIOD = 60*10 # period in seconds

STEP_NUMBER = 60

# Setup the BK connected to the DC side
itech_instrument = itech_source(ITECH_ADDRESS)


itech_instrument.recall_default()
itech_instrument.clear_errors()
itech_instrument.set_remote()

itech_instrument.set_current_limit_pos(BAT_CURRENT_LIMIT)
itech_instrument.set_current_limit_neg(-BAT_CURRENT_LIMIT)

itech_instrument.set_voltage((BAT_VOLTAGE_MAX + BAT_VOLTAGE_MIN)/2)


itech_instrument.output_state('ON')


print('MSG: Start BAT voltage Sweep...')

# Generate the voltage vector
voltage_vector = concatenate((linspace(BAT_VOLTAGE_MIN, BAT_VOLTAGE_MAX, num = STEP_NUMBER), linspace(BAT_VOLTAGE_MAX, BAT_VOLTAGE_MIN, num = STEP_NUMBER)))

while True:
    for bat_voltage in voltage_vector:
        # Set the BK Power Source
        
        for attempt in range(5): # Retry
            try:
                itech_instrument.set_voltage(int(bat_voltage))
            except Exception as error:
                # we fail an attemp - wait or print error and retry
                raise Exception(error)
            else:
                # Every thing ok, continue
                break
        else:
            # we failed all the attempts - deal with the consequences.
            print('ERR: to many errors quiting.')
        
        sleep(BAT_STEP_PERIOD/STEP_NUMBER)
        
            














