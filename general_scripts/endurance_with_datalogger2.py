"""
# Basic endurance script, this is highly based on Txema Jupiter script
@author: Emiliano Penovi
@company: WallBox
@date: 2020.05.05
"""
# Base Txema Jupiter project
# https://bitbucket.org/wallbox/power_electronics_scripts/commits/a535849a1ddbf5bb87a4bef9cf02702102bf8e17

# stimulus data generated with NL5 export data function, see

import sys
sys.path.append('../lib') # Inlcudes the path to the user libraries folder
sys.path.append('../aux_files') # Inlcudes the path to the user libraries folder

import logging
from time import sleep
from datetime import timedelta
import socket
import csv
import threading
from random import randint

from QuasarClass import *

# Configuration settings
GUN_CABLE = True
INIT_QUASAR = True

CHARGER_NUMBER = 'WB043014'
CHARGER_NUMBER = 'WB043017'
CHARGER_NUMBER = 'WB031989'
CHARGER_NUMBER = 'WB039476'
CHARGER_NUMBER = 'WB041018'
CHARGER_NUMBER = 'WB031989'



def init_logger():
    # initialize the msg logger
    # DEBUG    Detailed information, typically of interest only when diagnosing problems.
    # INFO     Confirmation that things are working as expected.
    # WARNING  An indication that something unexpected happened, or indicative of some problem in the near future (e.g. ‘disk space low’). The software is still working as expected.
    # ERROR    Due to a more serious problem, the software has not been able to perform some function.
    # CRITICAL A serious error, indicating that the program itself may be unable to continue running.
    LOG_FILENAME = 'logfile.log'
    FORMAT_STR = '%(asctime)s\t%(levelname)s -- %(filename)s:%(lineno)s -- %(message)s'

    file_handler = logging.FileHandler(filename=LOG_FILENAME)
    stdout_handler = logging.StreamHandler(sys.stdout)
    
    handlers = [file_handler, stdout_handler]
    
    logging.basicConfig(
            level=logging.INFO, 
            format=FORMAT_STR,
            handlers=handlers)
    
    #sys.stderr = logging.error
    #sys.stdout = logging.info


def main():
    init_logger()

    equipment_list = {'driver_picolog294' : 'driver_picolog',
                      'driver_picolog748' : 'driver_picolog',
                      'driver_yokogawa' : 'driver_yokogawa'}
    
    
    # driver_picolog A0066/294
    ports_picolog294 = [{'label':'plog_ambient_1', 'direction' : 'out', 'value': 0, 'params':[0]},
                     {'label':'plog_kclinks', 'direction' : 'out', 'value': 0, 'params':[1]},
                     {'label':'plog_ambient_in', 'direction' : 'out', 'value': 0, 'params':[2]},
                     {'label':'plog_cosel_control', 'direction' : 'out', 'value': 0, 'params':[3]},
                     {'label':'plog_cosel_hbridge', 'direction' : 'out', 'value': 0, 'params':[4]},
                     {'label':'plog_xfrm', 'direction' : 'out', 'value': 0, 'params':[5]},
                     {'label':'plog_film_caps_pcb', 'direction' : 'out', 'value': 0, 'params':[6]},
                     {'label':'plog_electrolitics', 'direction' : 'out', 'value': 0, 'params':[7]},
                     {'label':'plog_dcdc2_fet', 'direction' : 'out', 'value': 0, 'params':[8]}]
   
    # driver_picolog A0061/748
    ports_picolog748 = [{'label':'plog_ambient_2', 'direction' : 'out', 'value': 0, 'params':[0]},
                     {'label':'plog_pfc_drv', 'direction' : 'out', 'value': 0, 'params':[1]},
                     {'label':'plog_pfc_murata', 'direction' : 'out', 'value': 0, 'params':[2]},
                     {'label':'plog_pfc_fet', 'direction' : 'out', 'value': 0, 'params':[3]},
                     {'label':'plog_dcdc1_drv', 'direction' : 'out', 'value': 0, 'params':[4]},
                     {'label':'plog_dcdc1_murata', 'direction' : 'out', 'value': 0, 'params':[5]},
                     {'label':'plog_dcdc1_fet', 'direction' : 'out', 'value': 0, 'params':[6]},
                     {'label':'plog_dcdc2_drv', 'direction' : 'out', 'value': 0, 'params':[7]},
                     {'label':'plog_dcdc2_murata', 'direction' : 'out', 'value': 0, 'params':[8]}]    

    
    ports_yoko = [{'label':'yoko_vgrid', 'direction' : 'out', 'value': 0, 'params':['URMS,2']},
                 {'label':'yoko_vbat', 'direction' : 'out', 'value': 0, 'params':['URMS,1']},
                 {'label':'yoko_igrid', 'direction' : 'out', 'value': 0, 'params':['IRMS,2']},
                 {'label':'yoko_ibat', 'direction' : 'out', 'value': 0, 'params':['IRMS,1']},
                 {'label':'yoko_pgrid', 'direction' : 'out', 'value': 0, 'params':['P,2']},
                 {'label':'yoko_pbat', 'direction' : 'out', 'value': 0, 'params':['P,1']}]
    
          
    equipment_params = {'driver_picolog294': {'sn':'A0066/280'},
                        'driver_picolog748': {'sn':'A0066/314'}
                        }
    
    equipment_ports = {'driver_picolog294' : ports_picolog294,
                       'driver_picolog748' : ports_picolog748,
                       'driver_yokogawa': ports_yoko}
    
    
    eq_manager = equipments_manager(equipment_list, equipment_params, equipment_ports)

    
    # Load and parse the stimulus signals
    # creates a dictionary where key are first column csv names
    with open('stimulus_data.csv', mode='r') as stimulus_file:
        reader = csv.DictReader(stimulus_file, delimiter=',')
        # generate dictionary with stimulus_data
        stimulus_data = {}
        for row in reader:
            for key in row:
                value = row[key]
                stimulus_data.setdefault(key,[]).append(value)
    
    
    # Initialize the quasar.
    #p1 = quasar('TEST', 'RS485', 10) # to connect through modbus
    p1 = quasar(CHARGER_NUMBER, 'TCP', socket.gethostbyname(CHARGER_NUMBER)) # to connect through hmi tcp
    
    if INIT_QUASAR:
        p1.unlock_maintenance()
    
        p1.set_current_mode()
        p1.set_dc_current(17)    # No limit on current
        p1.set_ac_current(32)    # No limit on current
        p1.set_power(0)  # Set the power according to the table
        print('Setting operation parameters...')
        p1.update_values()
    
        if p1.is_in_error():
            p1.reset() # Reset manager is autonomous in the class. Resets are limited in number.
    
        p1.unlock_operator_mode()
        p1.write(GRID_FREQ_SETTING, 1, 0, False) # Set grid codes
    
        p1.unlock_test_mode()
        # Disable communication alarm
        p1.write(3001, 4, 0, False)
        p1.write(3008, 0xfdff, 0, False)
    
    
        if GUN_CABLE:
            while True:
                if p1.read(MODBUS_SYSTEM_BOOT_STATUS, 0, False) > 20:
                    break
                else:
                    sleep(5)
                    print('MSG: Waiting for offset calibration.')
    
    
            print('Locking GUN.')
            p1.close_DC_relays()
    
    print('Ready to rock...')
    
    
    # From now on, executes the script
    start_time = time.time()
    for idx, data_time in enumerate(stimulus_data['time(s)'][:-1]):
        next_step_time = float(stimulus_data['time(s)'][(idx+1)])
    
        while ((time.time() - start_time) < next_step_time) and (not(p1.is_in_error())):
            # parse NL5 valiables
            setting_op = 1 #int(float(stimulus_data['V(ACTIONS_REG)'][idx]))
            setting_power = -100#*int(float(stimulus_data['V(pw_setting)'][idx]))

            p1.append_instruments_buffer(eq_manager.data)

    
            p1.set_op(setting_op) # Basically, start and stop
            p1.set_power(setting_power)  # Set the power according to the table
            p1.update_values() # Get values from the converter
    
            p1.print_stats() # Some stats are printed using the class
    
            remaining_time = float(stimulus_data['time(s)'][-1]) - (time.time() - start_time)
    
            print ("Remaining time:\t{:s}".format(str(timedelta(seconds = remaining_time)).split('.')[0]))
    
            print ("\nGenerate a failure to stop, i.e. disconnect usb cable (then wait 10s) or trip the AC breaker.")
    
            print('Next step in:\t{:.2f} s'.format(next_step_time- (time.time() - start_time)))
            print ("--------------------------------------------------------------------------\n")
    
        
    
            # save data to file every 10 minutes
            try:
                if (time.time() - last_save_time) > 600:
                    print('Saving file.')

                    p1.buffer_2_file()
                    last_save_time = time.time()
            except Exception as error:
                print('Cound\'t save file: %s' % error)
                last_save_time = time.time()
    
            try:
                time.sleep(max(0, min(10, remaining_time)))
                if p1.is_in_error():
                    p1.reset() # Reset manager is autonomous in the class. Resets are limited in number.
            except KeyboardInterrupt:
                p1.raise_error("Terminated by user")
    
    
    print('Exiting script and saving data.')
    data = p1.terminate() # Stop the quasar
    del p1 # Destroy the instance and free allocated memory
    

class equipments_manager:
    def __init__(self, equipment_list, equipment_parameters={}, equipment_ports={}):
        self.equipment_drivers = {}

        self.data = {}
        
        for drv_tag in equipment_list.keys():
            try:
                params = equipment_parameters[drv_tag]
            except:
                params = {}
            
            ports = equipment_ports[drv_tag]
            drv_name = equipment_list[drv_tag]
            
            driver = equipment_driver(drv_tag, drv_name, params, ports)
            
            self.equipment_drivers.setdefault(drv_tag, driver) # creates a item in the dictionary
            
            sleep(3)
            threading.Thread(target=self.update_drivers).start()
        

    def update_drivers(self):
        # function to update the drivers in parallel
        while True:
            for equipment in self.equipment_drivers:
                try:
                    driver = self.equipment_drivers[equipment]
                    
                    driver.update_state()
                    
                    # udpate data
                    for port in driver.ports:
                        port_label = port['label']
                        port_value = port['value']
                        
                        # update port data
                        self.data[port_label] = port_value           
                
                except Exception as error:
                    print(error)
                    
            next_step_time = 20
            sleep(next_step_time)
    

class equipment_driver:
    # creates a generic driver object
    def __init__(self, drv_tag, drv_name, params, ports):
        
        self.drv_tag = drv_tag
        self.drv_name = drv_name
        self.params = params
        self.ports = ports
        self.drv_state = 'FAIL'
    
        self.load_driver_class()
    

    def load_driver_class(self):
        # import driver functions form driver python file
        import importlib
        try:
            driver_python_file = self.drv_name

            module = importlib.import_module(driver_python_file)

            driver_class = getattr(module, 'cmp_driver')

            self.cmp_driver = driver_class(self.params)
            self.drv_state = 'OK'
            
            # Ejecute init code
            logging.info('%s executing init code.' % self.drv_tag)
            logging.info('%s functions loaded.' % self.drv_tag)
            
        except Exception as error:
            logging.error('%s functions cant be loaded. %s' % (self.drv_tag, error))
            raise Exception(error)


    def update_state(self):
        # this function should be runned every simulation step
        # each driver has three functions update_step, update_input, update_output and init, delete
        try:
            # check if driver is down and reinitialize
            if not self.drv_state == 'OK':
                self.load_driver_class()
            
        except Exception as error:
            logging.error('coudn\t initialize %s, check device status. %s' % (self.drv_tag, error))
            return
        
        try:
            logging.info('%s updating state.' % self.drv_tag)
    
            self.cmp_driver.update_step()
            
            for idx, port in enumerate(self.ports):
                if port['direction'] == 'in':
                    self.cmp_driver.update_input(port)        
                elif port['direction'] == 'out':
                    value = self.cmp_driver.update_output(port) 
                    self.ports[idx]['value'] = value
                    
        except Exception as error:
            self.drv_state = 'FAIL'
            logging.error('%s updating state fail. %s' % (self.drv_tag, error))            



if __name__ == '__main__':
    main()

