"""
# Script for calibration with Yoko in engineering lab 
Author: EP
Date: 2020.05.07
"""
import sys
sys.path.append('../lib') # Inlcudes the path to the user libraries folder

from time import sleep
import socket
from termcolor import colored

from QuasarClass import *
from instruments import yokogawa

CHARGER_NUMBER = 'WB043017'
CHARGER_NUMBER = 'WB039476'
CHARGER_NUMBER = 'WB041018'
CHARGER_NUMBER = 'WB031989'
CHARGER_NUMBER = 'WB039484'



def main():
    #p1 = quasar('TEST', 'RS485', 10) # to connect through modbus
    p1 = quasar('TEST', 'TCP', socket.gethostbyname(CHARGER_NUMBER)) # to connect through hmi tcp
    
    p1.unlock_maintenance()
            
    p1.set_current_mode()
    p1.set_dc_current(17)    # No limit on current
    p1.set_ac_current(32)    # No limit on current
    p1.set_power(0)  # Set the power according to the table
    print('Setting operation parameters...')
    p1.update_values()
    
    p1.unlock_operator_mode()
    p1.write(GRID_FREQ_SETTING, 1, 0, False) # Set grid codes
    
    p1.unlock_test_mode()
    # Disable communication alarm
    p1.write(3001, 4, 0, False)  
    p1.write(3008, 0xfdff, 0, False)
    
    
    while True:
        if p1.read(MODBUS_SYSTEM_BOOT_STATUS, 0, False) > 20:
            break
        else:
            sleep(5)
            print('MSG: Waiting for offset calibration.')
            
    
        print('Locking GUN.')
        p1.close_DC_relays()
    
                
    else:
        pass
    
    print('Ready to rock...')

    calibration(p1)
 
 



def calibration(p1):
    YOKO_ADDRESS = 'TCPIP::192.168.2.241::inst0::INSTR'
    
    GRID_ELEMENT = '2'
    BATT_ELEMENT = '1'
    
    yoko_instrument = yokogawa(YOKO_ADDRESS, GRID_ELEMENT, BATT_ELEMENT)

    p1.unlock_calibration()
    
    # Checklist verifies that there is DC voltage and no errors
    for attempt in range(10): # Retry 10 times
        try:
            
            # Tell the charger the calibration process is going to start
            p1.write(MODBUS_PS_ACTIONS_REGISTER, 64, 0, False)
    
            # Should return a 71
            charger_status = p1.read(MODBUS_SYSTEM_BOOT_STATUS, 0, False)
    
            print('Charger status (Should be 71): %s'% charger_status)
    
            if 71 != (charger_status): # Fail if conditions aren't met
                raise Exception('MSG: Charging initializing... try #: %s'% attempt)
    
        except Exception as error:
            # we fail an attemp - wait or print error and retry
            print(error)
            sleep(5)
            
        else:
            # Everythig ok, continue
            break
    else:
        # we failed all the attempts - deal with the consequences.
        raise Exception(colored("ERR: Charger unready.", 'red'))
    
    
    for attempt in range(10): # Retry 10 times
        try:
            # Check if eprom was written
            calibration_status = p1.read(MODBUS_CHARGER_MESSAGES, 0, False) >> 3 & 1

            if calibration_status == 1:
                print(colored('MSG: Calibration init succesfull (reg#182): must be 1, result: %d' % calibration_status,'green'))
            else:
                raise Exception('MSG: Calibration start failed (reg#182): must be 1, result: %d' % calibration_status)      

        except Exception as error:
            # we fail an attemp - wait or print error and retry
            print(error)
            sleep(1)
            
        else:
            # Everythig ok, continue
            break
    else:
        # we failed all the attempts - deal with the consequences.
        raise Exception(colored('ERR: Calibration failed (reg#182): must be 0, result: %d' % calibration_status, 'red'))
    
    
    # Calibrate Charger Voltages
    VGRID, VBATT = yoko_instrument.read_voltages()
    
    p1.write_calibration_voltages(VGRID, VBATT)
    
    sleep(1) # Wait some time before reading

    
    # Prepare instrument to calibrate current
    p1.set_power(100)  # Set the power according to the table
    p1.set_op(START)
    
    p1.update_values()

    sleep(5) # Wait some time before reading
    
    
    IGRID, IBATT = yoko_instrument.read_currents()
    
     
    # Check the measured values are within range
    if abs(IGRID) <= 2 or abs(IBATT) <= 2:
        raise Exception('ERR: The calibration Currents are to low.')
    
    
    p1.write_calibration_currents(IGRID, IBATT)

    sleep(1)
    
    for attempt in range(10): # Retry 10 times
        try:
            # Check if eprom was written
            calibration_status = p1.read(MODBUS_CHARGER_MESSAGES, 0, False) >> 3 & 1

            if calibration_status == 0:
                print(colored('MSG: Calibration succesfull (reg#182): must be 0, result: %d' % calibration_status, 'green'))

            else:
                raise Exception('WAR: Calibration failed (reg#182): must be 0, result: %d' % calibration_status)      

        except Exception as error:
            # we fail an attemp - wait or print error and retry
            print(error)
            sleep(1)
            
        else:
            # Everythig ok, continue
            break
    else:
        # we failed all the attempts - deal with the consequences.
        itech_instrument.output_state('OFF')
        raise Exception(colored('ERR: Calibration start failed (reg#182): must be 0, result: %d' % calibration_status, 'red'))
    
    

if __name__ == "__main__":

    main()



