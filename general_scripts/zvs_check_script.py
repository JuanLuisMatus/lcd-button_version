from instruments import yokogawa
from instruments import keysight, detect_coms
from instruments import itech_source
from statistics import mean
from statistics import stdev
from pathlib import Path
import sys
import time
from time import sleep
from QuasarClass import quasar
from numpy import linspace, abs

class Logger(object):
    # This class is used for logging to a file the console printcouts
    def __init__(self, log_file):
        self.terminal = sys.stdout
        self.log = open(log_file, "a")

    def write(self, message):
        self.terminal.write(message)
        self.log.write(message)  

    def flush(self):
        #this flush method is needed for python 3 compatibility.
        #this handles the flush command by doing nothing.
        #you might want to specify some extra behavior here.
        pass  


ITECH_ADDRESS = 'USB0::0x2EC7::0x6000::803384022747230013::0::INSTR'
KEYSIGHT_ADDRESS = 'USB0::0x2A8D::0x1766::MY58493160::0::INSTR'
YOKO_ADDRESS = 'TCPIP::192.168.2.241::inst0::INSTR'


GRID_ELEMENT = '3'
BATT_ELEMENT = '4'

GRID_FREQ_SETTING = 50

BAT_VOLTAGE_MAX = 500
BAT_VOLTAGE_MIN = 150
STEP_NUMBER = 60

BAT_VOLTAGE_SET = 500


# Initialize log file
LOGS_FOLDER = './'
LOG_FILE_NAME = LOGS_FOLDER + str(BAT_VOLTAGE_SET) + time.strftime("_%Y%m%d%H%M%S") + '.log'
Path(LOGS_FOLDER).mkdir(parents=True, exist_ok=True)
sys.stdout = Logger(LOG_FILE_NAME)

# Create instruments to use
yoko_instrument = yokogawa(YOKO_ADDRESS, GRID_ELEMENT, BATT_ELEMENT)
keysight_instrument = keysight(KEYSIGHT_ADDRESS)
itech_instrument = itech_source(ITECH_ADDRESS)

itech_instrument.set_remote()


# Create charger to control
CHARGER_NUMBER = 'WB039476'

GUN = False
'''
p1 = quasar('TEST', 'RS485', 10) # to connect through modbus
# p1 = quasar('TEST', CHARGER_NUMBER) # to connect through hmi tcp

p1.unlock_maintenance()
        
p1.set_current_mode()
p1.set_dc_current(17)    # No limit on current
p1.set_ac_current(32)    # No limit on current
p1.set_power(0)  # Set the power according to the table
p1.update_values()

p1.unlock_operator_mode()
p1.write(GRID_FREQ_SETTING, 1, 0, False) # Set grid codes

p1.reset()


# Set Operating point
itech_instrument.set_voltage(400)

p1.set_op(1)
p1.update_values()
'''

voltage_vector = linspace(BAT_VOLTAGE_MIN, BAT_VOLTAGE_MAX, num = STEP_NUMBER)



#print('MSG: Connect battery voltage.')
#input()


itech_instrument.set_voltage(BAT_VOLTAGE_SET)

sleep(10)


# Read yokogawa variables
vrms_yoko, vbat_yoko = yoko_instrument.read_voltages()
irms_yoko, ibat_yoko = yoko_instrument.read_currents()

pin_yoko, pbat_yoko = yoko_instrument.read_powers()

pin_yoko = abs(pin_yoko)
pbat_yoko = abs(pbat_yoko)

if pin_yoko > pbat_yoko:
    power_eff_yoko = pbat_yoko/pin_yoko*100
    power_eff_norm = pbat_yoko/pin_yoko*6000
else:
    power_eff_yoko = pin_yoko/pbat_yoko*100
    power_eff_norm = pin_yoko/pbat_yoko*6000



print('\nYokogawa data:\n\n')

print('In Voltage RMS: \t%.2f [V]' % vrms_yoko)
print('Out Voltage RMS:\t%.2f [V]' % vbat_yoko)
print('In Current RMS: \t%.2f [A]' % irms_yoko)
print('Out Current RMS:\t%.2f [A]' % ibat_yoko)


print('In Power:  \t\t%.2f [W]' % pin_yoko)
print('Out Power: \t\t%.2f [W]' % pbat_yoko)

print('Efficiency: \t\t%.2f [%%]' % power_eff_yoko)
print('Losses: \t\t%.2f [W]' % abs(pin_yoko - pbat_yoko))
print('Losses NORM 6kW:\t%.2f [W]' % 6000 - power_eff_norm)

print('\n')

sleep(10)

keysight_instrument.save_capture('_'+str(BAT_VOLTAGE_SET))


