
NL5 Complete Package
----------------------------------------

This Package contains latest build of the NL5 Circuit Simulator, and all files 
distributed with the complete NL5 package.

Without a License, NL5 will work in Demo mode, which is fully functional, but 
with 20 components limit. However, it will still simulate schematics with any 
number of components created by licensed NL5 version.

No special installation is required: simply copy content of the Package into 
one directory, for instance: C:/Program Files/NL5.

License and new updates are available at NL5 website: nl5.sidelinesoft.com.



Files and directories in the Package:
-------------------------------------

nl5.exe - NL5 executable

nl5.chm - NL5 Help file, should be located in the same directory as nl5.exe

manual.pdf - NL5 User's Manual 

releasenotes.txt - release notes for all NL5 revisions

Examples/Components - examples on all component types and models. Open schematic, 
        run transient (Transient/Start) or AC analysis (AC/Start), where applicable.

Examples/Demo - demo schematics. Open schematic, run transient (Transient/Start) 
        or AC analysis (AC/Start), where applicable.

Examples/MATLAB - example on NL5-MATLAB link (NL5 schematic and MATLAB m-file)

Examples/Schematic - examples on some schematic features

Examples/Script - script examples. Open script (Tools/Script/Open script), then 
        Run script. 
	
Examples/Sweep_AC_Source - examples on calculating AC response using Sweep AC Source 
        method. Open schematic and run AC analysis (AC/Start).

Examples/Z-transform - examples on calculating AC response using Z-transform 
        method. Open schematic and run AC analysis (AC/Start).

Examples/components.txt - this NL5 script runs demo circuits for all components 
        located in the Examples/Components directory. For each component it runs 
        Transient and AC response (if applicable). To run the NL5 script, see 
        instructions in the file.

Examples/demo.txt - This NL5 script runs demo circuits located in the Examples/Demo 
        directory. For each schematic it runs Transient and/or AC response, performs
        Transient and/or AC parameter sweep. To run the NL5 script, see instructions
        in the file.

Examples/sweep_AC_source.txt - this NL5 script runs AC response of demo circtiots using
        "Sweep AC source" method (even if "Linearize schematic" method is applicable).
        To run the NL5 script, see instructions in the file.

Examples/Z-transform.txt - this NL5 script runs AC response of demo circuits using
        "Z-transformSweep AC source" method (even if "Linearize schematic" method 
	is applicable). To run the NL5 script, see instructions in the file.