/**********************************************************
*
* This NL5 script runs demo circuits 
* located in the Examples/Sweep_AC_Source directory 
* 
* For each component it runs AC response using "Sweep AC source" method,
* even if "Linearize schematic" method is applicable
*
*  To run the script:
*
*      drag and drop this file onto NL5 icon, 
*  or
*      open NL5
*      open script window (Tools/Script)
*      open script file sweep_AC_source.txt
*      run script
*
***********************************************************/

open Sweep_AC_Source/boost.nl5;
sleep(1000); ac; Sleep(2000); close;

open Sweep_AC_Source/buck.nl5;
sleep(1000); sleep(1000); ac; Sleep(2000); close;

open Sweep_AC_Source/cauer.nl5;
sleep(1000); ac; Sleep(2000); close;

open Sweep_AC_Source/cauer_lc.nl5;
sleep(1000); ac; Sleep(2000); close;

open Sweep_AC_Source/lcr.nl5;
sleep(1000); ac; Sleep(2000); close;

open Sweep_AC_Source/notch.nl5;
sleep(1000); ac; Sleep(2000); close;

open Sweep_AC_Source/rc.nl5;
sleep(1000); tran; sleep(1000); ac; Sleep(2000); close;

open Sweep_AC_Source/shifter.nl5;
sleep(1000); ac; Sleep(2000); close;

open Sweep_AC_Source/sweep_AC.nl5;
sleep(1000); tran; sleep(1000); ac; Sleep(2000); close;

open Sweep_AC_Source/X_Block-8_SubCir.nl5;
sleep(1000); tran; sleep(1000); ac; Sleep(2000); close;

open Sweep_AC_Source/X_Code_C_Ex2.nl5;
sleep(1000); tran; sleep(1000); ac; Sleep(2000); close;

open Sweep_AC_Source/X_Code_C_Ex3.nl5;
sleep(1000); tran; sleep(1000); ac; Sleep(2000); close;

open Sweep_AC_Source/X_Code_DLL_Ex2.nl5;
sleep(1000); tran; sleep(1000); ac; Sleep(2000); close;

open Sweep_AC_Source/X_Code_DLL_Ex3.nl5;
sleep(1000); tran; sleep(1000); ac; Sleep(2000); close;


