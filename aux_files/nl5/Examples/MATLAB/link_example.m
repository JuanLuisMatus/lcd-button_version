clear
clc
close all
R=logspace(-1,1);
Header='http://127.0.0.1/?';
Cmd2=[Header,'tran'];
Cmd3=[Header,'ready'];
Cmd4=[Header,'V(out)%200,50,.1'];
for k=1:length(R)
    Cmd1=[Header,'R1=',num2str(R(k))];
    urlread(Cmd1);
    urlread(Cmd2);
    Response='0';
    while strcmp(Response,'0')
        Response=urlread(Cmd3);
    end
    Graph(k,:)=str2num(urlread(Cmd4));
end
Graph=Graph';
surf(Graph)
shading flat
colormap jet
colorbar
ylim([0 400])