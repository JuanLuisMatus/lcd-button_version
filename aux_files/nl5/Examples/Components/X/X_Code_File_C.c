int size=20;
double x[size];
double k[size] = { 1 }; // the rest are zeroes

init() {
int i, j;
for( i=1; i<size; ++i ) {
  for( j=i; j>0; --j ) {
    k[j] = k[j] + k[j-1];
  }
}

double sum = sum(k);
for( i=0; i<size; i+=1 ) k[i] = k[i]/sum;

}

main() {

int i;
for( i=size-1; i>0; --i) {
  x[i]=x[i-1];
}
x[0]=input;

output = 0;
for( i=0; i<size; ++i) {
  output += x[i] * k[i];
}

}