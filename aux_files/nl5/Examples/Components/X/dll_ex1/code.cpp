//---------------------------------------------------------------------------

#include <windows.h>

#pragma argsused

extern "C"  __declspec (dllexport) int main( double t, double* x )
{
    x[3] = x[0];  // max
    if( x[1] > x[3] ) x[3] = x[1];
    if( x[2] > x[3] ) x[3] = x[2];

    x[4] = x[0];  // min
    if( x[1] < x[4] ) x[4] = x[1];
    if( x[2] < x[4] ) x[4] = x[2];

    x[5] = (x[0] + x[1] + x[2]) / 3.;  // average
    return 0;
}


