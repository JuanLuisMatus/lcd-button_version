//---------------------------------------------------------------------------

#include <windows.h>

#pragma argsused


#define SIZE 20
double d[SIZE];
double k[SIZE];

extern "C"  __declspec (dllexport) int init( double t, double* x )
{
    int i, j;
    k[0] = 1.0;

    for( i=1; i<SIZE; ++i ) {
        k[i] = 0.0;
        for( j=i; j>0; --j ) {
            k[j] = k[j] + k[j-1];
        }
    }

    double sum=0;
    for( i=0; i<SIZE; ++i ) sum += k[i];
    for( i=0; i<SIZE; ++i ) k[i] = k[i]/sum;
    return 0;

}

extern "C"  __declspec (dllexport) int main( double t, double* x )
{
    int i;
    for( i=SIZE-1; i>0; --i ) {
        d[i] = d[i-1];
    }
    d[0] = x[0];

    double y=0;
    for( i=0; i<SIZE; ++i ) {
        y += d[i] * k[i];
    }
    x[1] = y;
    return 0;
}

