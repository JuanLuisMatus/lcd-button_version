//---------------------------------------------------------------------------

#include <windows.h>

#pragma argsused

static double d[10];
static int index;
static double y;

extern "C"  __declspec (dllexport) int init( double t, double* x )
{
    for( int i=0; i<10; ++i ) d[i] = 0.0;
    index = 0;
    y = 0.0;
    return 0;
}

extern "C"  __declspec (dllexport) int main( double t, double* x )
{
    y -= d[index];
    d[index] = x[0];
    y += d[index];
    ++index;
    if( index>=10 ) index=0;
    x[1] = y/10;
    x[2] = index;
    return 0;
}

