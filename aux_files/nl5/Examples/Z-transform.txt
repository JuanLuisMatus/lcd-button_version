/**********************************************************
*
* This NL5 script runs demo circuits 
* located in the Examples/Z-transform directory
* 
* For each component it runs AC response using "Z-transform" method,
* even if "Linearize schematic" method is applicable
*
*  To run the script:
*
*      drag and drop this file onto NL5 icon, 
*  or
*      open NL5
*      open script window (Tools/Script)
*      open script file Z-transform.txt
*      run script
*
***********************************************************/

open Z-transform/boost.nl5;
sleep(1000); ac; Sleep(2000); close;

open Z-transform/buck.nl5;
sleep(1000); ac; Sleep(2000); close;

open Z-transform/cauer.nl5;
sleep(1000); ac; Sleep(2000); close;

open Z-transform/lcr.nl5;
sleep(1000); ac; Sleep(2000); close;

open Z-transform/notch.nl5;
sleep(1000); ac; Sleep(2000); close;

open Z-transform/rc.nl5;
sleep(1000); ac; Sleep(2000); close;

open Z-transform/shifter.nl5;
sleep(1000); ac; Sleep(2000); close;

open Z-transform/sweep_AC.nl5;
sleep(1000); ac; Sleep(2000); close;

